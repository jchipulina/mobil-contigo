import React, { Component } from "react";
import { View, Text, AsyncStorage } from "react-native";
import Reactotron from "reactotron-react-native";
import "./reactotron";
import { applyMiddleware, compose, createStore } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import redux from "@redux";
import Estructura from "@navigation";
import { BackHandler } from 'react-native';

//console.disableYellowBox = true;

Reactotron.configure() // controls connection & communication settings
  .useReactNative() // add all built-in react native plugins
  .connect(); // let's connect!

const thunks = [thunk];
global.backgroundColor = '#FFF'//'#2369bd';//"#f8d12c"
global.textColor = '#333'//'#fff'; //#000
global.topBgColor = '#f8d12c' //#'#f3f3f3'
global.topTextColor = '#000' //#000
global.topBgColorInvert = '#000'
global.topTextColorInvert = '#fff'
global.buttonColorInvert = '#ccc';


global.azul_super_claro = "rgba(133,163,206,1)";
global.azul_oscuro = "#001E60";
global.azul = "#0C479D";
global.rojo = "#FE000C";
global.azul_claro = "#009CDE";
global.gris = "#838C9B";
global.blanco = "#FFFFFF";

let almacen = null;
if (__DEV__) {
  almacen = Reactotron.createStore(redux, compose(applyMiddleware(...thunks)));
} else {
  almacen = createStore(redux, compose(applyMiddleware(...thunks)));
}

class Aplicacion extends React.Component {
  constructor(props) {
    super(props);
    this.state = { load: false };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
  }

  onBackButtonPressed() {
    return false;
  }


  componentWillMount() {
    this.checkUser();
  }
  checkUser() {
    global.user = null;
    global.saveUser = function () {
      AsyncStorage.setItem("user", JSON.stringify(global.user)).then((value) => { });
    }
    AsyncStorage.getItem("user").then((value) => {
      if (value == null) {
        Reactotron.log("USER NULL");
      } else {
        global.user = JSON.parse(value);
        Reactotron.log("USER EXIST");
      }
      this.setState({ load: true });
    });

  }
  render() {
    if (this.state.load) {
      return (
        <Provider store={almacen}>
          <Estructura />
        </Provider>
      );
    }
    return (<View style={{ flex: 1, backgroundColor: '#000' }} />);
  }
}

export default (__DEV__ ? Reactotron.overlay(Aplicacion) : Aplicacion);
