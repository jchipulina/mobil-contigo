import React, { Component } from "react";
import {
  View,
  Image,
  StyleSheet,
  Alert,
  ImageBackground,
  Text,
  TextInput,
  Dimensions,
  Platform,
  ActivityIndicator,
  NetInfo
} from "react-native";
import { connect } from "react-redux";
import { Navbar, Header, Button, IphoneXFix } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import Reactotron from "reactotron-react-native";
import { img } from "@media";
import OneSignal from 'react-native-onesignal';


var { height, width } = Dimensions.get("window");
var dniValues = ["", "", "", "", "", "", "", ""];
global.notification_push_id = '';
global.notification_user_id = '';

class PerfilSelector extends Component {
  constructor(props) {
    super(props);
   	 	let requiresConsent = false;       
        this.state = {
	        loading: false,
			netStatus: false,
      
            emailEnabled: false, 
            animatingEmailButton : false, 
            initialOpenFromPush : "Did NOT open from push",
            activityWidth : 0,
            width: 0,
            activityMargin: 0,
            jsonDebugText : "",
            privacyButtonTitle : "Privacy Consent: Not Granted",
            requirePrivacyConsent : requiresConsent 
        };
		OneSignal.init("e35af3c3-387b-4fa7-8c6a-2aa81538a308");
	    OneSignal.addEventListener('ids', this.onIds); 
	    //OneSignal.configure();
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }


  onIds(device) {
	  //console.warn("device.pushToken "+device.pushToken);
	  //console.warn("device.userId "+device.userId);
	  global.notification_push_id = device.pushToken;
	  global.notification_user_id = device.userId;
  }
  render() {
    var inputSize = width / 8 - 8;

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    return (
      <ImageBackground source={img.bg_img_4} style={{width: '100%', height: '100%',flex:1,backgroundColor: '#000'}}>
      <View style={{ flex: 1, alignItems: "center",backgroundColor: 'rgba(0,0,0,0.6)'}}>
        <View style={{flex:1,width: '100%',backgroundColor: 'transparent',justifyContent: 'center',alignItems: 'center'}}>
        <View style={{ width: 100,height: 100,backgroundColor: "transparent",marginBottom:0 }}>
          <Image
            style={{
              flex: 1,
              width: 100,
              height: 100,
              resizeMode: "contain",
              marginBottom: 10
            }}
            source={img.logo}
          />
        </View>
        <View style={{ width: "90%", backgroundColor: "transparent",marginBottom:20 }}>
          <Text
            style={[
              styles.text,
              {
                backgroundColor: "transparent",
                fontSize: 25,
                textAlign: "center",
                padding: 0,
                color: 'white'
              }
            ]}
          >
           <Text style={{ fontFamily: "EMprint-Bold" }}>¡</Text>
            <Text> Bienvenido al{"\n"}</Text>
            <Text style={{ fontFamily: "EMprint-Bold" }}>{"Club Mobil Delvac "}</Text>
            <Text style={{ fontFamily: "EMprint-Bold" }}>!</Text>

            <Text>
              {"\n"}
              {"\n"}
            </Text>

            
          </Text>
        </View>
        
        <View style={{ width: "90%", backgroundColor: "transparent" ,bottom:50,position:'absolute'}}>
          <Button title="INGRESAR" type={"yellow"} onPress={() => {
		  		
		  		if (global.user == null) {
                  this.props.navigation.navigate("Login");
                } else {
                  if (global.user.activo==true) {
                    this.props.navigation.navigate("Home");
                  } else {
                    this.props.navigation.navigate("Login");
                  }
                }
		  		
          }} />
        </View>
       

        </View>


        <IphoneXFix />
        {loading}
      </View>
      </ImageBackground>
    );
  }

}
const styles = StyleSheet.create({
  wrapper: {},
  slide0: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  slide1: {
    flex: 1,
    alignItems: "center"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  slide3: {
    flex: 1
  },
  text: {
    fontSize: 22,
    padding: 25,
    fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Regular",
    backgroundColor: "transparent",
    color: global.textColor
  }
});
export default PerfilSelector;
