import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Linking,
  View,
  Image,
  Platform,
  ActivityIndicator,
  FlatList
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { Inicio, Header, Navbar } from "@components";
import { List, ListItem } from "react-native-elements";
import Swipeout from 'react-native-swipeout';
 
import TimeAgo from 'react-native-timeago';

// Timestamp can be any valid data type accepted in a Moment.js constructor
// Currently accepts string, number, array, or a Date instance

var BadgeAndroid;
if(Platform.OS === 'android'){
	BadgeAndroid = require('react-native-android-badge');
}

var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');



var counter = 0;
var lista_all;
var lista;
var scope;
var miembroObj;


class Notificaciones extends Component {
  constructor(props) {
    super(props);
    scope = this;
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      activeRowKey:null,
      index:0,
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    
    var tipo = '0';
    const url = global.domain + "/api/notificaciones/" + global.user.id+'/'+tipo;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.notifications,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };
  deleteNotification = (id) => {
    const { page, seed } = this.state;
    const url = global.domain + "/api/notificaciones_eliminar/"+id+"/"+global.user.id+'/0';
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.notifications,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };

  _keyExtractor = (item, index) => item.id;
  onRemoveItemList(){  
	  console.warn("onRemoveItemList "+rowID);
	  scope.deleteNotification(rowID);
  }
  onChange(){
	  rowID = this.state.rowID;
	  console.warn("onChange "+this.state.rowID);
  }
  renderItem(item) {
    if (counter == 0) {
      counter = 1;
    } else {
      counter = 0;
    }
    let timestamp = Number(item.timestamp)*1000;
	var swipeoutBtns = [ { text: 'Eliminar',backgroundColor:'red',color:'white',onPress:this.onRemoveItemList} ]

    return (
	    // {...swipeSettings}
	    <Swipeout 
	    	right={swipeoutBtns} 
	    	rowID={item.id}
			sectionID={1}
	    	autoClose={true}
			onOpen={(sectionID, rowID) => {
			          this.setState({
			            sectionID,
			            rowID,
			          },this.onChange)
			}}
	    >
      <View
        style={{
          backgroundColor: counter == 0 ? "#eee" : "#ccc",
          padding: 15,
          paddingTop: 8,
          paddingBottom: 8,  
          minHeight:50, 
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems:'center'
        }} 
      >
      
        <Text
          style={{
            flex: 1,
            color: "black",
            fontFamily: global.fontRegular,
            fontSize: 16
          }}
        >
          {item.content}
        </Text>
        <Text
          style={{
            paddingLeft: 10,
            color: "black",
            fontFamily: global.fontRegular,
            fontSize: 16
          }}
        > 
          
        </Text>
         <TimeAgo time={timestamp} /> 
        
      </View>
      </Swipeout>
    );
  }
  

  refreshNotifications() { 
  		fetch(global.domain + "/api/notificaciones/" + global.user.id, {method: "POST"})
        .then((response) => response.json())
        .then((responseData) => {
	        	lista = new Array();
				lista_all = new Array();
	        	for(var i=0;i<responseData.notifications.length;i++){
		        	var not = responseData.notifications[i];
		        	console.warn(not.queued_at);
		        	var date = new Date(Number(not.queued_at)*1000);
		        	var obj = {	id: 1, phone: "1 hora", phones: "", name: not.contents.es, datetime: date, picture: '', ismenber: '' };
					lista.push(obj);
					lista_all.push(obj);
					
	        	}
				scope.setState({refreshing: false}); 
				scope.buildList();
        })
        .done();
  }
 
  componentWillMount(){
	  if(Platform.OS === 'android'){
	  	BadgeAndroid.setBadge(0);
	  }
	  //scope.refreshNotifications();
  }
  onNavigatorEvent(event) {

  }
  	_genRows(pressData: {[key: number]: boolean}): Array<Object> {

	    return lista;
  	}

  	_pressRow(rowID: number) {
	  	
	  	  
	}
	
  	_renderRow(rowData: Object, sectionID: number, rowID: number, highlightRow: (sectionID: number, rowID: number) => void) {
	   // var imgSource = THUMB_URLS[0];
	    var styleMenber = {};
	    if(rowData.ismenber){
		    styleMenber = {color:'rgba(225,27,76,1)'}
	    }
	    return (
	        <View>
	          <View style={styles.row}>
	            
	            <Text style={[styles.text,styleMenber]}>{rowData.name+'adoi asiudh asiudas iudas idughasidu asudg aiudg asiudg asid'}  </Text>
	            
	            <Text style={[styles.textnum,styleMenber]}> <TimeAgo time={rowData.datetime} />  </Text>
	            
	          </View>
	        </View>
	      
	    );
	  }
  		 _renderSeparator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) { 
	  		 return ( <View 
	  		 key={`${sectionID}-${rowID}`} 
	  		 style={{ height: 1, backgroundColor: adjacentRowHighlighted ? '#dadadd' : '#dadadd', }} />
	  		  ); 
	  		}
  		 
  		 
   buildList() { 
	    //var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}); 
	    //return { dataSource: ds.cloneWithRows(this._genRows({})) }; 
	    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}); 
	    this.setState({ dataSource : ds.cloneWithRows(this._genRows({})), loading : true });
	}

  _handleResults(results) {
	  if(results=="ALL"){
		  console.warn("ALL");
		  lista = lista_all;
		  scope.buildList();
	  }else{
		  lista = results;
		  scope.buildList();
		  console.warn(JSON.stringify(results));
	  }
	  
	  //scope.setState({ results });
	  
	  //scope.setState({ results });
	}
  _onRefresh() { 
	  this.setState({refreshing: true}); 
	  scope.refreshNotifications();
	}
	_onSelectCelular(index, data){
		console.warn("index "+index);
		console.warn("key "+index.key);
		console.warn("data "+index.data);
		//console.warn("DATA "+data);
	}
    render() {
    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.data.length == 0) {
        content = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Text
              style={{
                fontSize: 16,
                fontFamily: global.fontRegular,
                color: "black"
              }}
            >
              {"Aún no tiene notificaciones."}
            </Text>
          </View>
        );
      } else {
        content = (
          <FlatList
            keyExtractor={this._keyExtractor}
            data={this.state.data}
            renderItem={({ item }) => this.renderItem(item)}
          />
        );
      }
    }

    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Navbar
          onBack={() => {
            this.props.navigation.goBack();
          }}
        />
        
        <Header title={"NOTIFICACIONES"} type={"normal"} size={"normal"} />
       
        <View style={{ flex: 1 }}>{content}</View>
      </View>
    );
  }
}

    
var styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingTop:15,
    paddingRight:15,
    paddingBottom:15,
    backgroundColor: 'transparent',
  },
  retoslist:{
	  flex: 1,
	  backgroundColor: 'transparent',
  },
  text: {
    flex: 1,
    paddingTop: 4,
    marginTop:0,
    textAlign:'left',
    color:'#666',
    fontFamily: 'Montserrat',
    fontSize:12,
    marginLeft:5,
  },
  textnum: {
    paddingTop: 4,
    marginTop:0,
    textAlign:'left',
    color:'rgba(225,27,76,1)',
    fontFamily: 'Montserrat',
    fontSize:12,
    marginLeft:5,
  },
  thumb: {
    width: 30,
    height: 30,
    borderRadius: 13,
    marginLeft:18,
  },
  tabContent: {
    flex: 1,
    alignItems: 'center'
  },
  tabText: {
    color: 'white',
    margin: 50,
  },
  topText:{
	  marginTop:30
  },
  button: {fontFamily: 'Montserrat',fontSize: 15,paddingTop:5,letterSpacing:1.3, color: 'white'},
	button_container:{position:"relative",padding:12,width:280, height:47, overflow:'hidden', borderRadius:15,borderColor:'#FFF', borderWidth:1,backgroundColor: '#16113e'},
	
	button_small: {fontSize: 10,letterSpacing:1.3, color: 'white',margin:10,textDecorationLine:'underline'},
});

export default Notificaciones;
