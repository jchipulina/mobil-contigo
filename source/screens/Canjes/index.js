import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Linking,
  View,
  RefreshControl,
  ActivityIndicator,
  FlatList,
  List,
  ScrollView,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
//import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, HeaderMobil, Header, BottomSpace } from "@components";
import LinearGradient from 'react-native-linear-gradient';
import Image from 'react-native-image-progress';
import Icon from "react-native-vector-icons/MaterialIcons";
var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');


var scope = null;
class Canjes extends Component {
  constructor(props) {
    super(props);
    scope = this;
    global.canje = this;
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      para_canjear: [],
      canjeados: []
    };
  }
  static update() {
    if (global.updateCanjes) {
      global.updateCanjes = false;
      if (scope != null) {
        if (global.canje != null) global.canje.makeRemoteRequest();
      }
    }
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  onRefresh() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const url = global.domain + "/api/canjes/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        Reactotron.log(responseJson);

        this.setState({
          para_canjear: responseJson.para_canjear,
          canjeados: responseJson.canjeados,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };
  _keyExtractor = (item, index) => item.id;


  _renderItem = ({ item }) => (
    <MyListItem canjeado={false} onPress={(canje) => {
      this.props.navigation.navigate("CanjeItem", { canje: canje, canjeado: false });
    }}
      canje={item}
    />
  );
  _renderItemCanjeado = ({ item }) => (
    <MyListItem canjeado={true} onPress={(canje) => {
      this.props.navigation.navigate("CanjeItem", { canje: canje, canjeado: true });
    }}
      canje={item}
    />
  );
  render() {

    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.para_canjear.length == 0) {
        content = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center", marginBottom: 20 }}>
            <Text
              style={{
                fontSize: 14,
                fontFamily: global.fontRegular,
                color: "black"
              }}
            >{'No se encontraron canjes en proceso de entrega'}</Text>
          </View>
        );
      } else {
        content = (

          <View style={{ backgroundColor: "transparent" }}>
            <FlatList
              scrollEnabled={false}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(l) => l.id}
              data={this.state.para_canjear}
              onEndReachedThreshold={20}
              getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
              renderItem={this._renderItem}
            />
          </View>

        );
      }
    }
    content_canjeados = (<View></View>);


    if (this.state.canjeados.length == 0) {
      content_canjeados = (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>


          <Text
            style={{
              fontSize: 14,
              fontFamily: global.fontRegular,
              color: "black"
            }}
          >{'No se encontraron canjes cerrados'}</Text>


        </View>
      );
    } else {
      content_canjeados = (<View style={{ backgroundColor: "transparent" }}>
        <FlatList
          scrollEnabled={false}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(l) => l.id}
          data={this.state.canjeados}
          onEndReachedThreshold={20}
          getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
          renderItem={this._renderItemCanjeado}
        />
      </View>);
    }


    renderSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "86%",
            backgroundColor: "#CED0CE",
            marginLeft: "14%"
          }}
        />
      );
    };
    return (
      <LinearGradient colors={global.tabGradientColors} style={{ flex: 1 }}>
        <ScrollView refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={() => { this.onRefresh() }} />}>
          <Header title={"Historial de Canjes"} type={"azul"} marginLeft={25} />

          <View style={{ width: '100%', paddingLeft: 30, paddingRight: 30, marginTop: 20 }}>

            <View style={{ width: '100%', flexDirection: "row", marginBottom: 5 }}>
              <Text style={{ fontFamily: global.fontBold, color: "rgba(148,174,212,1)", fontSize: 14, paddingBottom: 15, paddingTop: 0 }}>{'CANJES EN PROCESO DE ENTREGA'}</Text>
              <Icon name={"lock-open"} size={15} color={"rgba(148,174,212,1)"} style={{ marginLeft: 5, marginTop: 1 }}></Icon>
            </View>

            {content}

          </View>
          <View style={{ width: '94%', marginTop: 10, marginLeft: '6%', marginBottom: 20, backgroundColor: '#D4EEF8', height: 1 }}></View>
          <View style={{ width: '100%', paddingLeft: 30, paddingRight: 30 }}>

            <View style={{ width: '100%', flexDirection: "row", marginBottom: 5 }}>
              <Text style={{ fontFamily: global.fontBold, color: "rgba(148,174,212,1)", fontSize: 14, paddingBottom: 15 }}>{'CANJES CERRADOS'}</Text>
              <Icon name={"lock"} size={15} color={"rgba(148,174,212,1)"} style={{ marginLeft: 5, marginTop: 1 }}></Icon>
            </View>

            {content_canjeados}

          </View>
          <BottomSpace />
        </ScrollView>
        <BottomSpace />
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({});



class MyListItem extends React.PureComponent {
  render() {
    var moment_fecha = null;
    if (this.props.canjeado) {
      moment_fecha = moment(this.props.canje.fecha_redencion, 'YYYY-MM-DD HH:mm:ss');
    } else {
      moment_fecha = moment(this.props.canje.fecha_canje, 'YYYY-MM-DD HH:mm:ss');
    }

    var fecha = moment_fecha.format('D MMMM YYYY');
    var nombre = this.props.canje.titulo;
    var codigo = this.props.canje.codigo;
    return (
      <TouchableOpacity onPress={() => {
        this.props.onPress(this.props.canje);
      }}>
        <View style={{ flexDirection: 'row', marginBottom: 20 }}>
          <View style={{ width: 70, backgroundColor: '#fff', borderRadius: 10 }}>
            <Image indicator={ActivityIndicator} resizeMode='contain' style={{ width: 70, height: 70, flex: 1, borderRadius: 10 }}
              source={{ uri: global.urlimg + this.props.canje.imagen }} />
          </View>
          <View style={{ flex: 1, marginLeft: 10, marginTop: 10 }}>
            <Text style={{ color: global.azul_oscuro, textAlign: 'left', fontFamily: global.fontBold }}>
              {nombre}
            </Text>
            {(codigo != "" && codigo != null) &&
              <Text style={{ color: global.azul, textAlign: 'left', fontFamily: global.fontBold }}>
                {codigo}
              </Text>
            }
            <Text style={{ color: '#7584A4', textAlign: 'left', fontFamily: global.fontRegular, fontWeight: '500' }}>
              {fecha}
            </Text>
          </View>

          <View style={{ backgroundColor: 'transparent', justifyContent: "center" }}>
            <View style={{
              height: 50, justifyContent: 'center', alignItems: 'center',
              borderRadius: 50 / 2, backgroundColor: '#7392C1'
            }}>
              <Text
                style={{
                  color: '#FFF',
                  textAlign: 'center',
                  fontFamily: global.fontBold,
                  paddingLeft: 10,
                  paddingRight: 10
                }}>
                {this.props.canje.puntos + '\nKMS'}
              </Text>
            </View>

          </View>

        </View>
      </TouchableOpacity>
    )
  }
}

export default Canjes;
