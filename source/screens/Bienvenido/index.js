import React, { Component } from 'react';
import { Platform, Dimensions, StyleSheet, Text, ImageBackground, View } from 'react-native';
import { Button, Header, IphoneXFix, Navbar } from '@components';
import Reactotron from "reactotron-react-native";
import { img } from "@media";
var { height, width } = Dimensions.get("window");
class Bienvenido extends Component {
    render() {
        var styleText = {
            color: '#FFF',
            fontSize: 40,
            padding: 20,
            paddingLeft: 50,
            paddingBottom: 0,
            textAlign: "left",
            fontFamily: global.fontBold,
            backgroundColor: "transparent"
        };
        var padding = "5%";
        if (new IphoneXFix().isIphoneX()) {
            padding = "15%"
        }
        return (
            <ImageBackground source={img.bg_porshe} style={{ flex: 1, width: '100%', backgroundColor: 'transparent' }}>
                <View style={{ flex: 1, justifyContent: 'flex-end', width: '100%', marginTop: 0, backgroundColor: "transparent", paddingBottom: padding }} >

                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <Text style={styleText}>
                            <Text>
                                <Text>{'Hola ' + global.user.nombres + ',\n'}</Text>
                                <Text>{'Bienvenido a,\n'}</Text>
                                <Text style={{ color: global.rojo }}>{'Mobil contigo'}</Text>
                            </Text>
                        </Text>
                        <Text style={{ fontFamily: 'EMprint-Bold', fontSize: 16, padding: 20, paddingLeft: 50, paddingTop: 25, color: "white", textAlign: "left", width: '100%' }}>{'¡Accede a un mundo de\nbeneficios por tus consumos!'}</Text>
                    </View>
                </View>
                <View style={{ alignItems: "center", marginBottom: 70 }}>
                    <Button title="Empezar" onPress={() => {
                        if (!global.user.activo) {
                            this.props.navigation.navigate('ValidacionForma');
                        } else {
                            this.props.navigation.navigate('App');
                        }


                    }} />
                </View>
            </ImageBackground>
        );
    }
}
export default Bienvenido;
