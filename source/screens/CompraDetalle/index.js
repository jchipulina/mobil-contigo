import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Linking,
  View,
  Image,
  ActivityIndicator,
  FlatList,
  List,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, HeaderMobil, Header } from "@components";
import LinearGradient from 'react-native-linear-gradient';

var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');

class CompraDetalle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  round(num) {
    return Math.round(num * 100) / 100
  }
  makeRemoteRequest = () => {
    const { params } = this.props.navigation.state;
    const url = "http://mobilcontigo.pe/api/ventas/ver/" + params.compra.id
    Reactotron.log("url " + url);
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        var total = params.compra.puntos;
        /* for (var i = 0; i < responseJson.detalle.length; i++) {
           total += Math.round(Number(responseJson.detalle[i].puntos));
         }*/
        this.setState({
          documento: responseJson.documento,
          fecha: responseJson.fecha,
          data: responseJson.detalle,
          total: this.round(total),
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };
  _keyExtractor = (item, index) => item.id;


  _renderItem = ({ item }) => (
    <MyListItem detalle={item} />
  );

  render() {

    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.data.length == 0) {
        content = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>


            <Text
              style={{
                fontSize: 16,
                fontFamily: global.fontRegular,
                color: "black"
              }}
            >{'No se pudo cargar el detalle.'}</Text>


          </View>
        );
      } else {
        content = (

          <View style={{ flex: 1 }}>
            <FlatList
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(l) => l.id}
              data={this.state.data}
              onEndReachedThreshold={20}
              getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
              renderItem={this._renderItem}
            />
          </View>

        );
      }
    }
    renderSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "86%",
            backgroundColor: "#CED0CE",
            marginLeft: "14%"
          }}
        />
      );
    };
    return (
      <LinearGradient colors={global.tabGradientColors} style={{ flex: 1 }}>
        <Header contentStyle={{ paddingTop: 5 }} onBack={() => {
          this.props.navigation.goBack();
        }} title={"FE-25105"} titleArrow="FACTURA" styleTitle={{ fontSize: 25, lineHeight: 35 }} styleTitleArrow={{ fontSize: 15, lineHeight: 15, paddingTop: 5 }} type={"azul"} marginLeft={25} kms={<Text style={{ fontSize: 12 }}>Total <Text style={{ fontSize: 15, paddingLeft: 10 }}> {this.state.total} KMS</Text></Text>} />
        <View style={{ width: '94%', marginTop: 10, marginLeft: '6%', marginBottom: 20, backgroundColor: '#D4EEF8', height: 1 }}></View>
        <View style={{ flex: 1, marginLeft: 30, marginRight: 30 }}>{content}</View>
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({});



class MyListItem extends React.PureComponent {
  round(num) {
    return Math.round(num * 100) / 100
  }
  render() {

    var moment_fecha = moment(this.props.detalle.fecha_canje, 'YYYY-MM-DD HH:mm:ss');
    var fecha = moment_fecha.format('D MMMM YYYY');
    var nombre = this.props.detalle.titulo;
    var codigo = this.props.detalle.codigo;

    return (
      <View>
        <View style={{ flexDirection: 'row', marginBottom: 20, backgroundColor: 'transparent' }}>
          <View style={{ flex: 1, marginLeft: 0, marginTop: 0 }}>
            <Text style={{ color: global.azul_oscuro, textAlign: 'left', fontFamily: global.fontBold, fontSize: 15 }}>
              {this.props.detalle.producto.descripcion}{" "}{(this.props.detalle.producto.tipo != null) ? this.props.detalle.producto.tipo : ''}
            </Text>
            <Text style={{ color: global.azul_claro, textAlign: 'left', fontFamily: global.fontSemiBold }}>
              {this.round(this.props.detalle.galones) + ' Galones'}
            </Text>
          </View>

          <View style={{ backgroundColor: 'transparent', justifyContent: "center" }}>
            <View style={{
              height: 32, justifyContent: 'center', alignItems: 'center',
              borderRadius: 50 / 2, backgroundColor: global.azul_oscuro
            }}>
              <Text
                style={{
                  color: '#FFF',
                  textAlign: 'center',
                  fontFamily: global.fontBold,
                  paddingLeft: 12,
                  paddingRight: 12,
                  fontSize: 15
                }}>
                {Math.round(this.props.detalle.puntos) + ' KMS'}
              </Text>
            </View>

          </View>

        </View>
      </View>
    )
  }
}

export default CompraDetalle;
