import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Header, Button, IphoneXFix } from "@components";
import Reactotron from "reactotron-react-native";
import { img } from "@media";
import { ScrollView } from "react-native-gesture-handler";
var scope;
var backgroundColor = "#FFF";
var colorText = "#000";
class Terminos extends Component {
  constructor(props) {
    super(props);
    scope = this;
    this.state = { index: 0, contenido: '' };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { params } = this.props.navigation.state;
    const url = global.domain + "/api/" + params.tipo;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          contenido: responseJson.contenido
        });
        this.setState({ loading: false });
      }).catch(error => {
        this.setState({ loading: false });
      });
  };
  render() {
    var loading = (<View></View>);
    if (this.state.loading) {
      loading = (
        <View style={{ flex: 1, height: '100%', top: 0, left: 0, right: 0, bottom: 0, position: "absolute", justifyContent: "center", alignItems: "center", backgroundColor: 'transparent' }}>
          <ActivityIndicator
            animating={true}
            color="#FFF"
            size="large"
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              height: 80
            }}
          />
        </View>
      );
    }

    const { params } = this.props.navigation.state;
    return (
      <View style={{ flex: 1, backgroundColor: global.azul_oscuro }}>
        <Header title={(params.tipo == "terminos") ? "Términos y\nCondiciones" : "Reglamento\nMobil™ Contigo"} styleTitle={{ color: global.azul_claro }} size={"normal"} />
        {loading}
        <ScrollView>
          <Text style={{ color: 'white', padding: 30, paddingTop: 20, fontFamily: global.fontRegular, textAlign: "justify" }}>{this.state.contenido}</Text>
        </ScrollView>
        <View style={{ width: '100%', alignItems: 'center', marginBottom: 40, marginTop: 20 }}>
          <Button
            title="Cerrar"
            onPress={() => {
              //this.saveInfo();
              this.props.navigation.pop();
            }}
          />
        </View>


      </View>
    );
  }
}

export default Terminos;
