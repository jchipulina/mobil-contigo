import React, { Component } from "react";
import {
  View,
  Alert,
  ActivityIndicator,
  Image,
  ImageBackground,
  Text,
  Platform,
  TouchableOpacity,
  TextInput,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { Navbar, Header, Button, IphoneXFix } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import Reactotron from "reactotron-react-native";
var { height, width } = Dimensions.get("window");
var dniValues = [0, 0, 0, 0, 0, 0];

class Codigo extends Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.state = {
      c1: "",
      c2: "",
      c3: "",
      c4: "",
      c5: "",
      c6: "",
      loading: false
    };
  }
  onChangeText(stext, num) {
    dniValues[num - 2] = stext;
    if (stext.length == 1) {
      this.focusFieldByID(num);
    }
    if (stext.length == 0) {
      //this.focusFieldByID(num-2);
    }
  }
  focusFieldByID(num) {
	  focus = num;
    switch (num) {
      case 1:
        this.txtField1.focus();
        break;
      case 2:
        this.txtField2.focus();
        break;
      case 3:
        this.txtField3.focus();
        break;
      case 4:
        this.txtField4.focus();
        break;
      case 5:
        this.txtField5.focus();
        break;
      case 6:
        this.txtField6.focus();
        break;
    }
  }
  emptyFieldByID(num) {
    switch (num) {
      case 1:
        this.setState({c1:''});
        break;
      case 2:
        this.setState({c2:''});
        break;
      case 3:
        this.setState({c3:''});
        break;
      case 4:
        this.setState({c4:''});
        break;
      case 5:
        this.setState({c5:''});
        break;
      case 6:
       this.setState({c6:''});
       break;
    }
  }
  getDNI() {
    console.warn(this.txtField1.text);
  }
  handleKeyPress({ nativeEvent: { key: keyValue } }) {
        if (keyValue === 'Backspace') {
            this.focusFieldByID(focus-1);
            this.emptyFieldByID(focus);
        }
    }
  render() {
    const { params } = this.props.navigation.state;
    var inputSize = 40;

    var styleBox = {
      width: 40,
      height: 40,
      borderColor: "#ccc",
      borderWidth: 1,
      marginLeft: 1,
      marginRight: 1,
      backgroundColor: "white",
      borderRadius: 10,
      fontFamily: "EMprint-Bold",
      fontSize: 25,
      paddingTop: 0,
      paddingBottom: 0
    };

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }
    var styleText = {
      color: "#000",
      fontSize: 20,
      padding: 20,
      paddingBottom: 0,
      fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Regular"
    };
    var mensaje_socio = (<View></View>);
    if(global.user.tipo=='lubricentro'){
	    var nombre_socio = 'Socio actual : '+global.socio.nombre+' '+global.socio.apellido_pat;
	    mensaje_socio = (<View> 
	    
	    	<Text style={styleText}> 
            	<Text>{nombre_socio}</Text>
            </Text>
 
          </View>)
    }
    
    
    return (
      <View
        style={{ flex: 1, alignItems: "center", backgroundColor: global.backgroundColor }}
      >
        <Navbar
          onBack={() => {
            this.props.navigation.pop();
          }}
        />
        <Header title={"INGRESE EL CODIGO"} type={"normal"} size={"normal"} />
        {mensaje_socio}
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <View
            style={{
              flexDirection: "row",
              width: width,
              height: inputSize,
              backgroundColor: "transparent",
              justifyContent: "center"
            }}
          >
            <TextInput
              underlineColorAndroid={"rgba(0, 0, 0, 0)"}
              value={this.state.c1}
              ref={c => (this.txtField1 = c)}
              returnKeyType="done"
              textAlign={"center"}
              maxLength={1}
              style={styleBox}
              onChangeText={c1 => {
                this.setState({ c1 });
                this.onChangeText(c1, 2);
              }}
              onKeyPress={ this.handleKeyPress }
            />

            <TextInput
              underlineColorAndroid={"rgba(0, 0, 0, 0)"}
              value={this.state.c2}
              ref={c => (this.txtField2 = c)}
              returnKeyType="done"
              textAlign={"center"}
              maxLength={1}
              style={styleBox}
              onChangeText={c2 => {
                this.setState({ c2 });
                this.onChangeText(c2, 3);
              }}
              onKeyPress={ this.handleKeyPress }
            />

            <TextInput
              underlineColorAndroid={"rgba(0, 0, 0, 0)"}
              value={this.state.c3}
              ref={c => (this.txtField3 = c)}
              returnKeyType="done"
              textAlign={"center"}
              maxLength={1}
              style={styleBox}
              onChangeText={c3 => {
                this.setState({ c3 });
                this.onChangeText(c3, 4);
              }}
              onKeyPress={ this.handleKeyPress }
            />

            <TextInput
              underlineColorAndroid={"rgba(0, 0, 0, 0)"}
              value={this.state.c4}
              ref={c => (this.txtField4 = c)}
              returnKeyType="done"
              textAlign={"center"}
              maxLength={1}
              style={styleBox}
              onChangeText={c4 => {
                this.setState({ c4 });
                this.onChangeText(c4, 5);
              }}
              onKeyPress={ this.handleKeyPress }
            />

            <TextInput
              underlineColorAndroid={"rgba(0, 0, 0, 0)"}
              value={this.state.c5}
              ref={c => (this.txtField5 = c)}
              returnKeyType="done"
              textAlign={"center"}
              maxLength={1}
              style={styleBox}
              onChangeText={c5 => {
                this.setState({ c5 });
                this.onChangeText(c5, 6);
              }}
              onKeyPress={ this.handleKeyPress }
            />

            <TextInput
              underlineColorAndroid={"rgba(0, 0, 0, 0)"}
              value={this.state.c6}
              ref={c => (this.txtField6 = c)}
              returnKeyType="done"
              textAlign={"center"}
              maxLength={1}
              style={styleBox}
              onChangeText={c6 => {
                this.setState({ c6 });
                this.onChangeText(c6, 7);
              }}
              onKeyPress={ this.handleKeyPress }
            />
            
            <TouchableOpacity onPress={()=>{
	            this.setState({
	              c1: "",
	              c2: "",
	              c3: "",
	              c4: "",
	              c5: "",
	              c6: ""
	            });
	            this.txtField1.focus();
            }} 
            style={{backgroundColor:'trasnparent',marginTop:3,width:30,height:30,justifyContent:'center',alignItems:'center'}}>
            	<Icon name={'trash'} size={30} color={'black'} /> 
            </TouchableOpacity>
            
          </View>
        </View>
        <View style={{ width: "100%" ,marginBottom:10}}>
          
          <Button
            type="yellow"
            title="CONTINUAR"
            onPress={() => {
              this.validarCodigo();
            }}
          />
        </View>
        <IphoneXFix />
        {loading}
      </View>
    );
  }
  componentWillMount() {
    setTimeout(() => {
      this.txtField1.focus();
    }, 500);
  }
  validarCodigo() {
    var { params } = this.props.navigation.state;
    if (params == undefined) {
      params = {};
      params.fromRegister = true;
    }
    var codigo =
      this.state.c1 +
      this.state.c2 +
      this.state.c3 +
      this.state.c4 +
      this.state.c5 +
      this.state.c6;
    if (codigo.length < 6) {
      Alert.alert(
        "Importante",
        "El codigo tiene 6 digitos.",
        [{ text: "Aceptar", onPress: () => {} }],
        { cancelable: false }
      );
      return;
    }
    this.setState({ loading: true });
    var request = {};
     request.codigo = codigo;
     request.app = 'socio';
	 request.socio = global.user.id;


    fetch(global.domain + "/api/validar", {
      method: "POST",
      body: JSON.stringify(request)
    })
      .then(response => response.json())
      .then(responseJson => {


        		this.setState({ loading: false });

        		var mensaje = responseJson.mensaje;
				
				var p={};
		  		if(responseJson.resultado == 'ERROR'){
			  			p.fromRegister = params.fromRegister;
			  			p.exito = false;
			  			p.mensaje = mensaje;
			  			if(params.fromRegister){
				  			this.props.navigation.navigate("ScanerGraciasRegistro",p);
			  			}else{
				  			this.props.navigation.navigate("ScanerGracias",p);
			  			}
		  		}else{
				       	global.user.activo = true;
				        global.saveUser();
			  			p.fromRegister = params.fromRegister;
			  			p.exito = true;
			  			p.mensaje = mensaje;
						this.props.navigation.navigate("ScanerGracias",p);
		  		}

      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }
}

export default Codigo;
