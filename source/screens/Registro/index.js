import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  Alert,
  ImageBackground,
  Platform,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from "react-native";
import NetInfo from "@react-native-community/netinfo";
//import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Navbar, Header, Input, Button, SvgCustom, IphoneXFix, BottomSpace } from "@components";
import Reactotron from "reactotron-react-native";
import { img } from "@media";
import PhotoUpload from 'react-native-photo-upload';
import LinearGradient from 'react-native-linear-gradient';
import Image from 'react-native-image-progress';
import Mask from "react-native-mask";

var { height, width } = Dimensions.get("window");

class Registro extends Component {
  constructor(props) {
    super(props);
    var { params } = this.props.navigation.state;
    if (params == undefined) {
      params = {};
      params.dni = "";
    }

    Reactotron.log(params);

    var emailEditable = true;
    var showPassword = true;
    var fbid = null;
    var googleid = null;
    if (global.user == null) {
      if (params.facebookData != null) {
        showPassword = false;
      }
      if (params.googleData != null) {
        if (params.googleData.user != null) {
          if (params.googleData.user.email != null) {
            if (params.googleData.user.email != "") {
              emailEditable = false;
              showPassword = false;

            }
          }
        }
      }
    } else {
      showPassword = false;

      if (global.user.googleid != null) {
        if (global.user.googleid != "") {
          emailEditable = false;
          showPassword = false;
          googleid = global.user.googleid;
        }
      }
      if (global.user.fbid != null) {
        if (global.user.fbid != "") {
          showPassword = false;
          fbid = global.user.fbid;
        }
      }
    }

    Reactotron.log(global.user);

    if (global.user != null) {
      this.state = {
        loading: false,
        disableAdd: false,
        ruc: global.user.ruc,
        nombres: (global.user.nombres != null) ? global.user.nombres : "",
        apellidos: (global.user.apellidos != null) ? global.user.apellidos : "",
        celular: (global.user.celular != null) ? global.user.celular : "",
        email: (global.user.email != null) ? global.user.email : "",
        password: "",
        password_repeat: "",
        terminos: true,
        imagen: (global.user.imagen != null) ? global.user.imagen : "",
        emailEditable: emailEditable,
        showPassword: showPassword,
        fbid: fbid,
        googleid: googleid,
      };
    } else {

      if (params.facebookData != null) {
        this.state = {
          loading: false,
          disableAdd: false,
          nombres: params.facebookData.first_name,
          apellidos: params.facebookData.last_name,
          ruc: "",
          password: "",
          password_repeat: "",
          celular: "",
          email: "",
          terminos: false,
          imagen: params.facebookData.picture.data.url,
          fbid: params.facebookData.id,
          emailEditable: emailEditable,
          showPassword: showPassword,

        };
      } else if (params.googleData != null) {
        this.state = {
          loading: false,
          disableAdd: false,
          nombres: params.googleData.user.givenName,
          apellidos: params.googleData.user.familyName,
          ruc: "",
          password: "",
          password_repeat: "",
          celular: "",
          email: params.googleData.user.email,/*params.googleData.user.email,*/
          terminos: false,
          imagen: "http://mobilcontigo.pe/assets/ico_user.png",
          googleid: params.googleData.user.id,
          emailEditable: emailEditable,
          showPassword: showPassword
        };
      } else {
        this.state = {
          loading: false,
          disableAdd: false,
          enableRegister: false,
          nombres: "",
          apellidos: "",
          ruc: "",
          razon_social: "",
          password: "",
          password_repeat: "",
          celular: "",
          email: "",
          terminos: false,
          imagen: "http://mobilcontigo.pe/assets/ico_user.png",
          register_error: "",
          emailEditable: emailEditable,
          showPassword: showPassword
        };
      }

    }


  }
  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
  pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }
  handleDatePicked = date => {
    Reactotron.log("A date has been picked: ");
    Reactotron.log(date);
    var dia = date.getDate(); //Current Date.
    var mes = date.getMonth() + 1; //Current Month.
    var anio = date.getFullYear(); //Current Year.
    var fecha = anio + '-' + this.pad(mes, 2) + '-' + this.pad(dia, 2);
    this.setState({ cumpleanios: fecha })
    this.hideDateTimePicker();
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  msg(msg) {
    Alert.alert("Importante", msg, [{ text: "Aceptar", onPress: () => { } }], {
      cancelable: false
    });
  }
  updateAvatar(avatar) {
    this.setState({ loading: true });
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {

        fetch(global.domain + "/api/upload_avatar", {
          method: 'POST',
          body: JSON.stringify({ avatar: avatar, id: global.user.id })
        }).then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.status == "OK") {
              this.setState({ loading: false });
            }
          })
          .catch((error) => {
            Reactotron.log(error);
            Alert.alert('Mensaje', 'Error al actualizar la imagen de usuario, intente nuevamente.',
              [{ text: 'Aceptar', onPress: () => { } }], { cancelable: false });
            this.setState({ loading: false });
          });
      }
    });
  }
  saveInfo() {

    var params = {};
    if (global.user != null) {
      params.id = global.user.id;
    }
    params.ruc = this.state.ruc;
    params.nombres = this.state.nombres;
    params.apellidos = this.state.apellidos;
    params.razon_social = this.state.razon_social;
    params.celular = this.state.celular;
    params.email = String(this.state.email).toLowerCase();

    if (global.user == null) {
      params.notification_push_id = global.notification_push_id;
      params.notification_user_id = global.notification_user_id;

    }

    params.os = Platform.OS;
    params.imagen = this.state.imagen;
    if (this.state.googleid != null) {
      params.googleid = this.state.googleid;
    }
    if (this.state.fbid != null) {
      params.fbid = this.state.fbid;
    }

    if (params.ruc == "") {
      this.msg("El número de RUC ó DNI es requerido.");
      return;
    }

    if (params.ruc.length != 8 && params.ruc.length != 11) {
      this.msg("El número de RUC ó DNI es invalido.");
      return;
    }

    if (params.nombres == "") {
      this.msg("Debe ingresar su nombre.");
      return;
    }
    if (params.celular == "") {
      this.msg("Debe ingresar un número de celular.");
      return;
    }
    if (params.celular.length < 9) {
      this.msg("Debe ingresar un número de celular válido.");
      return;
    }



    if (params.celular != "") {

      var isnum = /^\d+$/.test(params.celular);
      if (!isnum) {
        this.msg("El celular debe contener solo números.");
        return;
      }
    }

    if (this.state.showPassword) {
      if (this.state.password == "") {
        this.msg("La contraseña es requerida.");
        return;
      }
      if (this.state.password.length < 8) {
        this.msg("La contraseña debe ser igual o mayor a 8 caracteres.");
        return;
      }
      if (this.state.password != this.state.password_repeat) {
        this.msg("Las contraseñas no coinciden.");
        return;
      }
      params.password = this.state.password;
    }
    if (!this.state.terminos) {
      this.msg("Debe aceptar los términos y condiciones.");
      return;
    }
    var url = global.domain + "/api/registrar";
    var editar = false;
    var user = {};
    if (global.user != null) {
      user.id = global.user.id;
      user.activo = true;
      url = global.domain + "/api/editar";
      editar = true;
    }

    user.ruc = params.ruc;
    user.nombres = params.nombres;
    user.apellidos = params.apellidos;
    user.celular = params.celular;
    user.email = params.email;

    user.os = Platform.OS;
    user.imagen = params.imagen;

    if (this.state.googleid != null) {
      user.googleid = this.state.googleid;
    }
    if (this.state.fbid != null) {
      user.fbid = this.state.fbid;
    }

    if (this.state.showPassword) {
      user.password = params.password;
    }




    this.setState({ loading: true });

    Reactotron.log(params);
    if (this.state.netStatus) {
      fetch(url, { method: "POST", body: JSON.stringify(params) })
        .then(response => response.json())
        .then(responseJson => {
          Reactotron.log(responseJson);
          this.setState({ loading: false });

          if (responseJson.resultado == "OK") {
            if (editar) {
              global.user = user;
              global.saveUser();
              this.msg("Datos actualizados correctamente.");
            } else {
              user.id = responseJson.id;
              global.user = user;
              global.saveUser();
              this.props.navigation.navigate("Bienvenido");
            }
          } else {
            this.msg(responseJson.mensaje);
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({ loading: false });
        });
    } else {
      Alert.alert(
        "Error",
        "Sin conexión a internet.",
        [{ text: "Aceptar", onPress: () => { } }],
        { cancelable: false }
      );

      this.setState({ loading: false });
    }




  }
  closeSession() {
    Alert.alert("Mensaje", "¿Estas seguro que quieres salir? ", [{
      text: "Si",
      onPress: () => {
        global.deleteUser();
        this.props.navigation.navigate('Auth');
      }
    }, { text: "No", onPress: () => { } }], { cancelable: false });
  }
  componentDidMount() {
    NetInfo.isConnected.addEventListener("change", this.handleConnectionChange);
    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({ netStatus: isConnected });
    });
  }
  handleConnectionChange = isConnected => {
    this.setState({ netStatus: isConnected });
    Reactotron.log(`is connected: ${this.state.netStatus}`);
  };
  checkRUC(ruc) {
    var url = global.domain + "/api/check_ruc/" + ruc;
    fetch(url, { method: "POST", body: JSON.stringify({}) })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == "ERROR") {
          this.setState({ enableRegister: false, razon_social: "", register_error: responseJson.mensaje });
        }
        if (responseJson.status == "OK") {
          this.setState({ enableRegister: true, razon_social: responseJson.razon_social, register_error: "" });
        }

      }).catch(error => {
        console.error(error);
      });
  }
  render() {
    var { params } = this.props.navigation.state;
    if (params == undefined) {
      params = {};
    }
    var inputStyle = {
      paddingLeft: 10,
      fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Regular",
      color: "black",
      flex: 1,
      height: 40,
      borderColor: "gray",
      borderWidth: 0,
      backgroundColor: global.backgroundColor,
      fontSize: 16
    };
    var labelStyle = {
      paddingRight: 5,
      textAlign: "right",
      color: "white",
      fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Regular",
      fontSize: 15
    };
    var containerStyle = {
      borderColor: "#000",
      borderBottomWidth: 2,
      justifyContent: "center"
    };

    var navbar;
    if (params.isRegister) {
      btn = (
        <View style={{ width: "100%", alignItems: "center" }}>
          <Button
            disabled={!this.state.enableRegister}
            mb={true}
            title="Regístrate"
            onPress={() => {
              this.saveInfo();
            }}
          />


        </View>
      );

      navbar = (
        <Navbar
          onBack={() => {
            this.props.navigation.pop();
          }}
        />
      );
    } else {
      btn = (
        <View style={{ width: '100%', alignItems: "center", backgroundColor: 'transparent' }}>
          <Button
            title="Actualizar"
            onPress={() => {
              this.saveInfo();
            }}
          />
          <TouchableOpacity style={{ width: 140, height: 30, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }} onPress={() => {
            this.closeSession();
          }}>
            <Text style={{ fontFamily: global.fontBold, color: 'gray', fontSize: 18, marginTop: 0 }}>
              {'Cerrar sesión'}
            </Text>
          </TouchableOpacity>
        </View>
      );
      navbar = <View />;
    }

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var ico_1;
    var ico_2;
    var ico_3;
    var ico_4;

    var userCircleSize = (new IphoneXFix().isIphoneX()) ? 120 : 80;

    if (params.isRegister) {
      ico_1 = (<SvgCustom style={{ margin: 0, padding: 0, width: 20, backgroundColor: "transparent" }} width="20" height="20" source={"ico_ruc_black"} />);
      ico_2 = (<SvgCustom style={{ margin: 0, padding: 0, width: 20, backgroundColor: "transparent" }} width="20" height="20" source={"ico_user_black"} />);
      ico_3 = (<SvgCustom style={{ margin: 0, padding: 0, width: 20, backgroundColor: "transparent" }} width="20" height="20" source={"ico_email_black"} />);
      ico_4 = (<SvgCustom style={{ margin: 0, padding: 0, width: 20, backgroundColor: "transparent" }} width="23" height="23" source={"call_outline_black"} />);
    } else {
      ico_1 = (<SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_ruc"} />);
      ico_2 = (<SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_user"} />);
      ico_3 = (<SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_email"} />);
      ico_4 = (<SvgCustom style={{ margin: 0, padding: 0 }} width="23" height="23" source={"ico_call_outline"} />);
    }

    /*, width: width, height: height - 20,*/
    let mainContent = (<View style={{ flex: 1, width: "100%" }}>
      <View style={{
        flex: 1, backgroundColor: "transparent",
        justifyContent: (params.isRegister) ? "center" : "center", alignItems: 'center'
        , marginTop: (params.isRegister) ? 0 : 20
      }}>

        <View style={{ alignItems: "center", backgroundColor: "transparent", justifyContent: "center", marginTop: (params.isRegister) ? 0 : 20, marginBottom: '3%', height: userCircleSize }}>


          <PhotoUpload imagePickerProps={{
            cancelButtonTitle: "Cancelar", takePhotoButtonTitle: 'Tomar una foto…', chooseFromLibraryButtonTitle: 'Escoger de la biblioteca', permissionDenied: {
              title: 'Permiso denegado',
              text: 'Para poder tomar fotos con su cámara y elegir imágenes de su biblioteca.',
              reTryTitle: 'Reintentar',
              okTitle: 'Aceptar',
            }
          }} photoPickerTitle="Seleccione una foto" onPhotoSelect={avatar => {
            if (avatar) {
              this.setState({ imagen: 'data:image/gif;base64,' + avatar });
              if (!params.isRegister) {
                this.updateAvatar(avatar);
                global.user.imagen = 'data:image/gif;base64,' + avatar;
                global.saveUser();
              }
            }
          }}
            source={{ uri: this.state.imagen }}>
            <Mask shape={'circle'}>
              <View style={{ borderRadius: (userCircleSize / 2), width: userCircleSize, height: userCircleSize, backgroundColor: '#FFF' }}>
                <Image indicator={ActivityIndicator} style={{ borderRadius: (userCircleSize / 2), width: userCircleSize, height: userCircleSize }} source={{
                  uri: this.state.imagen
                }} />
              </View>
            </Mask>

            <SvgCustom
              return style={{ position: "absolute", bottom: 0, right: 0 }} width="35" height="35" source={"ico_pen"} />


          </PhotoUpload>
        </View>




        <Input placeholder={'RUC ó DNI *'}
          value={this.state.ruc}
          maxLength={11}
          onChange={ruc => {
            if (ruc.length >= 8) {
              this.checkRUC(ruc);
            } else {
              this.setState({ enableRegister: false, razon_social: "" });
            }
            this.setState({ ruc, register_error: "" });
          }} leftIcon={ico_1} />


        {(this.state.razon_social != "" && params.isRegister) &&
          <View style={{ backgroundColor: global.gris, padding: 4, width: "70%", borderRadius: 5, alignItems: "center", marginTop: 2, marginBottom: 2 }}>
            <Text style={{ fontSize: 13, color: "white", fontWeight: "bold", fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Bold" }}>{this.state.razon_social}</Text>
          </View>
        }

        {(this.state.register_error != "" && params.isRegister) &&
          <View style={{ backgroundColor: "red", padding: 4, width: "70%", borderRadius: 5, alignItems: "center", marginTop: 2, marginBottom: 2 }}>
            <Text style={{ fontSize: 13, color: "white", fontWeight: "bold", fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Bold" }}>{this.state.register_error}</Text>
          </View>
        }



        <Input placeholder={'Nombres *'}
          value={this.state.nombres}
          maxLength={200}
          onChange={nombres => {
            this.setState({ nombres });
          }}
          leftIcon={ico_2}
        />
        <Input placeholder={'Apellidos *'}
          value={this.state.apellidos}
          maxLength={200}
          onChange={apellidos => {
            this.setState({ apellidos });
          }}
          leftIcon={ico_2} />

        <Input placeholder={'Email'}
          disabled={!this.state.emailEditable}
          value={this.state.email}
          maxLength={100}
          onChange={email => {
            this.setState({ email });
          }} leftIcon={ico_3} />

        <Input placeholder={'Celular *'}
          value={this.state.celular}
          maxLength={10}
          onChange={celular => {
            this.setState({ celular });
          }} leftIcon={ico_4} />


        {(this.state.showPassword && params.isRegister) &&
          <Input placeholder={'Contraseña *'}
            password={true}
            password_style={true}
            value={this.state.password}
            maxLength={50}
            onChange={password => {
              this.setState({ password });
            }} leftIcon={
              <SvgCustom
                return style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_candado_black"} />
            } />
        }
        {(this.state.showPassword && params.isRegister) &&
          <Input placeholder={'Repetir Contraseña *'}
            password={true}
            password_style={true}
            value={this.state.password_repeat}
            maxLength={50}
            onChange={password_repeat => {
              this.setState({ password_repeat });
            }} leftIcon={
              <SvgCustom
                return style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_candado_black"} />
            } />}

        {params.isRegister &&
          <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: "center", marginTop: 20, marginBottom: 10 }}>

            <TouchableOpacity onPress={() => {
              this.setState({ terminos: true });
            }} style={{ justifyContent: 'center', alignItems: "center" }}>
              <SvgCustom
                return style={{}} width="23" height="23" source={"circle"} />
              {this.state.terminos &&
                <SvgCustom
                  return style={{ position: "absolute" }} width="14" height="14" source={"check"} />
              }
            </TouchableOpacity>
            <Text style={{ color: "white" }}>{' Acepto los '}</Text>
            <TouchableOpacity onPress={() => {
              this.props.navigation.navigate("Terminos", { tipo: "terminos" });
            }} style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ color: "white", fontWeight: "bold" }}>{'Términos y condiciones.'}</Text>
              <View style={{ width: "100%", height: 1, backgroundColor: "white" }}></View>
            </TouchableOpacity>

          </View>
        }
        {btn}
        {params.isRegister &&
          <View style={{ height: 40, width: 15, backgroundColor: "tranparent" }}></View>
        }
      </View>
      {
        !params.isRegister &&
        <BottomSpace></BottomSpace>
      }

    </View >)

    if (params.isRegister) {
      return (
        <ImageBackground source={img.bg_camion} style={{ flex: 1, width: "100%", backgroundColor: 'rgba(46,80,111,1)' }}>
          <KeyboardAwareScrollView bounces={false} style={{
            flex: 1, backgroundColor: 'transparent'
          }}>
            <View style={{ flex: 1 }}>
              <Header colorArrow={"white"} onBack={() => {
                this.props.navigation.pop();

              }} />
              {mainContent}
            </View>
          </KeyboardAwareScrollView>
          {loading}
        </ImageBackground >
      );
    } else {

      return (
        <LinearGradient colors={global.tabGradientColors} style={{ flex: 1 }}>
          <KeyboardAwareScrollView bounces={false} style={{
            flex: 1, backgroundColor: 'transparent'
          }}>
            <View style={{ flex: 1 }}>
              <Header title={"Tus datos personales"} styleTitle={{ fontSize: 24 }} type={"azul_oscuro"} marginLeft={30} onAyuda={() => {
                this.props.navigation.navigate("Ayuda");
              }} />
              {mainContent}
            </View>
          </KeyboardAwareScrollView>
          {loading}
        </LinearGradient >
      );
    }

  }
}

export default Registro;
