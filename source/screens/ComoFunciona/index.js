import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  FlatList
} from "react-native";
import { img } from "@media";
import Icon from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Reactotron from "reactotron-react-native";
import { BottomSpace, Header, Navbar } from "@components";
import { ProgressBar } from 'react-native-paper';

var counter = 0;
class ComoFunciona extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }
  render() {
    var loading;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }
    var textStyle = { fontFamily: global.fontSemiBold, color: global.azul_oscuro, textAlign: "justify" }
    var styleCircle = {};
    var grayLine = { width: '90%', height: 1, backgroundColor: "#eee", marginTop: 20, marginBottom: 10 }
    return (
      <View style={{ flex: 1, backgroundColor: '#eee' }}>


        <ScrollView style={{ flex: 1, width: '100%', backgroundColor: global.azul_oscuro }} contentContainerStyle={{ paddingTop: 0 }}>

          <View style={{ flex: 1, alignItems: "center", backgroundColor: 'white' }}>

            <View style={{ width: '100%', backgroundColor: global.azul_oscuro }}>
              <Header styleTitle={{ marginTop: 10 }} colorArrow={"white"} onBack={() => {
                this.props.navigation.goBack();
              }} title={"¿Cómo gano más KMS?"} type={"blanco"} marginLeft={30} style={{ marginTop: 0 }} />
            </View>

            <View style={{
              width: '100%', borderBottomLeftRadius: 30, borderBottomRightRadius: 30, padding: 30, paddingBottom: 0,
              justifyContent: "center", backgroundColor: global.azul_oscuro, paddingTop: 10
            }}>
              <View style={{ flexDirection: "row", justifyContent: "space-around", alignItems: "center" }}>
                <View style={{ marginTop: 20, flexDirection: 'row', width: '100%', backgroundColor: 'transparent' }}>


                  <View style={{
                    width: 25,
                    height: 25,
                    borderRadius: 25 / 2,
                    marginTop: 5,
                    backgroundColor: '#ccc',
                    justifyContent: "center"
                  }}>
                    <Text style={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 15, textAlign: "center" }}>{1}</Text>
                  </View>

                  {!global.user.meta &&
                    <Text style={{ fontFamily: global.fontSemiBold, color: global.blanco, marginTop: 0, marginLeft: 10 }}>{'Adquiere un mínimo de 130 galones en el trimestre para participar.'}</Text>
                  }
                  {global.user.meta &&
                    <Text style={{ fontFamily: global.fontSemiBold, color: global.blanco, marginTop: 8, marginLeft: 10 }}>{'Solo adquiriendo productos participantes Mobil'}</Text>
                  }



                </View>
              </View>

              <View style={{ width: 30, height: 30, backgroundColor: "transparent" }} />
            </View>

            <View style={{ width: '100%', backgroundColor: "transparent", marginTop: 5, marginBottom: 20, paddingLeft: 30, paddingRight: 30 }} >
              <View style={{ width: '100%', flexDirection: "row", marginTop: 20, backgroundColor: 'transparent' }}>


                <View style={{
                  width: 25,
                  height: 25,
                  borderRadius: 25 / 2,
                  marginTop: 5,
                  backgroundColor: '#ccc',
                  justifyContent: "center"
                }}>
                  <Text style={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 15, textAlign: "center" }}>{2}</Text>
                </View>
                <Text style={[textStyle, { marginTop: 0, marginLeft: 10, backgroundColor: "white", marginRight: 20 }]}>{'Compra cualquier producto que esté dentro de estas categorías. Cada una te brinda una cantidad distinta de KMS'}</Text>

              </View>


              <Product image={img.producto_lista_1} onPress={() => {

                if (global.user.activo) {
                  this.props.navigation.navigate("ComoFuncionaListado", { tipo: "SINTETICO" });
                } else {
                  this.props.navigation.navigate("ComoFuncionaListadoExterna", { tipo: "SINTETICO" });
                }


              }} titulo="Sintético" texto1="2.5 KMS" texto2="x1 GALÓN" widthCircle={80} />
              <Product image={img.producto_lista_2} onPress={() => {

                if (global.user.activo) {
                  this.props.navigation.navigate("ComoFuncionaListado", { tipo: "SEMI_SINTETICO" });
                } else {
                  this.props.navigation.navigate("ComoFuncionaListadoExterna", { tipo: "SEMI_SINTETICO" });
                }


              }} titulo={"Semi\nsintético"} texto1="1.5 KMS" texto2="x1 GALÓN" widthCircle={80} />
              <Product image={img.producto_lista_3} onPress={() => {

                if (global.user.activo) {
                  this.props.navigation.navigate("ComoFuncionaListado", { tipo: "MINERAL_PREMIUM" });
                } else {
                  this.props.navigation.navigate("ComoFuncionaListadoExterna", { tipo: "MINERAL_PREMIUM" });
                }

              }} titulo={"Mineral\npremium"} texto1="0.75 KMS" texto2="x1 GALÓN" widthCircle={80} />
              <Product image={img.producto_lista_4} onPress={() => {
                if (global.user.activo) {
                  this.props.navigation.navigate("ComoFuncionaListado", { tipo: "MINERAL_ESTANDAR" });
                } else {
                  this.props.navigation.navigate("ComoFuncionaListadoExterna", { tipo: "MINERAL_ESTANDAR" });
                }

              }} titulo={"Mineral\nestándar"} texto1="0.5 KMS" texto2="x1 GALÓN" widthCircle={80} />


              <Text style={[textStyle, { marginTop: 20, color: global.rojo }]}>{'Los KMS se agregarán a los 7 días de generado el pedido.'}</Text>


              <View style={grayLine}></View>

              <Text style={[textStyle, { marginTop: 10, color: global.azul_oscuro, fontSize: 20, fontFamily: global.fontBold }]}>{'Considera que ...'}</Text>



              <Text style={[textStyle, { marginTop: 10 }]}>{'1. Los KMS que acumules son Congelados hasta completar bloques de 100 KMS. Una vez completado, pasarán a estar Disponibles.'}</Text>


              <Medidor style={{ backgroundColor: '#ccc' }} textStyle={{ color: global.azul }}
                titulo="Congelados" descripcion="Completa 100 KMS para poder canjear" valor="80/100" percent={0.8} />
              <Medidor style={{ backgroundColor: global.azul_claro }} textStyle={{ color: 'white' }}
                type="red" titulo="Ganaste 100 KMS más" descripcion="¡Felicidades! Los completaste el 15.04.20" valor="" percent={1} />


              <Text style={[textStyle, { marginTop: 20, color: global.azul_oscuro }]}>{'2. Los KMS que no lleguen a completar el bloque de 100 KMS, no se acumulan para el siguiente trimestre.'}</Text>

              <View style={grayLine}></View>
              
              
              <Text style={[textStyle, { marginTop: 10, color: global.azul_oscuro }]}>
	              <Text>{'3. Los KMS Disponibles que acumules podrán ser canjeados hasta su fecha de caducidad señalada en la parte inferior del cuadro que puedes obsevar en tu sección '}</Text>
	              <Text style={{fontFamily: global.fontBold}}>{'Compras'}</Text>
              </Text>

              <View style={{ width: '100%', padding: 20, marginTop: 20, borderRadius: 10, backgroundColor: global.azul_oscuro, justifyContent: "center", alignItems: "center" }}>


                <Text style={[textStyle, { marginTop: 0, color: global.azul_claro, fontWeight: 'bold', fontSize: 20 }]}>
                  3ER TRIMESTRE 2020</Text>
                <Text style={[textStyle, { marginTop: 0, color: global.blanco, fontWeight: 'bold', fontSize: 14 }]}>
                  01/07 al 30/09
                </Text>


                <Text style={{
                  fontFamily: global.fontBold, color: global.blanco, fontSize: 100, lineHeight: 100, backgroundColor: 'transparent'
                  , paddingBottom: 0, marginBottom: 0, marginTop: 25, height: 85
                }}>{'200'}</Text>

                <Text style={[textStyle, { marginTop: 0, color: global.blanco, fontWeight: 'bold', fontSize: 16 }]}>
                  <Text style={{ color: '#B2BBCF' }}>{'KMS '}</Text>
                  {'disponibles'}</Text>

                <View style={{ width: "100%", justifyContent: "space-between", flexDirection: 'row', marginTop: 15 }}>
                  <View style={{ left: -15, top: -10, width: 110, height: 70, backgroundColor: "transparent", position: "absolute", borderRadius: 95 / 2, borderWidth: 5, borderColor: "red" }}></View>

                  <View>
                    <Text style={[textStyle, {
                      marginTop: 0, color: global.azul_claro,
                      fontSize: 12, fontWeight: '400'
                    }]}>
                      <Text style={{ color: "#B2BBCF", fontSize: 15, fontWeight: 'bold' }}>170 KMS</Text>{'\n'}Válidos hasta el{'\n'}15.12.20 </Text>
                  </View>
                  <View>
                    <View style={{ left: -15, top: -10, width: 110, height: 70, backgroundColor: "transparent", position: "absolute", borderRadius: 95 / 2, borderWidth: 5, borderColor: "red" }}></View>
                    <Text style={[textStyle, {
                      marginTop: 0, color: global.azul_claro,
                      fontSize: 12, fontWeight: '400'
                    }]}>
                      <Text style={{ color: "#B2BBCF", fontSize: 15, fontWeight: 'bold' }}>30 KMS</Text>{'\n'}Válidos hasta el{'\n'}30.06.21</Text>


                  </View>
                </View>
              </View>


              <Text style={[textStyle, { marginTop: 20, marginBottom: 30, color: global.azul_oscuro }]}>{'4. Último día del 2020 para realizar canjes: 15 de diciembre. En el 2021, podrás empezar a canjear a partir del 02 de enero.'}</Text>





            </View>


          </View>
        </ScrollView>
        {loading}
      </View >
    );

  }
}
/* */
class Medidor extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var contentStyle = { marginTop: 15, borderRadius: 10, backgroundColor: 'red', padding: 13 };
    var type = this.props.type;
    var textStyle = { fontFamily: global.fontBold, color: (type == 'white') ? global.azul_oscuro : '#FFF', fontSize: 14 }
    return (
      <View style={[contentStyle, this.props.style]}>
        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end" }}>
          <Text style={[textStyle, { fontSize: 16 }, this.props.textStyle]}>{this.props.titulo}</Text>
          <Text style={[textStyle, { fontSize: 18 }, this.props.textStyle, { color: '#FFF', fontSize: 22 }]} > {this.props.valor}</Text>
        </View>
        <Text style={[textStyle, { fontFamily: global.fontSemiBold, marginTop: 5, fontSize: 14 }, this.props.textStyle]}>{this.props.descripcion}</Text>

        <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", marginTop: 5 }}>
          <View style={{ flex: 1, backgroundColor: 'transparent' }}>
            <ProgressBar progress={this.props.percent} color={(type == 'white') ? global.azul_claro : '#FFF'} style={{ marginTop: 5, height: 6, borderRadius: 10 }} />
          </View>

          {(type == 'white') &&
            <View style={{ backgroundColor: 'transparent', paddingTop: 5 }}>
              <Image style={{ width: 17, height: 17 }} source={img.check_blue} />
            </View>
          }

        </View>

      </View>
    );
  }
}
class Product extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var contentStyle = { borderStyle: 'solid', borderWidth: 1, borderColor: '#CCC', marginTop: 15, borderRadius: 10, backgroundColor: '#fff', padding: 7, flexDirection: "row", justifyContent: "center", alignItems: "center" };
    var type = this.props.type;
    var textStyle = { marginLeft: 10, fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 14 }
    return (
      <TouchableOpacity onPress={() => {
        this.props.onPress();
      }} style={[contentStyle, this.props.style]}>
        <Image height style={{
          marginTop: 0, width: 55, height: 65, resizeMode: 'cover',
        }} source={this.props.image} />
        <View style={{ flex: 1, justifyContent: "space-between", alignItems: "flex-start", justifyContent: "center" }}>
          <Text style={[textStyle, { fontSize: 15, marginLeft: 20 }, this.props.textStyle]}>{this.props.titulo}</Text>
        </View>
        <View style={{ width: this.props.widthCircle, height: 60, backgroundColor: 'transparent' }}>
          <View style={{
            width: this.props.widthCircle,
            height: 60,
            borderRadius: 60 / 2,
            backgroundColor: 'rgba(137,167,212,1)',
            justifyContent: "center"
          }}>
            <Text style={{ color: global.azul_oscuro, fontFamily: global.fontRegular, fontWeight: 'bold', fontSize: 14, lineHeight: 14, textAlign: "center" }}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.props.texto1 + '\n'}</Text>
              <Text style={{ fontSize: 12 }}>{this.props.texto2}</Text>
            </Text>
          </View>
        </View>
        <View style={{ width: 25, height: 25, backgroundColor: 'transparent', justifyContent: "center", alignItems: "center" }}>
          <EvilIcons name={'chevron-right'} size={35} color={global.azul_oscuro} />
        </View>

      </TouchableOpacity >
    );
  }
}
class Medidor2 extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var type = this.props.type;
    return (
      <View style={{ marginTop: 15, marginLeft: 20, borderRadius: 10, backgroundColor: (type == 'gris') ? '#ccc' : global.azul_claro, padding: 13 }}>
        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "flex-start" }}>
          <View>
            <Text style={{ fontFamily: global.fontBold, color: (type == 'gris') ? global.azul_oscuro : '#FFF', fontSize: 18 }}>{'Congelados'}</Text>
            <Text style={{ fontFamily: global.fontSemiBold, color: (type == 'gris') ? global.azul_oscuro : global.azul_oscuro, fontSize: 14 }}>{'Completa 100 KMS para canjear'}</Text>
          </View>

          <Text style={{ marginLeft: 10, fontFamily: global.fontBold, color: (type == 'gris') ? '#FFF' : '#FFF', fontSize: 25 }}>{'80/100'}</Text>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", marginTop: 5 }}>
          <View style={{ flex: 1, backgroundColor: 'transparent' }}>
            <ProgressBar progress={0.6} color={(type == 'gris') ? '#FFF' : '#FFF'} style={{ marginTop: 5, height: 7, borderRadius: 10 }} />
          </View>
        </View>
      </View >
    );
  }
}


const styles = StyleSheet.create({});

export default ComoFunciona;




