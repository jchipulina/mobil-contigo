import React, { Component } from "react";
import {
  ActivityIndicator,
  Text,
  Dimensions,
  TouchableOpacity,
  Platform,
  Linking,
  View
} from "react-native";
import QRCode from 'react-native-qrcode-svg';
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, Header, Button, IphoneXFix } from "@components";
import Reactotron from "reactotron-react-native";
import LinearGradient from 'react-native-linear-gradient';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AwesomeAlert from 'react-native-awesome-alerts';
import Image from 'react-native-image-progress';
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import { ScrollView } from "react-native-gesture-handler";
class PromocionesItem extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
  }
  static getDerivedStateFromProps(props, state) {

  }
  render() {
    const { beneficio } = this.props.navigation.state.params;
    Reactotron.log(beneficio);
    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View style={{ width: "100%", height: "100%", position: "absolute", justifyContent: "center", alignItems: "center", backgroundColor: "rgba(0,0,0,0.5)" }}>
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }
    return (
      <ScrollView style={{ flex: 1, backgroundColor: global.backgroundColor }}>
        <View style={{ width: '100%', height: 45, alignItems: 'center', backgroundColor: 'transparent', position: "absolute", top: (new IphoneXFix().isIphoneX()) ? 40 : 20, zIndex: 10 }}>
          <TouchableOpacity style={{ width: '100%', height: 45, flexDirection: 'row', alignItems: 'center' }}
            onPress={() => { this.props.navigation.goBack(); }}>
            <EvilIcons name={'chevron-left'} size={50} color={global.azul} style={{ backgroundColor: 'transparent', width: 40 }} />
            <Text style={{ fontFamily: global.fontSemiBold, color: global.azul_oscuro, fontSize: 12 }}>{'PROMOCIONES'}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <Image indicator={ActivityIndicator} resizeMode="cover" style={{ width: '100%', height: (height / 2.3), flex: 1, marginTop: 0 }}
              source={{ uri: global.urlimg + beneficio.imagen }} />
            <View style={{ width: '100%', height: '27%', backgroundColor: 'transparent', position: "absolute", left: 0, top: 0 }}>
              <LinearGradient colors={['rgba(255,255,255,1)', 'rgba(255,255,255,0.99)', 'rgba(255,255,255,0.8)', 'rgba(255,255,255,0)']} style={{ flex: 1 }} />
            </View>
          </View>
          <View style={{
            width: '100%', borderTopRightRadius: 40,
            backgroundColor: 'white', borderTopLeftRadius: 40, position: "relative", top: 0,
            padding: 30, paddingTop: 25, position: "relative", top: -40
          }}>
            <View style={{ flex: 1, justifyContent: "space-between" }}>
              <View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: "flex-end" }}>
                <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                  <Text style={{ fontFamily: global.fontBold, color: global.azul, fontSize: 22 }}>{beneficio.titulo}</Text>
                </View>
              </View>
              <Text style={{ fontFamily: global.fontSemiBold, alignContent: 'flex-start', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>
                {beneficio.descripcion_larga}
              </Text>
              <Text style={{ paddingTop: 10, fontFamily: global.fontRegular, alignContent: 'flex-start', fontWeight: 'bold', color: global.azul, fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{"Terminos y condiciones:"}</Text>
              <Text style={{ fontFamily: global.fontSemiBold, textAlign: "justify", alignContent: 'flex-start', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{beneficio.terminos}</Text>

            </View>




          </View>
        </View>





        {loading}



      </ScrollView >
    );
  }
}
export default PromocionesItem;
