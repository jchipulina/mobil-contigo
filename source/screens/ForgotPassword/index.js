import React, { Component } from "react";
import {
  View,
  Image,
  Alert,
  ImageBackground,
  Text,
  TextInput,
  Dimensions,
  Platform,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import NetInfo from "@react-native-community/netinfo";
import { Navbar, Header, Input, Button, IphoneXFix, BottomSpace, SvgCustom } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import Reactotron from "reactotron-react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { img } from "@media";
import Ionicons from "react-native-vector-icons/Ionicons";

var { height, width } = Dimensions.get("window");
var dniValues = ["", "", "", "", "", "", "", "", "", "", ""];
var timeOut;
class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    var { params } = this.props.navigation.state;
    this.state = {
      email: "",
      celular: "",
      loading: false,
      netStatus: false,
      paso: params.paso,
      c1: '',
      c2: '',
      c3: '',
      c4: '',
      userid: '',
      code_generated: '',
      password: '',
      password_repeat: '',
      password_cambiado: false,
    };
  }
  componentDidMount() {
    NetInfo.isConnected.addEventListener("change", this.handleConnectionChange);
    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({ netStatus: isConnected });
    });
  }
  handleConnectionChange = isConnected => {
    this.setState({ netStatus: isConnected });
    Reactotron.log(`is connected: ${this.state.netStatus}`);
  };
  componentWillMount() {

  }
  handleKeyPress({ nativeEvent: { key: keyValue } }) {
    if (keyValue === 'Backspace') {
      this.focusFieldByID(focus - 1);
      this.emptyFieldByID(focus);
    }
  }
  focusFieldByID(num) {
    focus = num;
    switch (num) {
      case 1:
        this.txtField1.focus();
        break;
      case 2:
        this.txtField2.focus();
        break;
      case 3:
        this.txtField3.focus();
        break;
      case 4:
        this.txtField4.focus();
        break;
    }
  }
  emptyFieldByID(num) {
    switch (num) {
      case 1:
        this.setState({ c1: '' });
        break;
      case 2:
        this.setState({ c2: '' });
        break;
      case 3:
        this.setState({ c3: '' });
        break;
      case 4:
        this.setState({ c4: '' });
        break;
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  onChangeText(stext, num) {

    clearTimeout(timeOut);
    timeOut = setTimeout(() => {
      Reactotron.log("setTimeout");
      if (this.state.c1 != '' && this.state.c2 != '' && this.state.c3 != '' && this.state.c4 != '') {
        this.setState({ loading: true })
        this.validarCodigo();
        return
      }
    }, 1000);




    if (num == 5) return;

    if (stext.length == 1) {
      this.focusFieldByID(num);
    }
  }
  componentWillFocus() {
    this.setState({
      c1: "",
      c2: "",
      c3: "",
      c4: ""
    });
  }
  render() {
    var inputSize = (width / 10) - 7;

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var styleBox = {
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      width: '70%',
      height: 40,
      borderColor: "#ccc",
      borderWidth: 1,
      marginLeft: 1,
      marginRight: 1,
      backgroundColor: "#FFF",
      borderRadius: 10,
      fontFamily: "EMprint-Bold",
      fontSize: 18,
      paddingTop: 0,
      paddingBottom: 0
    };
    //<SvgCustom return  style={{ margin: 0, padding: 0 }} width="20" height="20" source={img.ico_email_white} />
    //<SvgCustom return  style={{ margin: 0, padding: 0 }} width="20" height="20" source={img.call_outline_white} />




    var passwod_success = (<View></View>);
    if (this.state.password_cambiado) {
      passwod_success = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: global.azul
          }}
        >

          <View style={{ flex: 1, justifyContent: "center", alignItems: "center", width: '80%' }}>

            <SvgCustom style={{ margin: 10, padding: 0 }} width="100" height="100" source={"check_circle"} />


            <Text style={{ fontSize: 22, textAlign: "center", color: 'white', fontWeight: 'bold', marginBottom: 5, marginTop: 10 }}>{'Contraseña Cambiada correctamente'}</Text>
          </View>

          <Button
            style={{ marginTop: 0 }}
            title="Regresar a Iniciar sesión"
            onPress={() => {
              this.props.navigation.goBack();
            }}
          />
          <IphoneXFix></IphoneXFix>
          <View style={{ height: 20 }}></View>
        </View>
      );
    }

    var msg_codigo = 'Escribe el código que enviamos a tu ';
    if (this.state.email == '') {
      msg_codigo += 'celular'
    } else {
      msg_codigo += 'correo electrónico. También revisa tu bandeja de spam';
    }
    /*contentStyle={{ position: "absolute", zIndex: 150, top: (new IphoneXFix().isIphoneX()) ? 40 : 20 }}*/

    /*<KeyboardAwareScrollView bounces={false} style={{ flex: 1, width: '100%' }} innerRef={ref => { this.scroll = ref }}>*/
    /*</KeyboardAwareScrollView>*/
    return (

      <ImageBackground source={img.bg_camion} style={{ flex: 1, width: '100%', backgroundColor: 'rgba(46,80,111,1)' }}>
        <View style={{ flex: 1, backgroundColor: "transparent" }}>
          <Header colorArrow={"white"} onBack={() => {
            this.props.navigation.pop();

          }} />
          <View
            style={{ flex: 1, alignItems: "center" }}
          >
            <View
              style={{
                width: width,
                marginTop: (new IphoneXFix().isIphoneX()) ? 0 : 0,
                backgroundColor: "transparent",
                justifyContent: "center",
                alignItems: 'center'
              }}
            >
              {this.state.paso == 1 &&
                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 26, fontFamily: "EMprint", marginBottom: 25 }}>{'Recuperar contraseña'}</Text>
              }
              {this.state.paso == 2 &&
                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 26, fontFamily: "EMprint", marginBottom: 25 }}>{'Código enviado'}</Text>
              }
              {this.state.paso == 3 &&
                <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 26, fontFamily: "EMprint", marginBottom: 25 }}>{'Restablecer Contraseña'}</Text>
              }

              {this.state.paso == 1 &&
                <View style={{ width: "100%", alignItems: "center" }}>
                  <Text style={{ fontSize: 16, textAlign: "center", width: '70%', color: 'white', fontWeight: 'bold', marginBottom: 5 }}>{'Ingresa tu correo electrónico'}</Text>
                  <Input placeholder={'Email'}
                    invert={true}
                    value={this.state.email}
                    maxLength={100}
                    onChange={email => {
                      this.setState({ email, celular: "" });
                    }} leftIcon={
                      <Ionicons name="ios-mail" size={27} color={"#fff"}></Ionicons>
                    } />
                  <Text style={{ fontSize: 16, textAlign: "center", width: '70%', color: 'white', fontWeight: 'bold', marginBottom: 5, marginTop: 15 }}>{'o tu celular'}</Text>
                  <Input placeholder={'Celular'}
                    invert={true}
                    value={this.state.celular}
                    maxLength={9}
                    onChange={celular => {
                      this.setState({ celular, email: "" });
                    }} leftIcon={
                      <Ionicons name="ios-call" size={27} color={"#fff"}></Ionicons>
                    } />
                  <Text style={{ fontSize: 16, textAlign: "center", width: '90%', color: 'white', fontWeight: 'bold', marginBottom: 5, marginTop: 15 }}>{'y enviaremos instrucciones para cambiarla'}</Text>

                  <Button
                    style={{ marginTop: 30 }}
                    title="Enviar"
                    onPress={() => {
                      this.onEnviar();
                    }}
                  />
                </View>
              }
              {this.state.paso == 2 &&



                <View style={{ width: "100%", alignItems: "center" }}>
                  <Text style={{ fontSize: 17, textAlign: "center", width: '90%', color: 'white', fontWeight: 'bold', marginBottom: 10 }}>
                    {msg_codigo}
                  </Text>

                  <View style={{ backgroundColor: "transparent", width: "100%", flexDirection: "row", justifyContent: "center" }}>
                    <Input
                      style={{ width: 50, height: 50 }}
                      textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                      placeholder={''}
                      keyboardType='numeric'
                      invert={true}
                      ref={c => (this.txtField1 = c)}
                      value={this.state.c1}
                      maxLength={1}
                      onChange={c1 => {
                        this.setState({ c1 }, this.onChangeText(c1, 2));
                      }}
                    />
                    <Input
                      style={{ width: 50, height: 50 }}
                      textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                      placeholder={''}
                      keyboardType='numeric'
                      invert={true}
                      ref={c => (this.txtField2 = c)}
                      value={this.state.c2}
                      maxLength={1}
                      onChange={c2 => {
                        this.setState({ c2 }, this.onChangeText(c2, 3));

                      }}
                    />
                    <Input
                      style={{ width: 50, height: 50 }}
                      textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                      placeholder={''}
                      keyboardType='numeric'
                      invert={true}
                      ref={c => (this.txtField3 = c)}
                      value={this.state.c3}
                      maxLength={1}
                      onChange={c3 => {
                        this.setState({ c3 }, this.onChangeText(c3, 4));
                      }}
                    />
                    <Input
                      style={{ width: 50, height: 50 }}
                      keyboardType='numeric'
                      textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                      placeholder={''}
                      invert={true}
                      ref={c => (this.txtField4 = c)}
                      value={this.state.c4}
                      maxLength={1}
                      onChange={c4 => {
                        this.setState({ c4 }, this.onChangeText(c4, 5));
                      }}
                    />
                  </View>
                  <Text style={{ fontSize: 16, textAlign: "center", width: '90%', color: 'white', fontWeight: 'bold', marginBottom: 5, marginTop: 20 }}>{'No he recibido el código'}</Text>

                  <TouchableOpacity style={{ width: 115, marginBottom: 20, marginTop: 10 }} onPress={() => {
                    this.volverAEnviar();
                  }}>
                    <Text style={{ fontSize: 16, lineHeight: 16, textAlign: "center", width: '100%', color: 'white', fontWeight: 'bold', color: "red" }}>{'volver a enviar'}</Text>
                    <View style={{ width: "100%", height: 1, backgroundColor: global.rojo }}></View>
                  </TouchableOpacity>

                </View>
              }
              {this.state.paso == 3 &&
                <View style={{ width: "100%", alignItems: "center" }}>
                  <Text style={{ fontSize: 16, textAlign: "center", width: '90%', color: 'white', fontWeight: 'bold', marginBottom: 5 }}>{'Ingresa tu nueva contraseña'}</Text>
                  <Input placeholder={'Nueva Contraseña'}
                    invert={true}
                    value={this.state.password}
                    maxLength={30}
                    password={true}
                    onChange={password => {
                      this.setState({ password });
                    }} leftIcon={
                      <SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_candado"} />
                    } />
                  <Text style={{ fontSize: 16, textAlign: "center", width: '70%', color: 'white', fontWeight: 'bold', marginBottom: 5, marginTop: 15 }}>{'confirma tu contraseña'}</Text>
                  <Input placeholder={'Nueva Contraseña'}
                    invert={true}
                    value={this.state.password_repeat}
                    maxLength={30}
                    password={true}
                    onChange={password_repeat => {
                      this.setState({ password_repeat });
                    }} leftIcon={
                      <SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_candado"} />
                    } />


                  <Button
                    style={{ marginTop: 30 }}
                    title="Cambiar Contraseña"
                    onPress={() => {
                      this.onCambiarPassword();
                    }}
                  />
                </View>
              }




            </View>
          </View>

          <IphoneXFix />
        </View>
        {loading}
        {passwod_success}

      </ImageBackground>

    );
  }

  processRequest(params) {

  }
  msg(msg) {
    Alert.alert("Importante", msg, [{ text: "Aceptar", onPress: () => { } }], {
      cancelable: false
    });
  }
  validarCodigo() {
    var codigo = this.state.c1 + this.state.c2 + this.state.c3 + this.state.c4;
    if (this.state.code_generated == codigo) {
      this.setState({ paso: 3, loading: false });
    } else {
      this.msg("El código no es válido.");
      this.setState({ paso: 2, loading: false, c1: "", c2: "", c3: "", c4: "" }, () => {
        this.txtField1.focus();
      });
    }
  }
  volverAEnviar() {
    this.setState({ c1: '', c2: '', c3: '', c4: '', loading: true });
    this.onEnviar();
  }
  onCambiarPassword() {
    if (this.state.password != this.state.password_repeat) {
      this.msg("Las contraseña no coinciden.");
      return;
    }
    if (this.state.password.length < 8) {
      this.msg("La contraseña debe ser igual o mayor a 8 caracteres.");
      return;
    }
    var params = {};
    params.password = this.state.password;
    params.userid = this.state.userid;
    fetch(global.domain + "/api/updatepassword", { method: "POST", body: JSON.stringify(params) }).then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        if (responseJson.status == 'OK') {
          this.setState({ paso: 4, password_cambiado: true });
        } else {
          Alert.alert("Error", responseJson.mensaje, [{ text: "Aceptar", onPress: () => { } }],
            { cancelable: false }
          );
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }
  onEnviar() {
    var action = "";
    var params = {};
    if (this.state.email != "") {
      if (!this.validateEmail(this.state.email)) {
        this.msg("Debe ingresar un email válido.");
        return;
      }
      params.email = this.state.email;
      params.action = "email";
    }
    if (this.state.celular != "") {
      if (String(this.state.celular).length < 9) {
        this.msg("Debe ingresar un celular válido.");
        return;
      }
      if (isNaN(this.state.celular)) {
        this.msg("Debe ingresar un celular válido.");
        return;
      }
      params.celular = this.state.celular;
      params.action = "celular";
    }
    if (this.state.email == "" && this.state.celular == "") {
      this.msg("Debe de especificar su correo electronico o su número de celular.");
      return;
    }
    fetch(global.domain + "/api/recuperar", { method: "POST", body: JSON.stringify(params) }).then(response => response.json())
      .then(responseJson => {

        this.setState({ loading: false });
        if (responseJson.status == 'OK') {

          Reactotron.log("  responseJson.code " + responseJson.code);

          this.setState({ paso: 2, code_generated: responseJson.code, userid: responseJson.userid });
        } else {
          Alert.alert("Error", responseJson.status, [{ text: "Aceptar", onPress: () => { } }],
            { cancelable: false }
          );
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }
}

export default ForgotPassword;
