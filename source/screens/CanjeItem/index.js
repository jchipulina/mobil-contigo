import React, { Component } from "react";
import {
  AppRegistry,
  ActivityIndicator,
  StyleSheet,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform,
  Linking,
  Animated,
  View
} from "react-native";
import { connect } from "react-redux";
import QRCode from 'react-native-qrcode-svg';
import { IphoneXFix } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, Header, Button } from "@components";
import Reactotron from "reactotron-react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Image from 'react-native-image-progress';
import { PinchGestureHandler, State } from 'react-native-gesture-handler'

var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');

var scale = new Animated.Value(1);
class CanjeItem extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
  }
  static getDerivedStateFromProps(props, state) {

  }
  onZoomEvent = Animated.event(
    [
      {
        nativeEvent: { scale: scale }
      }
    ],
    {
      useNativeDriver: true
    }
  )
  onZoomStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      Animated.spring(scale, {
        toValue: 1,
        useNativeDriver: true
      }).start()
    }
  }
  render() {
    const { params } = this.props.navigation.state;
    Reactotron.log(params.canje);
    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var w = width;
    /*<Header styleTitle={{fontSize:20}} title={"Disponible para canje"} type={"azul"} align={"center"} style={{backgroundColor:'white',alignItems:'center',justifyContent:'center'}} />*/

    var fix = new IphoneXFix().isIphoneX();

    var fecha_redencion = "";

    if (params.canjeado) {
      var moment_fecha = moment(params.canje.fecha_redencion, 'YYYY-MM-DD HH:mm:ss');
      fecha_redencion = moment_fecha.format('D MMMM YYYY');
    }
    var imagenHeight = (height - 100 - fix) / 2.3
    var slideImageStyle = { width: '100%', height: imagenHeight };
    return (
      <ScrollView style={{ flex: 1, padding: 0, margin: 0 }} contentContainerStyle={{ backgroundColor: 'white' }}>
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: 'white' }}>
          {(params.canjeado) &&
            <View style={{
              width: '100%', backgroundColor: 'rgba(131,140,154,1)', borderBottomRightRadius: 30, borderBottomLeftRadius: 30, marginBottom: 15, paddingBottom: 15
            }}>
              <View style={{ backgroundColor: 'transparent', width: '100%', position: "relative", top: 0, zIndex: 10 }}>
                <Header fecha={fecha_redencion} colorArrow={"white"} styleTitleArrow={{ paddingTop: 20, textAlign: "center", fontWeight: 'bold', fontSize: 20, lineHeight: 20, color: "white", backgroundColor: "transparent", width: width - 105 }} contentStyle={{ flexDirection: "row", paddingTop: 5 }} onBack={() => {
                  this.props.navigation.goBack();
                }} styleTitle={{ fontSize: 22, lineHeight: 22, color: global.azul }} titleArrow={(!params.canjeado) ? "Disponible para canje" : "Canje realizado con éxito el"}
                  style={{ backgroundColor: 'transparent', paddingBottom: 0, marginTop: 0, top: 0, marginLeft: 5, marginTop: 14, marginBottom: 10 }} />
              </View>
            </View>
          }
          {(!params.canjeado) &&
            <View style={{ backgroundColor: 'transparent', width: '100%', position: "relative", top: 0, zIndex: 10 }}>
              <Header styleTitleArrow={{ fontSize: 18, lineHeight: 18, fontWeight: 'bold', marginBottom: 0, paddingTop: 5 }}
                contentStyle={{ backgroundColor: "transparent", flexDirection: "row", paddingTop: 5, marginBottom: 10 }} onBack={() => {
                  this.props.navigation.goBack();
                }} styleTitle={{ fontSize: 20, lineHeight: 22, color: global.azul }} titleArrow={"Canje en proceso de entrega"}
                style={{ paddingBottom: 0, marginTop: 0, top: 0, marginLeft: 5, marginTop: 14, marginBottom: 10 }} />
            </View>
          }
          <View style={slideImageStyle}>
            <Image
              indicator={ActivityIndicator}
              source={{ uri: global.urlimg + params.canje.imagen }}
              style={{
                width: width,
                height: imagenHeight
              }}
              resizeMode='cover'
            />
          </View>


          <View style={{
            width: '100%', flex: 1, borderTopRightRadius: 0,
            backgroundColor: 'white', borderTopLeftRadius: 0, position: "relative", top: 0,
            position: "relative", top: 0, paddingLeft: 30, paddingRight: 30, paddingBottom: 40, paddingTop: 15
          }}>


            <Text style={{
              fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 22
            }}>{params.canje.titulo}</Text>


            <View style={{ flexDirection: "row", alignItems: "center", width: '100%', justifyContent: "space-between", backgroundColor: 'white' }}>
              <Text style={{
                fontFamily: global.fontBold, color: (!params.canjeado) ? global.rojo : global.gris, fontSize: 16,

              }}>{'Código de Redención '}</Text>
              <Text style={{ fontFamily: global.fontBold, fontSize: 20, color: (!params.canjeado) ? global.azul_oscuro : global.gris }}>{params.canje.codigo}</Text>
            </View>


            {(!params.canjeado) &&
              <View>
                <View style={{ width: '100%', marginTop: 15, marginBottom: 15, justifyContent: "center", alignItems: "center" }}>
                  <QRCode value={params.canje.codigo} size={130} />
                </View>
                <Text style={{ fontFamily: global.fontSemiBold, alignContent: 'flex-start', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{params.canje.descripcion_larga}</Text>
                <Text style={{ paddingTop: 10, fontFamily: global.fontRegular, alignContent: 'flex-start', fontWeight: 'bold', color: global.azul_oscuro, fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{"Terminos y condiciones:"}</Text>
                <Text style={{ fontFamily: global.fontSemiBold, alignContent: 'flex-start', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{params.canje.terminos}</Text>
              </View>
            }





          </View>
        </View>
        {loading}
      </ScrollView >
    );
  }
}

const styles = StyleSheet.create({
  centerText: { flex: 1, fontSize: 18, padding: 32, color: "#777" },
  textBold: { fontWeight: "500", color: "#000" },
  buttonText: { fontSize: 21, color: "rgb(0,122,255)" },
  buttonTouchable: { padding: 16 },
  text: {
    color: "#000",
    fontSize: 25,
    backgroundColor: "transparent",
    fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Regular"
  }
});

export default CanjeItem;
/*
<Text style={{
                  fontFamily: global.fontRegular, fontWeight: '500',
                  color: '#7584A4', fontSize: 13
                }}>{'Instrucciones Adicionales'}</Text>

                <Text style={{
                  fontFamily: global.fontRegular, fontWeight: 'bold',
                  color: '#7584A4', fontSize: 13, marginTop: 5, marginBottom: 60
                }}>{'Pongase en contacto con al call Center ********* para coordinar la entrega de su beneficio.'}</Text>
*/
