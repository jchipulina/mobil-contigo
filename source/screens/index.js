import _PerfilSelector from "./PerfilSelector"
export const PerfilSelector = _PerfilSelector

import _Home from "./Home"
export const Home = _Home

import _Login from "./Login"
export const Login = _Login

import _Grupos from "./Grupos"
export const Grupos = _Grupos

import _ForgotPassword from "./ForgotPassword"
export const ForgotPassword = _ForgotPassword

import _Terminos from "./Terminos"
export const Terminos = _Terminos

import _Registro from "./Registro"
export const Registro = _Registro

import _CanjeItem from "./CanjeItem"
export const CanjeItem = _CanjeItem

import _Promociones from "./Promociones"
export const Promociones = _Promociones

import _Codigo from "./Codigo"
export const Codigo = _Codigo

import _Bienvenido from "./Bienvenido"
export const Bienvenido = _Bienvenido

import _Beneficios from "./Beneficios"
export const Beneficios = _Beneficios

import _Categorias from "./Categorias"
export const Categorias = _Categorias

import _BeneficioItem from "./BeneficioItem"
export const BeneficioItem = _BeneficioItem

import _Canjes from "./Canjes"
export const Canjes = _Canjes

import _Compras from "./Compras"
export const Compras = _Compras

import _CompraDetalle from "./CompraDetalle"
export const CompraDetalle = _CompraDetalle

import _PromocionesItem from "./PromocionesItem"
export const PromocionesItem = _PromocionesItem

import _ComoFunciona from "./ComoFunciona"
export const ComoFunciona = _ComoFunciona

import _ComoFuncionaListado from "./ComoFuncionaListado"
export const ComoFuncionaListado = _ComoFuncionaListado

import _Notificaciones from "./Notificaciones"
export const Notificaciones = _Notificaciones

import _BeneficiosListado from "./BeneficiosListado"
export const BeneficiosListado = _BeneficiosListado

import _Ayuda from "./Ayuda"
export const Ayuda = _Ayuda

import _Validacion from "./Validacion"
export const Validacion = _Validacion

import _ValidacionForma from "./ValidacionForma"
export const ValidacionForma = _ValidacionForma


