import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Dimensions
} from "react-native";
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { BottomSpace, Header, Button, SvgCustom } from "@components";
import { DefaultTheme, ProgressBar } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Tooltip } from "react-native-elements";
var { height, width } = Dimensions.get("window");
var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');

const theme = {
  roundness: 2,
  colors: {
    primary: 'red',
    accent: 'red',
  }
};

var counter = 0;
class Compras extends Component {
  constructor(props) {
    super(props);
    global.compras = this;
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      puntos: 0,
      puntos_compras: 0,
      puntos_congelados: 0,
      puntos_ganados: 0,
      galones: 0,
      trimestre_actual: "",
      trimestre_inicio: "",
      trimestre_fin: "",
      puntos_validez: [],
      compras: [],
      meta: true,
    };
  }
  static update() {
    if (global.updateCompras) {
      global.updateCompras = false;
      Reactotron.log("COMPRAS UPDATE");
      if (global.compras != null) global.compras.getCompras();
    }
  }
  componentDidMount() {
    this.getCompras();
  }
  onRefresh() {
    this.getCompras();
  }
  getCompras = () => {
    const url = "http://mobilcontigo.pe/api/clientes/compras/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "GET" })
      .then(response => response.json())
      .then(responseJson => {
        Reactotron.log(responseJson);

        var trimestre_inicio = "";
        if (responseJson.trimestre_inicio != "" && responseJson.trimestre_inicio != null) {
          trimestre_inicio = moment(responseJson.trimestre_inicio, 'YYYY-MM-DD').format('DD/MM');
        }
        var trimestre_fin = "";
        if (responseJson.trimestre_inicio != "" && responseJson.trimestre_fin != null) {
          trimestre_fin = moment(responseJson.trimestre_fin, 'YYYY-MM-DD').format('DD/MM');
        }
        var puntos_validez = new Array();
        if (responseJson.puntos_validez != "" && responseJson.puntos_validez != null && responseJson.puntos_validez != undefined) {
          for (var p = 0; p < responseJson.puntos_validez.length; p++) {
            var f = moment(responseJson.puntos_validez[p].fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
            puntos_validez.push({ fecha: f, puntos: responseJson.puntos_validez[p].puntos });
          }
        }
        var ultima_fecha_en_la_que_gano_puntos;
        if (responseJson.ultima_fecha_en_la_que_gano_puntos != "" && responseJson.ultima_fecha_en_la_que_gano_puntos != null) {
          ultima_fecha_en_la_que_gano_puntos = moment(responseJson.ultima_fecha_en_la_que_gano_puntos, 'YYYY-MM-DD').format('DD/MM/YYYY');
        }
        var comp_arr = responseJson.compras;
        var puntos_compras = 0;
        if (comp_arr != undefined && comp_arr != null) {
          for (var i = 0; i < comp_arr.length; i++) {
            puntos_compras += comp_arr[i].puntos;
          }
        }

        Reactotron.log(responseJson.compras);


        this.setState({
          loading: false,
          puntos: Math.round(Number(responseJson.puntos)),
          puntos_congelados: Math.round(Number(responseJson.puntos_congelados)),
          puntos_ganados: Math.round(Number(responseJson.puntos_ganados)),
          puntos_ganados_fecha: ultima_fecha_en_la_que_gano_puntos,
          galones: Number(responseJson.galones),
          puntos_compras: puntos_compras,
          trimestre_actual: String(responseJson.trimestre_actual.nombre).toUpperCase(),
          trimestre_inicio: trimestre_inicio,
          trimestre_fin: trimestre_fin,
          puntos_validez: puntos_validez,
          compras: responseJson.compras,
          meta: responseJson.meta,
          puntos_canjeados: Number(responseJson.puntos_canjeados)
        });


        //this.setState({ loading: false, galones: 80, puntos: Number(responseJson.puntos), puntos_congelados: Number(responseJson.puntos_congelados) });
      })
      .catch(error => {
        Reactotron.log("ERROR " + error);
        this.setState({ loading: false });
      });
  };
  round(num) {
    return Math.round(num * 100) / 100
  }
  render() {
    var loading;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }
    var textStyle = { fontFamily: global.fontSemiBold, color: global.azul_oscuro }
    /* <Text style={{ textAlign: 'left', fontSize: 12, fontFamily: global.fontNormal, textStyle: "italic", fontWeight: '700', color: 'white', marginTop: 0 }}>
                  {"KMS que aun no puedes utilizar para canjes (50 galones) por que aún no completaste los 130 galones"}</Text>*/
    return (
      <LinearGradient colors={global.tabGradientColorsGray} style={{ flex: 1, backgroundColor: '#000' }}>


        <ScrollView refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={() => { this.onRefresh() }} />} style={{ flex: 1, width: '100%', backgroundColor: 'transparent' }} contentContainerStyle={{ alignItems: "center", paddingTop: 0 }}>
          <View style={{ width: '100%', backgroundColor: global.azul_oscuro }}>
            <Header title={"Compras y metas"} type={"blanco"} marginLeft={30} />
          </View>
          <View style={{
            width: '100%', borderBottomLeftRadius: 30, borderBottomRightRadius: 30, padding: 30, paddingBottom: 0, paddingTop: 10,
            justifyContent: "center", backgroundColor: global.azul_oscuro, backgroundColor: global.azul_oscuro
          }}>

            {!this.state.loading &&
              <Text style={{ fontFamily: global.fontBold, color: global.azul_claro, fontSize: 24 }}>{this.state.trimestre_actual}</Text>
            }
            {!this.state.loading &&
              <Text style={{ fontFamily: global.fontBold, color: global.blanco, fontSize: 15 }}>{'Del '}{this.state.trimestre_inicio}{' al '}{this.state.trimestre_fin}</Text>
            }


            {(!this.state.meta) &&
              <View>
                <View style={{ flexDirection: "row", backgroundColor: 'transparent', justifyContent: "space-between", alignItems: "center" }}>
                  <View style={{ marginTop: 0 }}>
                    <Text style={{ fontFamily: global.fontBold, color: global.gris, fontSize: 30 }}>{'KMS'}</Text>
                    <Text style={{ fontFamily: global.fontBold, color: global.blanco, fontSize: 16 }}>{'disponibles'}</Text>
                  </View>
                  <Text style={{
                    fontFamily: global.fontBold, color: global.blanco, fontSize: 80, lineHeight: 100, backgroundColor: 'transparent'
                    , paddingBottom: 0, marginBottom: 0,
                  }}>{this.state.puntos}</Text>
                </View>

                <View style={{
                  backgroundColor: global.azul, borderRadius: 10, padding: 15, marginTop: 20, paddingBottom: 10,
                  justifyContent: "space-between"
                }}>



                  <View style={{ flexDirection: "row", alignItems: "flex-start" }}>
                    <View style={{ flex: 1, backgroundColor: "transparent" }}>
                      <Text style={{ fontFamily: global.fontBold, color: global.blanco, fontSize: 15 }}>
                        <Text style={{ color: global.rojo }}>Recuerda que debes completar 130 Galones</Text> para acumular KMS en este {this.state.trimestre_actual}
                      </Text>
                    </View>

                    <View style={{ marginTop: 0, backgroundColor: 'transparent', justifyContent: "flex-end" }}>
                      <View style={{ justifyContent: "center", alignItems: "center", width: 44, height: 44, backgroundColor: global.rojo, borderRadius: 22, position: "absolute", top: -30, right: -15 }}>
                        <Icon name="close" size={34} color={"#FFF"}></Icon>
                      </View>
                      <View style={{ flex: 1 }}></View>
                      <Text style={{ textAlign: 'right', fontSize: 10, fontFamily: global.fontNormal, fontWeight: '700', color: 'white' }}>
                        {this.round(this.state.galones) + "/130"}{"\n"}<Text style={{ fontSize: 9 }}>Galones</Text></Text>
                    </View>
                  </View>

                  <ProgressBar style={{ backgroundColor: 'red' }} progress={Number((this.state.galones * 100) / 130) * 0.01} style={{ marginTop: 10 }} theme={{
                    roundness: 2,
                    colors: {
                      primary: 'white',
                      secondary: 'red',
                      accent: 'red',
                    }
                  }} />
                  {this.state.puntos_compras > 0 &&
                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10, height: 20, backgroundColor: "transparent" }}>
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text style={{
                          textAlign: 'right', fontSize: 11, marginRight: 0,
                          fontFamily: global.fontNormal, fontWeight: '700', color: 'white'
                        }}>{'KMS que aún no puedes acumular '}</Text>

                      </View>
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text style={{ textAlign: 'right', marginRight: 5, fontSize: 11, fontFamily: global.fontNormal, fontWeight: '700', color: 'white' }}>{this.state.puntos_compras + ' KMS'}</Text>
                        <Tooltip skipAndroidStatusBar={true} withPointer={false} containerStyle={{ flexWrap: "wrap", paddingBottom: 10, left: 20, height: 60, right: 20, width: 'auto', marginTop: 5 }} overlayColor={'rgba(0,0,0,0.8)'}
                          popover={<Text style={{ paddingLeft: 5, paddingRight: 5, fontFamily: global.fontRegular, color: global.azul_oscuro }}>Para acumularlos debes llegar a los 130 Galones necesarios</Text>} pointerColor={'white'} backgroundColor={'white'}>
                          <SvgCustom width="18" height="18" source={"ico_info"} />
                        </Tooltip>
                      </View>
                    </View>}

                </View>



                <View style={{
                  backgroundColor: 'transparent', width: '100%', marginTop: 10, marginBottom: 20,
                  justifyContent: "center", alignItems: "center"
                }}>
                  <Button
                    style={{ width: '90%' }}
                    title="¿Cómo empezar a participar?"
                    onPress={() => {
                      this.props.navigation.navigate("ComoFunciona");
                    }} ddd
                  />
                </View>
              </View>}
            {(this.state.meta) &&
              <View>
                <View style={{ width: '100%', padding: 20, marginTop: 0, borderRadius: 10, backgroundColor: global.azul_oscuro, justifyContent: "center", alignItems: "center" }}>
                  <Text style={{
                    fontFamily: global.fontBold, color: global.blanco, fontSize: 100, lineHeight: 100, backgroundColor: 'transparent'
                    , paddingBottom: 0, marginBottom: 0, marginTop: 25, height: 85
                  }}>{this.state.puntos}</Text>

                  <Text style={[textStyle, { marginTop: 0, color: global.blanco, fontWeight: 'bold', fontSize: 16 }]}>
                    <Text style={{ color: '#B2BBCF' }}>{'KMS '}</Text>
                    {'disponibles'}</Text>

                  <View style={{ width: "100%", justifyContent: "space-between", flexDirection: 'row', marginTop: 15 }}>
                    {this.state.puntos_validez.map((item, index) => {
                      return <View>
                        <Text style={[textStyle, {
                          marginTop: 0, color: global.azul_claro,
                          fontSize: 12, fontFamily: global.fontSemiBold
                        }]}>
                          <Text style={{
                            color: "#B2BBCF", fontSize: 20, fontFamily:
                              global.fontBold
                          }}>
                            {item.puntos}{' KMS'}
                          </Text>{'\n'}Válidos hasta el{'\n'}{item.fecha}</Text>
                      </View>
                    })}
                  </View>
                </View>
              </View>
            }




          </View>

          <ScrollView bounces={false} showsHorizontalScrollIndicator={false} horizontal={true} contentContainerStyle={{ flexDirection: "row", paddingRight: 15, marginTop: 5 }}>

            {(Number(this.state.puntos_congelados) > 0) &&
              <Medidor2 percent={(Number(this.state.puntos_congelados) * 0.01)} color="#BCC1C8" tooltip={true} title={"Congelados"} value={this.state.puntos_congelados + "/100 KMS"} textColor={global.azul_oscuro} descripcion={""} />
            }
            {(Number(this.state.puntos_ganados) > 0) &&
              <Medidor2 percent={-1} color={global.azul_claro} title={"Ganados"} value={this.state.puntos_ganados + " KMS"} textColor={"#FFF"} descripcion={"¡Felicidades! Los completaste el " + this.state.puntos_ganados_fecha} />
            }
            {(Number(this.state.puntos_canjeados) > 0) &&
              <Medidor2 percent={-1} color={global.azul_oscuro} title={"Canjeados"} value={this.state.puntos_canjeados + " KMS"} textColor={"#FFF"} descripcion={this.state.puntos_canjeados + " KMS canjeados"} />
            }

          </ScrollView>
          <View style={{ width: '90%', backgroundColor: "transparent", marginTop: 5, marginBottom: 20 }} >
            <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
              <Text style={{ fontFamily: global.fontBold, color: global.gris, fontSize: 14 }}>{'COMPRAS'}</Text>
            </View>

            {this.state.compras.length == 0 &&
              <Text style={{ fontFamily: global.fontSemiBold, color: global.azul_oscuro, fontSize: 14, marginTop: 10 }}>{'Aun no realizaste compras'}</Text>
            }
            {this.state.compras.map((compras) => {
              return <Medidor disabled={(compras.promocion) ? true : false} fecha={compras.fecha} onPress={() => {
                if (compras.promocion == false) {
                  this.props.navigation.navigate("CompraDetalle", { compra: compras });
                }

              }} type="white" value={compras.puntos + " KMS"} descripcion={(compras.promocion == false) ? "Comprobante " + compras.documento : "" + compras.documento} />
            })}

          </View>



        </ScrollView>
        <BottomSpace />
        {loading}
      </LinearGradient >
    );

  }
}

class Medidor extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var type = this.props.type;
    var moment_fecha = moment(this.props.fecha, 'YYYY-MM-DD HH:mm:ss');
    var fecha = moment_fecha.format('D MMMM YYYY');

    return (
      <TouchableOpacity disabled={this.props.disabled} onPress={this.props.onPress} style={{ justifyContent: "space-between", marginTop: 15, borderRadius: 10, backgroundColor: (type == 'white') ? 'white' : 'red', padding: 13, flexDirection: 'row' }}>
        <View style={{ backgroundColor: "white" }}>
          <Text style={{ fontFamily: global.fontBold, color: (type == 'white') ? global.azul_oscuro : '#FFF', fontSize: 14 }}>{'Compra ' + fecha}</Text>
          <Text style={{ marginTop: 3, textAlign: 'left', fontFamily: global.fontSemiBold, color: (type == 'white') ? global.gris : '#FFF', fontSize: 14 }}>{this.props.descripcion}</Text>
        </View>
        <View style={{ marginTop: 0 }}>
          <Text style={{ fontFamily: global.fontBold, color: (type == 'white') ? global.azul_claro : '#FFF', fontSize: 18 }}>{this.props.value}</Text>
        </View>

      </TouchableOpacity>
    );
  }
}
class Medidor2 extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var type = this.props.type;
    return (
      <View style={{ marginTop: 15, marginLeft: 20, borderRadius: 10, backgroundColor: this.props.color, padding: 13, width: width - 60 }}>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", backgroundColor: "transparent", height: 28 }}>

            <View style={{ flexDirection: 'row' }}>
              <Text style={{ marginRight: 7, marginTop: 0, fontFamily: global.fontBold, color: (type == 'gris') ? global.azul_oscuro : '#FFF', fontSize: 18 }}>{this.props.title}</Text>
              {this.props.tooltip &&
                <Tooltip withPointer={false} containerStyle={{ flexWrap: "wrap", paddingBottom: 10, left: 20, height: 60, right: 20, width: 'auto', marginTop: 5 }} overlayColor={'rgba(0,0,0,0.8)'}
                  popover={<Text style={{ paddingLeft: 5, paddingRight: 5, fontFamily: global.fontRegular, color: global.azul_oscuro }}>KMS que aun no puedes canjear debido a que no llegan a estar en un grupo de 100 KMS</Text>} pointerColor={'white'} backgroundColor={'white'}>
                  <SvgCustom width="18" height="18" source={"ico_info_blue"} style={{ marginTop: 2 }} />
                </Tooltip>
              }

            </View>

            <Text style={{ marginLeft: 10, fontFamily: global.fontBold, color: '#FFF', fontSize: 25, top: -5 }}>{this.props.value}</Text>
          </View>

          <View>
            <Text style={{ fontFamily: global.fontSemiBold, color: this.props.textColor, fontSize: 12 }}>{this.props.descripcion}</Text>
          </View>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", marginTop: 5 }}>
          <View style={{ flex: 1, backgroundColor: 'transparent' }}>
            {this.props.percent != -1 &&
              <ProgressBar progress={this.props.percent} color={(type == 'gris') ? '#FFF' : '#FFF'} style={{ marginTop: 5 }} />
            }

          </View>
        </View>
      </View >
    );
  }
}


const styles = StyleSheet.create({});

export default Compras;


