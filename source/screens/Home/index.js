import React, { Component, cloneElement } from "react";
import {
  AppRegistry,
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  ScrollView,
  RefreshControl,
  Dimensions,
  Linking,
  Platform,
  NetInfo,
  View,
  TextInput,
  FlatList,
  ActivityIndicator,
  DeviceEventEmitter
} from "react-native";
import { connect } from "react-redux";
import { Inicio, Header, BottomSpace, Beneficio, IphoneXFix, SvgCustom } from "@components";
import { Button, Tooltip } from "react-native-elements";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from "react-native-vector-icons/FontAwesome";
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import OneSignal from 'react-native-onesignal';
import { ProgressBar } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
var hmiphonex = 0;
var { height, width } = Dimensions.get("window");

var _v1 = 0;
var _v2 = 0;
var _v3 = 0;
var _v4 = 0;

var v1 = 0;
var v2 = 0;
var v3 = 0;
var v4 = 0;

var scope;
var direccion = "galones_kms";

class Home extends Component {
  constructor(props) {
    super(props);
    scope = this;
    global.home = this;
    if (this.isIphoneX()) {
      hmiphonex = 10;
    }
    this.state = {
      enableCalc: false,
      calcOpen: false,
      totalNuevos: 0,
      refreshing: false,
      promociones: [],
      _v1: 0,
      _v2: 0,
      _v3: 0,
      _v4: 0,
      v1: 0,
      v2: 0,
      v3: 0,
      v4: 0,
      vtotal: 0,
      puntos_: 0,
      puntos_congelados: 0,
      direccion: "galones_kms",
    };

    OneSignal.init("e35af3c3-387b-4fa7-8c6a-2aa81538a308");
    OneSignal.addEventListener('opened', this.onOpened);

  }
  static update() {
    if (global.updateHome) {
      global.updateHome = false;
      Reactotron.log("HOME UPDATE");
      if (global.home != null) global.home.getCompras();
    }
  }
  switchCalculadora() {
    if (this.state.direccion == "galones_kms") {
      this.setState({ direccion: "kms_galones" }, () => {
        direccion = "kms_galones";
        this.calcProcessLeft();
      });
    } else {
      this.setState({ direccion: "galones_kms" }, () => {
        direccion = "galones_kms";
        this.calcProcessLeft();
      });
    }
  }
  calcProcessLeft() {
    Reactotron.log(direccion);

    if (direccion == "galones_kms") {
      var t1 = _v1 * 2.5;
      var t2 = this.round(_v2 * 1.5);
      var t3 = _v3 * 0.75;
      var t4 = _v4 * 0.5;
      var suma = t1 + t2 + t3 + t4;
      this.setState({ v1: this.round(t1) + "" }, () => {
        v1 = this.round(t1);
      });
      this.setState({ v2: this.round(t2) + "" }, () => {
        v2 = this.round(t2);
      });
      this.setState({ v3: this.round(t3) + "" }, () => {
        v3 = this.round(t3);
      });
      this.setState({ v4: this.round(t4) + "" }, () => {
        v4 = this.round(t4);
      });
      this.setState({ vtotal: Math.round(suma) + "" });
    } else {
      var t1 = _v1 / 2.5;
      var t2 = this.round(_v2 / 1.5);
      var t3 = _v3 / 0.75;
      var t4 = _v4 / 0.5;
      var suma = t1 + t2 + t3 + t4;
      this.setState({ v1: this.round(t1) + "" }, () => {
        v1 = this.round(t1);
      });
      this.setState({ v2: this.round(t2) + "" }, () => {
        v2 = this.round(t2);
      });
      this.setState({ v3: this.round(t3) + "" }, () => {
        v3 = this.round(t3);
      });
      this.setState({ v4: this.round(t4) + "" }, () => {
        v4 = this.round(t4);
      });
      this.setState({ vtotal: Math.round(suma) + "" });
    }




  }
  calcProcessRight() {

    var t1 = this.round(v1 / 2.5);
    var t2 = this.round(v2 / 1.5);
    var t3 = this.round(v3 / 1);
    var t4 = this.round(v4 / 0.75);
    var suma = t1 + t2 + t3 + t4;
    this.setState({ _v1: this.round(t1) + "" }, () => {
      _v1 = this.round(t1);
    });
    this.setState({ _v2: this.round(t2) + "" }, () => {
      _v2 = this.round(t2);
    });
    this.setState({ _v3: this.round(t3) + "" }, () => {
      _v3 = this.round(t3);
    });
    this.setState({ _v4: this.round(t4) + "" }, () => {
      _v4 = this.round(t4);
    });
    this.setState({ vtotal: this.round(suma) + "" });
  }
  round(num) {
    return Math.round(num * 100) / 100
  }
  componentDidMount() {
    this.makeRemoteRequest();
    this.getCompras();
  }
  getCompras = () => {
    const url = "http://mobilcontigo.pe/api/clientes/compras/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "GET" })
      .then(response => response.json())
      .then(responseJson => {
        Reactotron.log(responseJson);

        if (responseJson.meta) {
          global.user.meta = true;
        } else {
          global.user.meta = false;
        }

        this.setState({ loading: false, puntos: Number(responseJson.puntos), puntos_congelados: Number(responseJson.puntos_congelados) });
      })
      .catch(error => {
        Reactotron.log("ERROR " + error);
        this.setState({ loading: false });
      });
  };
  onRefresh() {
    this.makeRemoteRequest();
    this.getCompras();
  }
  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = global.domain + "/api/start_info/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.nuevos_beneficios,
          promociones: responseJson.promociones,
          totalNuevos: responseJson.nuevos_beneficios.length,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };
  _keyExtractor = (item, index) => item.id;
  _renderItem = ({ item }) => (
    <Beneficio beneficio={item} onPress={(beneficio) => {
      this.props.navigation.navigate("BeneficioItem", { beneficio: beneficio });
    }} />
  );


  onOpened(openResult) {
    setTimeout(() => {
      scope.props.navigation.navigate("Notificaciones");
    }, 500)

  }
  componentWillUnmount() {
    global.onUpdateUserData = null;
  }
  isIphoneX() {
    const dimen = Dimensions.get("window");

    return (
      Platform.OS === "ios" &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      (dimen.height === 812 || dimen.height > 812)
    );
  }

  render() {
    var loading;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }
    var leftFlex = 0.8
    var ItemCalcStyle = { flex: 1, backgroundColor: 'white', marginRight: 7, marginLeft: 7, marginBottom: 5 };
    var ItemCalcTextStyle = { fontFamily: global.fontBold, color: 'rgba(193,198,205,1)', fontSize: 10, lineHeight: 10, backgroundColor: 'white', marginRight: 10 };
    var ItemCalcTextInputStyle = { flex: 1, backgroundColor: 'transparent', textAlign: 'right', fontFamily: global.fontRegular, color: 'rgba(193,198,205,1)', fontSize: 30, fontWeight: '400', padding: 0, margin: 0 }
    var ItemCalcLineStyle = { width: '100%', height: 1, backgroundColor: 'rgba(193,198,205,1)', marginTop: 5 }
    return (
      <LinearGradient colors={global.tabGradientColorsGray} style={{ flex: 1, backgroundColor: '#000' }}>


        <KeyboardAwareScrollView
          style={{ flex: 1, width: '100%', backgroundColor: 'transparent' }}
          refreshControl={
            <RefreshControl refreshing={this.state.refreshing} onRefresh={() => { this.onRefresh() }} />}
          contentContainerStyle={{ alignItems: "center", paddingTop: 0 }}>

          <View style={{
            width: '100%', borderBottomLeftRadius: 30, borderBottomRightRadius: 30, padding: 30, paddingBottom: 0,
            justifyContent: "center", backgroundColor: global.azul_oscuro, paddingTop: 25
          }}>
            <View style={{ marginTop: (this.isIphoneX()) ? 40 : 20 }}>
              <SvgCustom width="150" height="60" source={"logo_mobil_home"} />

            </View>
            <View style={{ alignItems: "center", marginTop: 15 }}>

              <View style={{ flexDirection: 'row' }}>
                <Text style={{ fontFamily: global.fontBold, color: global.blanco, fontSize: 20, marginBottom: 0, marginRight: 7 }}>{'KMS disponibles'}</Text>
                <Tooltip skipAndroidStatusBar={true} withPointer={false} containerStyle={{ flexWrap: "wrap", left: 20, height: 50, right: 20, width: 'auto' }} overlayColor={'rgba(0,0,0,0.8)'}
                  popover={<Text style={{ paddingLeft: 5, paddingRight: 5, fontFamily: global.fontRegular, color: global.azul_oscuro }}>KMS que ya puedes canjear por premios </Text>} pointerColor={'white'} backgroundColor={'white'}>
                  <SvgCustom style={{ marginTop: 3 }} width="18" height="18" source={"ico_info"} />
                </Tooltip>
              </View>


              <Text style={{
                fontFamily: global.fontBold, color: global.blanco, fontSize: 100, lineHeight: 100, backgroundColor: 'transparent'
                , paddingBottom: 0, marginBottom: 0, marginTop: 15
              }}>{this.state.puntos}</Text>

            </View>

            <View style={{ marginTop: 0, flexDirection: 'row', justifyContent: "space-between", backgroundColor: 'transparent' }}>
              <View style={{ flex: 1, flexDirection: "row", backgroundColor: 'transparent' }}>
                <Text style={{ fontFamily: global.fontBold, color: global.blanco, backgroundColor: 'transparent', marginTop: 2, marginRight: 7 }}>
                  {'KMS Congelados Trimestre Actual'}</Text>
                <Tooltip skipAndroidStatusBar={true} withPointer={false} containerStyle={{ flexWrap: "wrap", paddingBottom: 10, left: 20, height: 60, right: 20, width: 'auto', marginTop: 5 }} overlayColor={'rgba(0,0,0,0.8)'}
                  popover={<Text style={{ paddingLeft: 5, paddingRight: 5, fontFamily: global.fontRegular, color: global.azul_oscuro }}>KMS que aun no puedes canjear debido a que no llegan a estar en un grupo de 100 KMS</Text>} pointerColor={'white'} backgroundColor={'white'}>
                  <SvgCustom width="18" height="18" source={"ico_info"} />
                </Tooltip>
              </View>

              <Text style={{ fontFamily: global.fontSemiBold, color: global.blanco, fontSize: 12, marginTop: 2 }}>{this.state.puntos_congelados + '/100'}</Text>
            </View>
            <ProgressBar progress={(Number(this.state.puntos_congelados) * 0.01)} color={global.azul_claro} style={{ marginTop: 10, height: 6, borderRadius: 10 }} />

            <View style={{ width: '100%', backgroundColor: 'transparent', justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate("ComoFunciona");
              }}>
                <Text style={{ marginTop: 20, textAlign: "center", fontFamily: global.fontSemiBold, color: global.rojo }}>{'¿Cómo gano más KMS?'}</Text>
              </TouchableOpacity>

            </View>
            <View style={{ width: 25, height: 25, backgroundColor: "transparent" }} />
          </View>

          <View style={{ width: '90%', backgroundColor: "transparent", marginTop: 20, marginBottom: 20 }} >





            {this.state.promociones.length > 0 &&
              <View style={{ marginBottom: 20 }}>
                <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between" }}>
                  <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 14 }}>{'PROMOCIONES'}</Text>
                  <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate("Promociones");
                  }}>
                    <Text style={{ fontFamily: global.fontBold, color: global.rojo, fontSize: 14 }}>{'Ver todas (' + this.state.promociones.length + ')'}</Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => {
                  this.props.navigation.navigate("PromocionesItem", { beneficio: this.state.promociones[0] });
                }}
                  style={{ backgroundColor: global.azul, borderRadius: 15, padding: 15, marginTop: 8 }}>
                  <Text style={{ fontFamily: global.fontBold, color: global.blanco, fontSize: 17 }}>{this.state.promociones[0].titulo}</Text>
                  <Text style={{ fontFamily: global.fontSemiBold, color: global.gris, fontSize: 14, marginTop: 4 }}>{this.state.promociones[0].descripcion}</Text>
                </TouchableOpacity>
              </View>
            }






            <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between", marginTop: 0 }}>
              <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 14 }}>{'NUEVOS BENEFICIOS'}</Text>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate("Beneficios");
              }}>
                <Text style={{ fontFamily: global.fontBold, color: global.rojo, fontSize: 14 }}>{'Ver todos'}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              <FlatList
                scrollEnabled={false}
                style={{ paddingLeft: 0, paddingRight: 0 }}
                ItemSeparatorComponent={this.renderSeparator}
                keyExtractor={(l) => l.id}
                data={this.state.data}
                onEndReachedThreshold={20}
                getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
                renderItem={this._renderItem}
              />
            </View>
            <View style={{ height: 20 }}></View>



            {this.state.enableCalc && <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between" }}>
              <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 14 }}>{'CALCULADORA DE KMS'}</Text>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate("Promociones");
              }}>

              </TouchableOpacity>

            </View>}

            {this.state.enableCalc &&
              <TouchableOpacity onPress={() => {
                if (this.state.calcOpen) {
                  this.setState({ calcOpen: false });
                } else {
                  this.setState({ calcOpen: true });
                }

              }} style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", backgroundColor: global.azul_oscuro, borderRadius: 10, padding: 10, marginTop: 15, paddingRight: 20, marginBottom: (this.state.calcOpen) ? 0 : 30 }}>
                <Text style={{ fontFamily: global.fontBold, color: global.blanco, fontSize: 18, padding: 10 }}>
                  Usar calculadora</Text>
                <View style={{ flexDirection: 'row', justifyContent: "center", alignItems: "center" }}>
                  <View style={{ width: 20, height: 20, backgroundColor: 'transparent', justifyContent: "center", alignItems: "center", marginRight: 5 }}>
                    <Icon name={(!this.state.calcOpen) ? "chevron-down" : "chevron-up"} size={20} color={'white'}></Icon>
                  </View>
                  <SvgCustom width="25" height="25" source={"ico_block"} />
                </View>

              </TouchableOpacity>
            }


            {this.state.calcOpen &&
              <View style={{ backgroundColor: 'white', width: '100%', marginBottom: 5, borderRadius: 10, paddingLeft: 0, paddingRight: 0, marginBottom: 20 }}>
                <View style={{
                  backgroundColor: 'transparent', flexDirection: 'row', justifyContent: "space-between", paddingLeft: 12, paddingRight: 12,
                  paddingTop: 15, paddingBottom: 10
                }}>
                  <Text style={{ width: "30%", backgroundColor: 'transparent', fontFamily: global.fontBold, color: global.azul, fontSize: 15, padding: 5, paddingTop: 5 }}>{(this.state.direccion == "galones_kms") ? 'GALONES' : 'KMS'}</Text>
                  <TouchableOpacity onPress={() => {
                    this.switchCalculadora();
                  }} style={{ width: 60, height: 30, backgroundColor: 'white', justifyContent: "center", alignItems: "center" }}>
                    <SvgCustom width="25" height="25" source={"ico_change"} />
                  </TouchableOpacity>
                  <Text style={{ width: "30%", textAlign: "right", backgroundColor: 'transparent', fontFamily: global.fontBold, color: global.azul, fontSize: 15, padding: 5, paddingTop: 5 }}>{(this.state.direccion == "galones_kms") ? 'KMS' : 'GALONES'}</Text>
                </View>

                <View style={{ flexDirection: "row", paddingLeft: 10, paddingRight: 10 }}>



                  <View style={ItemCalcStyle}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{'\nSINTÉTICO'}</Text>
                      <TextInput keyboardType="numeric" placeholder={'0'} maxLength={4}
                        onChangeText={(value) => { _v1 = Number(value); this.setState({ _v1: Number(value) + "" }, this.calcProcessLeft()); }}
                        value={this.state._v1} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>


                  <View style={[ItemCalcStyle, { flex: leftFlex }]}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{''}</Text>
                      <TextInput editable={false} keyboardType="numeric" placeholder={'0'} maxLength={6}
                        onChangeText={(value) => { v1 = Number(value); this.setState({ v1: Number(value) + "" }, this.calcProcessRight()); }}
                        value={this.state.v1} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>


                </View>
                <View style={{ flexDirection: "row", paddingLeft: 10, paddingRight: 10 }}>

                  <View style={ItemCalcStyle}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{'SEMI\nSINTÉTICO'}</Text>
                      <TextInput keyboardType="numeric" placeholder={'0'} maxLength={4}
                        onChangeText={(value) => { _v2 = Number(value); this.setState({ _v2: Number(value) + "" }, this.calcProcessLeft()); }}
                        value={this.state._v2} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>

                  <View style={[ItemCalcStyle, { flex: leftFlex }]}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{''}</Text>
                      <TextInput editable={false} keyboardType="numeric" placeholder={'0'} maxLength={6}
                        onChangeText={(value) => { v2 = Number(value); this.setState({ v2: Number(value) + "" }, this.calcProcessRight()); }}
                        value={this.state.v2} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>
                </View>
                <View style={{ flexDirection: "row", paddingLeft: 10, paddingRight: 10 }}>


                  <View style={ItemCalcStyle}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{'MINERAL\nPREMIUM'}</Text>
                      <TextInput keyboardType="numeric" placeholder={'0'} maxLength={4}
                        onChangeText={(value) => {
                          _v3 = Number(value);
                          this.setState({ _v3: Number(value) + "" }, this.calcProcessLeft());
                        }}
                        value={this.state._v3} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>

                  <View style={[ItemCalcStyle, { flex: leftFlex }]}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{''}</Text>
                      <TextInput editable={false} keyboardType="numeric" placeholder={'0'} maxLength={6}
                        onChangeText={(value) => { v3 = Number(value); this.setState({ v3: Number(value) }, this.calcProcessRight()); }}
                        value={this.state.v3} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>
                </View>
                <View style={{ flexDirection: "row", paddingLeft: 10, paddingRight: 10 }}>

                  <View style={ItemCalcStyle}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{'MINERAL\nESTANDAR'}</Text>
                      <TextInput keyboardType="numeric" placeholder={'0'} maxLength={4}
                        onChangeText={(value) => { _v4 = Number(value); this.setState({ _v4: Number(value) + "" }, this.calcProcessLeft()); }}
                        value={this.state._v4} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>

                  <View style={[ItemCalcStyle, { flex: leftFlex }]}>
                    <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
                      <Text style={ItemCalcTextStyle}>{''}</Text>
                      <TextInput editable={false} keyboardType="numeric" placeholder={'0'} maxLength={6}
                        onChangeText={(value) => { v4 = Number(value); this.setState({ v4: Number(value) }, this.calcProcessRight()); }}
                        value={this.state.v4} style={ItemCalcTextInputStyle}></TextInput>
                    </View>
                    <View style={ItemCalcLineStyle}></View>
                  </View>


                </View>

                <View style={{
                  backgroundColor: 'rgba(131,140,154,1)', justifyContent: "space-between", padding: 5,
                  flexDirection: "row", borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                }}>
                  <Text style={{ backgroundColor: 'transparent', fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 16, padding: 5, paddingTop: 5 }}>{(this.state.direccion == "galones_kms") ? 'TOTAL\nKMS' : 'TOTAL\nGALONES'}</Text>
                  <Text style={{ backgroundColor: 'transparent', lineHeight: 50, fontFamily: global.fontRegular, color: global.blanco, fontWeight: '400', fontSize: 50, padding: 5, paddingBottom: 0 }}>{this.state.vtotal}</Text>
                </View>
              </View>
            }


          </View>
        </KeyboardAwareScrollView>
        <BottomSpace />
        {loading}
      </LinearGradient >
    );
  }
}



class ItemCalc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value + ""
    }
  }
  onChangeText(text) {
    this.setState({ value: text });
    this.props.onChange(text);
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white', marginRight: 7, marginLeft: 7, marginBottom: 22 }}>
        <View style={{ flexDirection: 'row', alignItems: "flex-end" }}>
          <Text style={{ fontFamily: global.fontBold, color: 'rgba(193,198,205,1)', fontSize: 12, lineHeight: 12, backgroundColor: 'white', marginRight: 10 }}>{this.props.title}</Text>
          <TextInput keyboardType="numeric" placeholder={'00'} onChangeText={text => this.onChangeText(text)} value={this.state.value} style={{ flex: 1, backgroundColor: 'transparent', textAlign: 'right', fontFamily: global.fontBold, color: 'rgba(193,198,205,1)', fontSize: 30, fontWeight: '400', padding: 0, margin: 0 }}></TextInput>
        </View>
        <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(193,198,205,1)', marginTop: 5 }}></View>
      </View>
    );
  }
}



export default Home;
