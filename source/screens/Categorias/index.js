import React, { Component } from "react";
import {
  Dimensions,
  View,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { Navbar, Header, Categoria } from "@components";

class Categorias extends Component {
  constructor(props) {
    super(props);
    this.state = { categorias: [], loading: false };
  }
  render() {
    const { params } = this.props.navigation.state;
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>

        <Header onBack={() => {
          this.props.navigation.goBack();
        }} titleArrow={"BENEFICIOS"} title={"Categorías"} type={"azul"} size={"normal"} />

        <View style={{ flex: 1, marginTop: 10, padding: 30, paddingTop: 0, backgroundColor: "rgba(243,243,243,1)" }}>
          {params.categorias.map((categoria, index) => {
            return <Categoria fontSize={18} style={{ width: "100%" }}
              styleText={{ justifyContent: "space-between", alignItems: "center", marginLeft: 0, paddingLeft: 0 }} title={categoria.nombre} imagen={categoria.imagen} type="full_width" onPress={() => {
                this.props.navigation.navigate("BeneficiosListado", { categoria: params.categorias[index], categorias: params.categorias });
              }} />
          })}



        </View>
      </View>
    );
  }
}

export default Categorias;
