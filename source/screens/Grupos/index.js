import React, { Component } from "react";
import {
  Dimensions,
  View,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { Navbar, Header, Categoria } from "@components";

class Grupos extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Navbar arrow={global.azul}
          onBack={() => {
            this.props.navigation.goBack();
          }}
        />
        <Header title={"Grupos"} type={"azul"} size={"normal"} />
        <View style={{ width: '94%', marginTop: 10, marginLeft: '6%', marginBottom: 10, backgroundColor: '#D4EEF8', height: 1 }}></View>
        <View style={{ flex: 1, padding: 30, paddingTop: 0 }}>
          <Categoria title="Familia" type="full_width" color={global.azul} onPress={() => {

          }} />
          <Categoria title="Familia" type="full_width" color={global.azul_claro} onPress={() => {

          }} />
          <Categoria title="Familia" type="full_width" color={global.rojo} onPress={() => {

          }} />
        </View>
      </View>
    );
  }
}

export default Grupos;
