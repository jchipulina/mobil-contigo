import React, { Component } from "react";
import {
  View,
  Image,
  Alert,
  ImageBackground,
  Text,
  TextInput,
  Linking,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import NetInfo from "@react-native-community/netinfo";
import { Navbar, Header, Input, Button, IphoneXFix, BottomSpace, SvgCustom } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import Reactotron from "reactotron-react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { img } from "@media";
import Ionicons from "react-native-vector-icons/Ionicons";
import { openComposer } from 'react-native-email-link'

var { height, width } = Dimensions.get("window");
var dniValues = ["", "", "", "", "", "", "", "", "", "", ""];
var timeOut;
class Validacion extends Component {
  constructor(props) {
    super(props);
    var { params } = this.props.navigation.state;
    this.state = {
      email: String(global.user.email).toLowerCase(),
      tipo: params.tipo,
      celular: global.user.celular,
      loading: false,
      netStatus: false,
      c1: '',
      c2: '',
      c3: '',
      c4: '',
      userid: '',
      code_generated: '',
      password: '',
      password_repeat: '',
      password_cambiado: false,
    };
  }
  componentDidMount() {
    NetInfo.isConnected.addEventListener("change", this.handleConnectionChange);
    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({ netStatus: isConnected });
    });
  }
  handleConnectionChange = isConnected => {
    this.setState({ netStatus: isConnected });
    Reactotron.log(`is connected: ${this.state.netStatus}`);
  };
  componentWillMount() {

  }
  handleKeyPress({ nativeEvent: { key: keyValue } }) {
    if (keyValue === 'Backspace') {
      this.focusFieldByID(focus - 1);
      this.emptyFieldByID(focus);
    }
  }
  focusFieldByID(num) {
    focus = num;
    switch (num) {
      case 1:
        this.txtField1.focus();
        break;
      case 2:
        this.txtField2.focus();
        break;
      case 3:
        this.txtField3.focus();
        break;
      case 4:
        this.txtField4.focus();
        break;
    }
  }
  emptyFieldByID(num) {
    switch (num) {
      case 1:
        this.setState({ c1: '' });
        break;
      case 2:
        this.setState({ c2: '' });
        break;
      case 3:
        this.setState({ c3: '' });
        break;
      case 4:
        this.setState({ c4: '' });
        break;
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  onChangeText(stext, num) {

    clearTimeout(timeOut);
    timeOut = setTimeout(() => {
      Reactotron.log("setTimeout");
      if (this.state.c1 != '' && this.state.c2 != '' && this.state.c3 != '' && this.state.c4 != '') {
        this.setState({ loading: true })
        this.validarCodigo();
        return
      }
    }, 1000);




    if (num == 5) return;

    if (stext.length == 1) {
      this.focusFieldByID(num);
    }
  }
  componentWillFocus() {
    this.setState({
      c1: "",
      c2: "",
      c3: "",
      c4: ""
    });
  }
  render() {
    var inputSize = (width / 10) - 7;

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var styleBox = {
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      width: '70%',
      height: 40,
      borderColor: "#ccc",
      borderWidth: 1,
      marginLeft: 1,
      marginRight: 1,
      backgroundColor: "#FFF",
      borderRadius: 10,
      fontFamily: "EMprint-Bold",
      fontSize: 18,
      paddingTop: 0,
      paddingBottom: 0
    };

    var boxStyle = { flexDirection: 'row', marginLeft: 10, backgroundColor: 'transparent' }
    var lineStyle = { width: '94%', marginTop: 0, marginLeft: '6%', marginBottom: 0, backgroundColor: 'transparent', height: 1 };
    var textStyle = { color: 'white', fontFamily: global.fontRegular, fontSize: 19, lineHeight: 20, marginBottom: 0 };
    var iconContainer = { width: 65, height: 50, backgroundColor: 'transparent', justifyContent: "center", alignItems: "center" }

    return (

      <ImageBackground source={img.bg_camion} style={{ flex: 1, width: '100%', backgroundColor: 'rgba(46,80,111,1)' }}>
        <View style={{ flex: 1, backgroundColor: "rgba(46,80,111,0.6)" }}>
          <Header colorArrow={"white"} onBack={() => {
            this.props.navigation.pop();

          }} />
          <View
            style={{ flex: 1, alignItems: "center" }}
          >
            <View
              style={{
                width: width,
                marginTop: (new IphoneXFix().isIphoneX()) ? 0 : 0,
                backgroundColor: "transparent",
                justifyContent: "center",
                alignItems: 'center'
              }}
            >
              <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 26, fontFamily: "EMprint", marginBottom: 25 }}>{'Código enviado'}</Text>

              {this.state.tipo == "email" &&
                <Text style={{ color: 'white', textAlign: "center", fontWeight: 'bold', fontSize: 18, fontFamily: "EMprint", marginBottom: 40 }}>{'Le enviamos un código al correo \n' + this.state.email}</Text>
              }
              {this.state.tipo == "celular" &&
                <Text style={{ color: 'white', textAlign: "center", fontWeight: 'bold', fontSize: 18, fontFamily: "EMprint", marginBottom: 40 }}>{'Escribe el código que enviamos a tu celular'}</Text>
              }



              <View style={{ width: "100%", alignItems: "center" }}>


                <View style={{ backgroundColor: "transparent", width: "100%", flexDirection: "row", justifyContent: "center" }}>
                  <Input
                    style={{ width: 50, height: 50 }}
                    textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                    placeholder={''}
                    keyboardType='numeric'
                    invert={true}
                    ref={c => (this.txtField1 = c)}
                    value={this.state.c1}
                    maxLength={1}
                    onChange={c1 => {
                      this.setState({ c1 }, this.onChangeText(c1, 2));
                    }}
                  />
                  <Input
                    style={{ width: 50, height: 50 }}
                    textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                    placeholder={''}
                    keyboardType='numeric'
                    invert={true}
                    ref={c => (this.txtField2 = c)}
                    value={this.state.c2}
                    maxLength={1}
                    onChange={c2 => {
                      this.setState({ c2 }, this.onChangeText(c2, 3));

                    }}
                  />
                  <Input
                    style={{ width: 50, height: 50 }}
                    textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                    placeholder={''}
                    keyboardType='numeric'
                    invert={true}
                    ref={c => (this.txtField3 = c)}
                    value={this.state.c3}
                    maxLength={1}
                    onChange={c3 => {
                      this.setState({ c3 }, this.onChangeText(c3, 4));
                    }}
                  />
                  <Input
                    style={{ width: 50, height: 50 }}
                    keyboardType='numeric'
                    textStyle={{ textAlign: "center", fontWeight: "bold", fontSize: 20, lineHeight: 22, paddingTop: 5, marginTop: (Platform.OS === "ios") ? 25 : 35 }}
                    placeholder={''}
                    invert={true}
                    ref={c => (this.txtField4 = c)}
                    value={this.state.c4}
                    maxLength={1}
                    onChange={c4 => {
                      this.setState({ c4 }, this.onChangeText(c4, 5));
                    }}
                  />
                </View>
                <Text style={{ fontSize: 16, textAlign: "center", width: '90%', color: 'white', fontWeight: 'bold', marginBottom: 5, marginTop: 20 }}>{'No he recibido el código'}</Text>

                <TouchableOpacity style={{ width: 115, marginBottom: 20, marginTop: 10 }} onPress={() => {
                  this.volverAEnviar();
                }}>
                  <Text style={{ fontSize: 16, lineHeight: 16, textAlign: "center", width: '100%', color: 'white', fontWeight: 'bold', color: "red" }}>{'volver a enviar'}</Text>
                  <View style={{ width: "100%", height: 1, backgroundColor: global.rojo }}></View>
                </TouchableOpacity>

              </View>


            </View>
          </View>

          <View style={{ flex: 1 }}></View>

          <Text style={[textStyle, {
            fontFamily: global.fontSemiBold, paddingLeft: 25, color: "#FFF",
            fontSize: 15, lineHeight: 16, backgroundColor: "transparent", width: "100%"
          }]} >{'Si tienes algun problema con la validación de tu cuenta comunícate con nosotros  de lunes a viernes de 9:00 a.m. a 1:00 p.m. y de 2:00 p.m. a 6:00 p.m. '}</Text>


          <View style={{ flexDirection: 'row', marginLeft: 10, backgroundColor: 'transparent', paddingTop: 10, paddingBottom: 20 }}>
            <View style={{ width: 65, height: 95, backgroundColor: 'transparent', justifyContent: "center", alignItems: "center" }}>
              <Icon name={'whatsapp'} size={30} color={'#FFF'} />
            </View>


            <View style={{ flex: 1, justifyContent: "center", marginLeft: 0 }}>
              <Text style={textStyle}>{'Llámanos o envíanos un mensaje a nuestro WhatsApp al:'}</Text>
              <TouchableOpacity onPress={() => {
                Linking.openURL('whatsapp://send?phone=51' + this.state.mobile_number_1)
              }}>
                <Text style={[textStyle, { fontWeight: 'bold', marginTop: 5 }]} >{'+51 913-033-857'}</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => {
                Linking.openURL('whatsapp://send?phone=51' + this.state.mobile_number_2)
              }}>
                <Text style={[textStyle, { fontWeight: 'bold', marginTop: 5 }]} >{'+51 965-406-943'}</Text>
              </TouchableOpacity>


            </View>


          </View>


          <View style={[boxStyle, { marginBottom: 30 }]}>
            <View style={iconContainer}>
              <Icon name={'at'} size={30} color={'#FFF'} />
            </View>
            <View style={{ flex: 1, justifyContent: "center", marginLeft: 0 }}>
              <Text style={textStyle}>{'También puedes escribirnos a'}</Text>

              <TouchableOpacity onPress={() => {
                openComposer({
                  to: 'contacto@mobilcontigo.pe',
                  subject: 'Contacto Mobil Contigo',
                  body: (Platform.OS === 'ios') ? 'Hola mi nombre es ' + global.user.nombres + ' ' + global.user.apellidos + ', con número de celular ' + global.user.celular + ' ' : '',
                  cancelLabel: 'Cancelar',
                })
              }}>
                <Text style={[textStyle, { fontWeight: 'bold', marginTop: 5 }]} >{'contacto@mobilcontigo.pe'}</Text>
              </TouchableOpacity>

            </View>
          </View>

          <IphoneXFix />
        </View>
        {loading}
      </ImageBackground>

    );
  }

  processRequest(params) {

  }
  msg(msg) {
    Alert.alert("Importante", msg, [{ text: "Aceptar", onPress: () => { } }], {
      cancelable: false
    });
  }
  validarCodigo() {
    this.setState({ loading: true });
    var codigo = this.state.c1 + this.state.c2 + this.state.c3 + this.state.c4;
    fetch(global.domain + "/api/validate_registration_code/" + global.user.id + "/" + codigo,
      { method: "POST", body: JSON.stringify({}) }).then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        if (responseJson.status == 'OK') {
          var user = global.user;
          user.activo = 1;
          global.user = user;
          global.saveUser();
          this.props.navigation.navigate("App");
        } else {
          this.txtField1.focus();
          Alert.alert("Error", responseJson.mensaje, [{ text: "Aceptar", onPress: () => { } }],
            { cancelable: false }
          );
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }
  volverAEnviar() {
    this.setState({ c1: '', c2: '', c3: '', c4: '', loading: true });
    this.onEnviar();
  }
  onEnviar() {
    this.setState({ loading: true });
    fetch(global.domain + "/api/sent_registration_code/" + global.user.id + "/" + this.state.tipo,
      { method: "POST", body: JSON.stringify({}) }).then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        if (responseJson.status == 'OK') {
          if (this.state.tipo == "email") {
            this.msg("Se envio un nuevo código a su correo electronico");
          }
          if (this.state.tipo == "celular") {
            this.msg("Se envio un nuevo código a su celular");
          }

        } else {
          this.txtField1.focus();
          Alert.alert("Error", "Intente más tarde por favor", [{ text: "Aceptar", onPress: () => { } }],
            { cancelable: false }
          );
        }
      }).catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }

}

export default Validacion;
