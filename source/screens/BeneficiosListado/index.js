import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Linking,
  View,
  Image,
  ActivityIndicator,
  FlatList,
  List,
  ScrollView,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, HeaderMobil, Header, Beneficio } from "@components";
import { Input } from 'react-native-elements';
import Material from "react-native-vector-icons/MaterialIcons";


var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');

class BeneficiosListado extends Component {
  constructor(props) {
    const { params } = props.navigation.state;

    var sc_obj = null;
    var sc = -1;
    if (params.categoria != null) {
      sc = params.categoria.id;
      sc_obj = params.categoria;
    }

    super(props);
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      key: null,
      grupo: params.grupo,
      selectedCategory: sc,
      selectedCategoryObj: sc_obj
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { page, seed } = this.state;

    Reactotron.log("grupo " + this.state.grupo);
    Reactotron.log("selectedCategoryObj " + this.state.selectedCategoryObj);

    var grupo = -1;
    var categoria = -1;
    if (this.state.grupo != null && this.state.grupo != undefined) {
      grupo = this.state.grupo;
    }
    if (this.state.selectedCategoryObj != null && this.state.selectedCategoryObj != undefined) {
      categoria = this.state.selectedCategoryObj.id;
    }

    const url = global.domain + "/api/beneficios/" + global.user.id + "/" + categoria + "/" + grupo;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };
  _keyExtractor = (item, index) => item.id;
  _renderItem = ({ item }) => (
    <Beneficio beneficio={item} onPress={(beneficio) => {
      this.props.navigation.navigate("BeneficioItem", { beneficio: beneficio });
    }} />
  );

  render() {

    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.data.length == 0) {
        content = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: global.fontRegular,
                color: "black"
              }}
            >{'No se encontraron beneficios.'}</Text>


          </View>
        );
      } else {
        content = (

          <View style={{ flex: 1 }}>
            <FlatList
              style={{ paddingLeft: 25, paddingRight: 25 }}
              contentContainerStyle={{ paddingBottom: 40 }}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(l) => l.id}
              data={this.state.data}
              onEndReachedThreshold={20}
              getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
              renderItem={this._renderItem}
            />
          </View>

        );
      }
    }
    renderSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "86%",
            backgroundColor: "#CED0CE",
            marginLeft: "14%"
          }}
        />
      );
    };
    const { params } = this.props.navigation.state;
    var id_selected = -1;
    if (this.state.selectedCategoryObj != null) {
      id_selected = this.state.selectedCategoryObj.id;
    }
    var lblTop = "Todos";
    if (id_selected != -1) {
      lblTop = this.state.selectedCategoryObj.nombre
    }
    if (this.state.grupo != null) {
      if (this.state.grupo == 1) {
        lblTop = "Para tu Empresa";
      }
      if (this.state.grupo == 2) {
        lblTop = "Para tus Clientes";
      }
      if (this.state.grupo == 3) {
        lblTop = "Para tu Familia y para ti";
      }
    }
    return (
      <View style={{ flex: 1, backgroundColor: "rgba(242,242,242,1)" }}>
        <Header onBack={() => {
          this.props.navigation.goBack();
        }} titleArrow={"BENEFICIOS"} styleTitle={{ color: global.azul_oscuro, fontSize: 27 }}
          styleTitleArrow={{ color: global.azul_super_claro, fontSize: 14 }} title={lblTop} />
        <View style={{ padding: 25, paddingTop: 0, marginTop: 0, paddingBottom: 10, backgroundColor: 'rgba(242,242,242,1)' }}>
          <View style={{ flexDirection: "row", marginTop: 10 }} >
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{ flexDirection: "row", marginTop: 10 }}>
              {params.categorias.map((categoria, index) => {
                return <Categoria title={categoria.nombre} id={categoria.id} selected={(id_selected == categoria.id) ? true : false}
                  onPress={(id) => {
                    if (id_selected == categoria.id) {
                      this.setState({ selectedCategoryObj: null });
                      return
                    }
                    for (var i = 0; i < params.categorias.length; i++) {
                      if (id == params.categorias[i].id) {
                        Reactotron.log("index " + index);
                        this.setState({ selectedCategoryObj: params.categorias[i] }, this.makeRemoteRequest);

                        break;
                      }
                    }
                  }} />
              })}
            </ScrollView>
          </View>
        </View>
        <View style={{ flex: 1 }}>{content}</View>
      </View >
    );
  }
}

class Categoria extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var contentStyle = {
      backgroundColor: global.azul_claro, paddingTop: 2, paddingLeft: 15, paddingRight: 15,
      height: 30, borderRadius: 20, justifyContent: "center", marginRight: 8, backgroundColor: (!this.props.selected) ? "rgba(181,186,195,1)" : global.azul_oscuro
    }
    var styleTagText = { fontFamily: global.fontSemiBold, color: 'white', fontSize: 14 }

    return (
      <TouchableOpacity onPress={() => {
        this.props.onPress(this.props.id);
      }} style={[contentStyle, this.props.style]}>
        <Text style={styleTagText}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}



export default BeneficiosListado;

/*<Input
            containerStyle={{ backgroundColor: "rgba(1,1,1,0.1)", borderRadius: 5, width: '100%' }}
            inputContainerStyle={{ borderBottomColor: "rgba(0,0,0,0)" }}
            inputStyle={{ fontFamily: "EMprint", color: '#000', fontSize: 16 }}
            underlineColorAndroid={'rgba(0,0,0,0)'}
            placeholder={'Buscar'}
            placeholderTextColor={"#8E8E93"}
            value={this.state.search}
            maxLength={100}
            onChangeText={p => {
              this.props.onChange({ p });
            }}
            leftIcon={
              <Material
                style={{ marginRight: 12, paddingLeft: 0, marginLeft: 0 }}
                name='search'
                size={24}
                color={'#8E8E93'}
              />
            }
          />*/
