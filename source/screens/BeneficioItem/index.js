import React, { Component } from "react";
import {
  AppRegistry,
  ActivityIndicator,
  StyleSheet,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform,
  Linking,
  Animated,
  View
} from "react-native";
import { connect } from "react-redux";
import QRCode from 'react-native-qrcode-svg';
import { IphoneXFix } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, Header, Button } from "@components";
import Reactotron from "reactotron-react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Mask from "react-native-mask";
import AwesomeAlert from 'react-native-awesome-alerts';
import { PinchGestureHandler, State } from 'react-native-gesture-handler'
import Swiper from 'react-native-swiper'
import Image from 'react-native-image-progress';

var scale = new Animated.Value(1);
class BeneficioItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      beneficio: null,
      loading: false,
      showAlert: false,
      showAlertError: false,
      showAlertErrorMsg: '',
      showAlertPost: false,
      showAlertPostError: false,
      showAlertNoPuntos: false,
      showAlertPostMsg: ''
    };
  }
  static getDerivedStateFromProps(props, state) {

  }
  onZoomEvent = Animated.event(
    [
      {
        nativeEvent: { scale: scale }
      }
    ],
    {
      useNativeDriver: true
    }
  )
  onZoomStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      Animated.spring(scale, {
        toValue: 1,
        useNativeDriver: true
      }).start()
    }
  }
  checkBeneficio() {
    const { beneficio } = this.props.navigation.state.params;
    const url = global.domain + "/api/beneficio_status/" + beneficio.id;
    this.setState({ loading: true });
    fetch(url, { method: "GET" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        if (responseJson.status == "OK") {
          this.setState({ showAlert: true });
        } else {
          this.setState({ showAlertError: true, showAlertErrorMsg: responseJson.mensaje });
        }

      })
      .catch(error => {
        Reactotron.log("ERROR " + error);
        this.setState({ loading: false });
      });

  }
  getCompras = () => {
    const { beneficio } = this.props.navigation.state.params;
    const url = "http://mobilcontigo.pe/api/clientes/compras/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "GET" })
      .then(response => response.json())
      .then(responseJson => {
        var puntos = Number(responseJson.puntos);
        Reactotron.log("responseJson.puntos " + responseJson.puntos);
        if (puntos < Number(beneficio.puntos)) {
          this.setState({ showAlertNoPuntos: true, loading: false })
        } else {
          this.makeRemoteRequest();
        }
      })
      .catch(error => {
        Reactotron.log("ERROR " + error);
        this.setState({ loading: false });
      });
  };
  makeRemoteRequest = () => {
    const { beneficio } = this.props.navigation.state.params;
    const url = "http://mobilcontigo.pe/api/beneficios/canjear";
    Reactotron.log("url " + url);
    fetch(url, {
      method: "POST",
      body: JSON.stringify({ beneficio: beneficio.id, lubricentro: global.user.id })
    }).then(response => response.json())
      .then(responseJson => {
        Reactotron.log("RESPONSE !");
        Reactotron.log(responseJson);
        if (responseJson.status == "OK") {
          Reactotron.log("updateCanjes / updateCompras / updateHome");
          global.updateCanjes = true;
          global.updateCompras = true;
          global.updateHome = true;
          this.setState({
            loading: false,
            beneficio: responseJson.beneficio,
            showAlertPost: true,
            showAlertPostError: false,
            showAlertPostMsg: responseJson.mensaje
          });
        } else {
          this.setState({
            loading: false,
            showAlertPost: true,
            showAlertPostError: true,
            showAlertPostMsg: responseJson.mensaje
          });
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };
  canjearConfirm() {
    this.setState({ loading: true, showAlert: false });
    this.getCompras();
  }

  render() {
    const { params } = this.props.navigation.state;
    // Reactotron.log(params.canje);
    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var w = width;
    /*<Header styleTitle={{fontSize:20}} title={"Disponible para canje"} type={"azul"} align={"center"} style={{backgroundColor:'white',alignItems:'center',justifyContent:'center'}} />*/

    /*<Mask shape={'square'}>
                    <PinchGestureHandler
                      onGestureEvent={this.onZoomEvent}
                      onHandlerStateChange={this.onZoomStateChange}>
                      <Animated.Image
                        source={{ uri: global.urlimg + params.beneficio.imagen }}
                        style={{
                          width: width,
                          height: (height - 100 - fix) / 2,
                          transform: [{ scale: scale }]
                        }}
                        resizeMode='contain'
                      />
                    </PinchGestureHandler>
                  </Mask>*/


    var fix = new IphoneXFix().isIphoneX();
    const { beneficio } = this.props.navigation.state.params;


    var imagenes = new Array();

    if (beneficio.imagen != null) {
      imagenes.push({ imagen: beneficio.imagen });
    }
    if (beneficio.imagen2 != null) {
      imagenes.push({ imagen: beneficio.imagen2 });
    }
    if (beneficio.imagen3 != null) {
      imagenes.push({ imagen: beneficio.imagen3 });
    }

    var imagenHeight = (height - 100 - fix) / 2.3
    var slideImageStyle = { width: '100%', height: imagenHeight };
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ backgroundColor: 'white' }}>
          <View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: 'transparent' }}>
            <View style={{ backgroundColor: 'transparent', width: '100%', position: "relative", top: 0, zIndex: 10, paddingBottom: 5 }}>
              <Header styleTitleArrow={{ fontSize: 24, lineHeight: 24 }} contentStyle={{ flexDirection: "row", paddingTop: 5 }} onBack={() => {
                this.props.navigation.goBack();
              }} styleTitle={{ fontSize: 20, lineHeight: 22, color: global.azul }} titleArrow={""}
                style={{ backgroundColor: 'transparent', paddingBottom: 0, marginTop: 0, top: 0, marginLeft: 5, marginTop: 14, marginBottom: 10 }} />
            </View>

            <Swiper style={{ height: imagenHeight }} nextButton={<View></View>} prevButton={<View></View>} activeDotColor={global.gris} paginationStyle={{ backgroundColor: "transparent", marginTop: -100, position: "absolute", top: imagenHeight + 100 + 5, height: 20 }} showsButtons={true}>

              {imagenes.map((item, index) => {
                return <View style={slideImageStyle}>
                  <Image
                    indicator={ActivityIndicator}
                    source={{ uri: global.urlimg + item.imagen }}
                    style={{
                      width: width,
                      height: imagenHeight
                    }}
                    resizeMode='cover'
                  />
                </View>
              })}


            </Swiper>

            <View style={{
              width: '100%', flex: 1, borderTopRightRadius: 0,
              backgroundColor: 'transparent', borderTopLeftRadius: 0, position: "relative", top: 0,
              position: "relative", top: 15, paddingTop: (imagenes.length > 1) ? 20 : 5, paddingLeft: 30, paddingRight: 30, paddingBottom: 40, bottom: 0, flex: 1
            }}>




              <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                  <View style={{ backgroundColor: 'white', flex: 1, paddingRight: 10 }}>
                    <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 22 }}>{beneficio.titulo}</Text>
                  </View>


                  {(Number(beneficio.grupo) == 1) &&
                    <View style={{
                      backgroundColor: global.azul_claro, paddingTop: 2, marginTop: 4,
                      paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
                    }}>
                      <Text style={{ fontFamily: global.fontSemiBold, color: 'white', fontSize: 12 }}>{'Empresa'}</Text>
                    </View>
                  }
                  {(Number(beneficio.grupo) == 2) &&
                    <View style={{
                      backgroundColor: global.rojo, paddingTop: 2, marginTop: 4,
                      paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
                    }}>
                      <Text style={{ fontFamily: global.fontSemiBold, color: 'white', fontSize: 12 }}>{'Clientes'}</Text>
                    </View>
                  }
                  {(Number(beneficio.grupo) == 3) &&
                    <View style={{
                      backgroundColor: global.azul, paddingTop: 2, marginTop: 4,
                      paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
                    }}>
                      <Text style={{ fontFamily: global.fontSemiBold, color: 'white', fontSize: 12 }}>{'Familia'}</Text>
                    </View>
                  }
                </View>


                <View style={{ flex: 1, width: '100%', backgroundColor: 'transparent' }}>
                  <Text style={{ fontFamily: global.fontSemiBold, alignContent: 'flex-start', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>
                    {beneficio.descripcion_larga}
                  </Text>
                  <Text style={{ paddingTop: 10, fontFamily: global.fontRegular, alignContent: 'flex-start', fontWeight: 'bold', color: global.azul_oscuro, fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{"Terminos y condiciones:"}</Text>
                  <Text style={{ fontFamily: global.fontSemiBold, textAlign: "justify", alignContent: 'flex-start', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{beneficio.terminos}</Text>
                </View>


                <View style={{ width: '100%', backgroundColor: 'transparent', alignItems: "center", marginTop: 20, marginBottom: 20, justifyContent: "flex-end" }}>



                  <Button title={'Canjear por ' + beneficio.puntos + ' KMS'} onPress={() => {

                    this.checkBeneficio();
                  }}></Button>
                </View>

              </View>


            </View>
          </View>







        </ScrollView >



        <AwesomeAlert
          show={this.state.showAlertError}
          showProgress={false}
          titleStyle={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 30, lineHeight: 30, paddingBottom: 10, backgroundColor: 'transparent' }}
          messageStyle={{ color: global.azul_claro, fontSize: 18, fontFamily: global.fontSemiBold, paddingTop: 2, textAlign: "center" }}
          confirmButtonStyle={{ backgroundColor: global.rojo, width: '100%', marginBottom: 30 }}
          confirmButtonTextStyle={{ color: '#FFF', fontFamily: "EMprint-Bold", fontSize: 18, paddingLeft: '20%', paddingRight: '20%', paddingTop: 4, paddingBottom: 4 }}
          cancelButtonStyle={{ backgroundColor: 'transparent', width: '100%', paddingTop: 8 }}
          cancelButtonTextStyle={{ color: 'gray', fontFamily: global.fontSemiBold, padding: 0, fontSize: 16 }}
          contentStyle={{ backgroundColor: 'white', flexDirection: "column" }}
          actionContainerStyle={{ marginTop: 10, width: '100%', backgroundColor: 'rgba(0,0,0,0.0)', flexDirection: "column-reverse", alignItems: "center" }}
          contentContainerStyle={{ backgroundColor: 'white', borderRadius: 18, width: 400 }}
          alertContainerStyle={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
          title={<Text style={{ textAlign: "center", backgroundColor: 'transparent' }}>
            <Text style={{ fontSize: 14, fontFamily: global.fontRegular, lineHeight: 20 }}>{"\n"}</Text>
            <Text style={{ paddingTop: 10, marginTop: 10, lineHeight: 30 }}>{beneficio.titulo}</Text>
          </Text>}
          message={this.state.showAlertErrorMsg}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={true}
          showConfirmButton={true}
          confirmText="Aceptar"
          confirmButtonColor="#000"
          onConfirmPressed={() => {
            this.setState({ showAlertError: false });
          }}
        />



        <AwesomeAlert
          show={this.state.showAlertNoPuntos}
          showProgress={false}
          titleStyle={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 30, lineHeight: 30, paddingBottom: 10, backgroundColor: 'transparent' }}
          messageStyle={{ color: global.azul_claro, fontSize: 18, fontFamily: global.fontSemiBold, paddingTop: 2 }}
          confirmButtonStyle={{ backgroundColor: global.rojo, width: '100%', marginBottom: 30 }}
          confirmButtonTextStyle={{ color: '#FFF', fontFamily: "EMprint-Bold", fontSize: 18, paddingLeft: '20%', paddingRight: '20%', paddingTop: 4, paddingBottom: 4 }}
          cancelButtonStyle={{ backgroundColor: 'transparent', width: '100%', paddingTop: 8 }}
          cancelButtonTextStyle={{ color: 'gray', fontFamily: global.fontSemiBold, padding: 0, fontSize: 16 }}
          contentStyle={{ backgroundColor: 'white', flexDirection: "column" }}
          actionContainerStyle={{ marginTop: 10, width: '100%', backgroundColor: 'rgba(0,0,0,0.0)', flexDirection: "column-reverse", alignItems: "center" }}
          contentContainerStyle={{ backgroundColor: 'white', borderRadius: 18, width: 400 }}
          alertContainerStyle={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
          title={<Text style={{ textAlign: "center", backgroundColor: 'transparent' }}>
            <Text style={{ fontSize: 14, fontFamily: global.fontRegular, lineHeight: 20 }}>{"\n"}</Text>
            <Text style={{ paddingTop: 10, marginTop: 10, lineHeight: 30 }}>{beneficio.titulo}</Text>
          </Text>}
          message={"No tienes KMS suficientes"}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={true}
          showConfirmButton={true}
          confirmText="Aceptar"
          confirmButtonColor="#000"
          onConfirmPressed={() => {
            this.setState({ showAlertNoPuntos: false });
          }}
        />
        <AwesomeAlert
          show={this.state.showAlertPost}
          showProgress={false}
          titleStyle={{ textAlign: "center", color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 30, lineHeight: 30, paddingBottom: 10, backgroundColor: 'transparent' }}
          messageStyle={{ color: global.azul_claro, fontSize: 18, fontFamily: global.fontSemiBold, paddingTop: 2 }}
          confirmButtonStyle={{ backgroundColor: global.rojo, width: '100%' }}
          confirmButtonTextStyle={{ color: '#FFF', fontFamily: "EMprint-Bold", fontSize: 18, paddingLeft: '20%', paddingRight: '20%', paddingTop: 4, paddingBottom: 4 }}
          cancelButtonStyle={{ backgroundColor: 'transparent', width: '100%', paddingTop: 8 }}
          cancelButtonTextStyle={{ color: 'gray', fontFamily: global.fontSemiBold, padding: 0, fontSize: 16 }}
          contentStyle={{ backgroundColor: 'white', flexDirection: "column" }}
          actionContainerStyle={{ marginTop: 10, width: '100%', backgroundColor: 'rgba(0,0,0,0.0)', flexDirection: "column-reverse", alignItems: "center" }}
          contentContainerStyle={{ backgroundColor: 'white', borderRadius: 18, width: 400 }}
          alertContainerStyle={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
          title={<Text style={{ textAlign: "center", backgroundColor: 'white', justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontSize: 14, fontFamily: global.fontRegular, lineHeight: 20, textAlign: "center", backgroundColor: "white" }}>{this.state.showAlertPostMsg + "\n\n"}</Text>
            {!this.state.showAlertPostError &&
              <Text style={{ paddingTop: 10, marginTop: 10, lineHeight: 30 }}>{beneficio.titulo}</Text>
            }

          </Text>}
          message={(!this.state.showAlertPostError) ? "por " + beneficio.puntos + " KMS" : ""}
          closeOnTouchOutside={false}
          showCancelButton={(this.state.showAlertPostError) ? false : true}
          closeOnHardwareBackPress={true}
          showConfirmButton={true}
          confirmText={(this.state.showAlertPostError) ? "Aceptar" : "Ver detalles"}
          cancelText="Aceptar"
          confirmButtonColor="#000"
          onCancelPressed={() => {
            this.setState({ showAlertPost: false });
          }}
          onConfirmPressed={() => {
            if (this.state.showAlertPostError == false) {
              this.props.navigation.navigate("CanjeItem", { canje: this.state.beneficio, canjeado: false });
            }
            this.setState({ showAlertPost: false });
          }}
        />
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          titleStyle={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 30, lineHeight: 30, paddingBottom: 10, backgroundColor: 'transparent' }}
          messageStyle={{ color: global.azul_claro, fontSize: 18, fontFamily: global.fontSemiBold, paddingTop: 2 }}
          confirmButtonStyle={{ backgroundColor: global.rojo, width: '100%' }}
          confirmButtonTextStyle={{ color: '#FFF', fontFamily: "EMprint-Bold", fontSize: 18, paddingLeft: '20%', paddingRight: '20%', paddingTop: 4, paddingBottom: 4 }}
          cancelButtonStyle={{ backgroundColor: 'transparent', width: '100%', paddingTop: 8 }}
          cancelButtonTextStyle={{ color: 'gray', fontFamily: global.fontSemiBold, padding: 0, fontSize: 16 }}
          contentStyle={{ backgroundColor: 'white', flexDirection: "column" }}
          actionContainerStyle={{ marginTop: 10, width: '100%', backgroundColor: 'rgba(0,0,0,0.0)', flexDirection: "column-reverse", alignItems: "center" }}
          contentContainerStyle={{ backgroundColor: 'white', borderRadius: 18, width: 400 }}
          alertContainerStyle={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
          title={<Text style={{ textAlign: "center", backgroundColor: 'white', justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontSize: 14, fontFamily: global.fontRegular, lineHeight: 20, textAlign: "center", backgroundColor: "white" }}>{"¿Está seguro que desea canjear?\n\n"}</Text>
            <Text style={{ paddingTop: 10, marginTop: 10, lineHeight: 30 }}>{beneficio.titulo}</Text>
          </Text>}
          message={"por " + beneficio.puntos + " KMS"}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={true}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancelar"
          confirmText="Confirmar Ahora"
          confirmButtonColor="#000"
          onCancelPressed={() => {
            this.setState({ showAlert: false });
          }}
          onConfirmPressed={() => {
            this.canjearConfirm();

          }}
        />

        {loading}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centerText: { flex: 1, fontSize: 18, padding: 32, color: "#777" },
  textBold: { fontWeight: "500", color: "#000" },
  buttonText: { fontSize: 21, color: "rgb(0,122,255)" },
  buttonTouchable: { padding: 16 },
  text: {
    color: "#000",
    fontSize: 25,
    backgroundColor: "transparent",
    fontFamily: Platform.OS === "ios" ? "EMprint" : "EMprint-Regular"
  }
});

export default BeneficioItem;
/*<View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: 'transparent' }}>



          <View style={{ backgroundColor: 'transparent', width: '100%', position: "relative", top: 0, zIndex: 10, paddingBottom: 5 }}>
            <Header styleTitleArrow={{ fontSize: 24, lineHeight: 24 }} contentStyle={{ flexDirection: "row", paddingTop: 5 }} onBack={() => {
              this.props.navigation.goBack();
            }} styleTitle={{ fontSize: 20, lineHeight: 22, color: global.azul }} titleArrow={""}
              style={{ backgroundColor: 'transparent', paddingBottom: 0, marginTop: 0, top: 0, marginLeft: 5, marginTop: 14, marginBottom: 10 }} />
          </View>



          <Swiper style={{ height: imagenHeight }} nextButton={<View></View>} prevButton={<View></View>} activeDotColor={global.gris} paginationStyle={{ marginTop: 0, paddingTop: 0, top: 305 }} showsButtons={true}>
            <View style={slideImageStyle}>
              <Mask shape={'square'}>
                <PinchGestureHandler
                  onGestureEvent={this.onZoomEvent}
                  onHandlerStateChange={this.onZoomStateChange}>
                  <Animated.Image
                    indicator={ActivityIndicator}
                    source={{ uri: global.urlimg + params.beneficio.imagen }}
                    style={{
                      width: width,
                      height: imagenHeight,
                      transform: [{ scale: scale }]
                    }}
                    resizeMode='contain'
                  />
                </PinchGestureHandler>
              </Mask>
            </View>
            <View style={slideImageStyle}>
              <Mask shape={'square'}>
                <PinchGestureHandler
                  onGestureEvent={this.onZoomEvent}
                  onHandlerStateChange={this.onZoomStateChange}>
                  <Animated.Image
                    source={{ uri: global.urlimg + params.beneficio.imagen }}
                    style={{
                      width: width,
                      height: (height - 100 - fix) / 2,
                      transform: [{ scale: scale }]
                    }}
                    resizeMode='contain'
                  />
                </PinchGestureHandler>
              </Mask>
            </View>
            <View style={slideImageStyle}>
              <Mask shape={'square'}>
                <PinchGestureHandler
                  onGestureEvent={this.onZoomEvent}
                  onHandlerStateChange={this.onZoomStateChange}>
                  <Animated.Image
                    source={{ uri: global.urlimg + params.beneficio.imagen }}
                    style={{
                      width: width,
                      height: (height - 100 - fix) / 2,
                      transform: [{ scale: scale }]
                    }}
                    resizeMode='contain'
                  />
                </PinchGestureHandler>
              </Mask>
            </View>
          </Swiper>




          <View style={{
            width: '100%', flex: 1, borderTopRightRadius: 0,
            backgroundColor: 'transparent', borderTopLeftRadius: 0, position: "relative", top: 0,
            position: "relative", top: 15, padding: 30, bottom: 0, flex: 1
          }}>




            <View style={{ flex: 1, backgroundColor: 'transparent' }}>
              <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                <View style={{ backgroundColor: 'transparent' }}>
                  <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 22 }}>{beneficio.titulo}</Text>
                </View>


                {(Number(beneficio.grupo) == 1) &&
                  <View style={{
                    backgroundColor: global.azul_claro, paddingTop: 2, marginTop: 4,
                    paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
                  }}>
                    <Text style={{ fontFamily: global.fontRegular, fontWeight: '500', color: 'white', fontSize: 12 }}>{'Empresa'}</Text>
                  </View>
                }
                {(Number(beneficio.grupo) == 2) &&
                  <View style={{
                    backgroundColor: global.rojo, paddingTop: 2, marginTop: 4,
                    paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
                  }}>
                    <Text style={{ fontFamily: global.fontRegular, fontWeight: '500', color: 'white', fontSize: 12 }}>{'Clientes'}</Text>
                  </View>
                }
                {(Number(beneficio.grupo) == 3) &&
                  <View style={{
                    backgroundColor: global.azul, paddingTop: 2, marginTop: 4,
                    paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
                  }}>
                    <Text style={{ fontFamily: global.fontRegular, fontWeight: '500', color: 'white', fontSize: 12 }}>{'Familia'}</Text>
                  </View>
                }



              </View>


              <View style={{ flex: 1, width: '100%', backgroundColor: 'transparent' }}>
                <Text style={{ fontFamily: global.fontRegular, alignContent: 'flex-start', fontWeight: '500', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>
                  {beneficio.descripcion_larga}
                </Text>
                <Text style={{ paddingTop: 10, fontFamily: global.fontRegular, alignContent: 'flex-start', fontWeight: 'bold', color: global.azul_oscuro, fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{"Terminos y condiciones:"}</Text>
                <Text style={{ fontFamily: global.fontRegular, alignContent: 'flex-start', fontWeight: '500', color: '#7584A4', fontSize: 13, marginTop: 10, flexWrap: 'wrap' }}>{beneficio.terminos}</Text>
              </View>


              <View style={{ width: '100%', backgroundColor: 'transparent', alignItems: "center", marginTop: 20, marginBottom: 20, justifyContent: "flex-end" }}>



                <Button title={'Canjear por ' + beneficio.puntos + ' KMS'} onPress={() => {
                  this.setState({ showAlert: true });
                }}></Button>
              </View>

            </View>


          </View>
        </View>
*/