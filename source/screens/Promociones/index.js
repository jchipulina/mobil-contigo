import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Linking,
  View,
  Image,
  ActivityIndicator,
  FlatList,
  List,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, HeaderMobil, Header, Beneficio } from "@components";
import { Input } from 'react-native-elements';
import Material from "react-native-vector-icons/MaterialIcons";

var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');

class Promociones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = global.domain + "/api/promociones/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        Reactotron.log(responseJson);
        this.setState({
          data: responseJson,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };
  _keyExtractor = (item, index) => item.id;


  _renderItem = ({ item }) => (
    <Beneficio type={'promocion'} beneficio={item} onPress={(beneficio) => {
      this.props.navigation.navigate("PromocionesItem", { beneficio: beneficio });
    }} />
  );

  render() {

    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.data.length == 0) {
        content = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>


            <Text
              style={{
                fontSize: 16,
                fontFamily: global.fontRegular,
                color: "white"
              }}
            >{'No se encontraron promociones.'}</Text>


          </View>
        );
      } else {
        content = (

          <View style={{ flex: 1 }}>
            <FlatList
              style={{ paddingLeft: 25, paddingRight: 25 }}
              contentContainerStyle={{ paddingBottom: 40 }}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(l) => l.id}
              data={this.state.data}
              onEndReachedThreshold={20}
              getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
              renderItem={this._renderItem}
            />
          </View>

        );
      }
    }
    renderSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "86%",
            backgroundColor: "#CED0CE",
            marginLeft: "14%"
          }}
        />
      );
    };

    var styleTag = {
      backgroundColor: global.azul_claro, paddingTop: 2, paddingLeft: 20, paddingRight: 20,
      height: 35, borderRadius: 20, justifyContent: "center", marginRight: 8
    }
    var styleTagText = { fontFamily: global.fontSemiBold, color: 'white', fontSize: 16 }
    return (
      <View style={{ flex: 1, backgroundColor: global.azul_oscuro }}>
        <Header styleTitle={{ marginTop: 2, color: global.azul_claro }} colorArrow={global.azul_claro} onBack={() => {
          this.props.navigation.goBack();
        }} title={"Promociones"} marginLeft={30} style={{ marginTop: 0 }} counter={this.state.data.length + ""} />

        <View style={{ width: '100%', marginTop: 10, marginBottom: 10, backgroundColor: global.azul_claro, height: 1 }}></View>
        <View style={{ flex: 1 }}>{content}</View>
      </View>
    );
  }
}
export default Promociones;
