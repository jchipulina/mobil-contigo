import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  ScrollView,
  Image,
  Linking,
  TouchableOpacity,
  ActivityIndicator,
  FlatList
} from "react-native";
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { Header, Navbar, Accordion, BottomSpace, IphoneXFix } from "@components";
import { ProgressBar } from 'react-native-paper';
import Icon from "react-native-vector-icons/FontAwesome";
//import { TouchableOpacity } from "react-native-gesture-handler";
import { openComposer } from 'react-native-email-link'

var counter = 0;
class Ayuda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile_number_1: "913033857",
      mobile_number_2: "965406943",
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      preguntas_frecuentes: []
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = global.domain + "/api/preguntas_frecuentes/";
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          preguntas_frecuentes: responseJson
        });
        this.setState({ loading: false });
      }).catch(error => {
        this.setState({ loading: false });
      });
  };
  renderAccordion = () => {
    const items = [];
    var counter = 0;
    for (item of this.state.preguntas_frecuentes) {


      items.push(
        <Accordion
          style={{ backgroundColor: "red" }}
          title={item.pregunta}
          data={<Text>{item.respuesta}</Text>}
        />
      );
      counter++;
    }
    return items;
  }
  render() {
    var loading;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var boxStyle = { flexDirection: 'row', marginLeft: 10, backgroundColor: 'white' }
    var lineStyle = { width: '94%', marginTop: 0, marginLeft: '6%', marginBottom: 0, backgroundColor: '#ccc', height: 1 };
    var textStyle = { color: global.azul, fontFamily: global.fontRegular, fontSize: 19, lineHeight: 20, marginBottom: 0 };
    var iconContainer = { width: 65, height: 95, backgroundColor: 'white', justifyContent: "center", alignItems: "center" }
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>

        <View style={{ backgroundColor: 'white', width: '100%', marginBottom: 0 }}>

          <Header style={{ marginBottom: 20 }} onCerrar={() => {
            this.props.navigation.goBack();
          }} title={"Ayuda"} type={"azul_oscuro"} size={"normal"} />
        </View>
        <ScrollView style={{ flex: 1, width: '100%', backgroundColor: 'white' }} contentContainerStyle={{ alignItems: "center", paddingTop: 0 }}>


          <View style={{ width: '95%', backgroundColor: "transparent", marginTop: 0, marginBottom: 20, marginLeft: '5%' }} >
            <Text style={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 25, marginLeft: 10, marginBottom: 10, marginTop: 0 }}>{"Preguntas Frecuentes"}</Text>
            {this.renderAccordion()}
          </View>

          <View style={{ flexDirection: 'row', marginLeft: 10, backgroundColor: 'white', paddingTop: 0, paddingBottom: 20, width: "100%", justifyContent: "flex-start" }}>

            <TouchableOpacity onPress={() => {
              if (global.user.activo) {
                this.props.navigation.navigate("ComoFunciona");
              } else {
                this.props.navigation.navigate("ComoFuncionaExterna");
              }
            }}>
              <Text style={[textStyle, {
                fontFamily: global.fontSemiBold, paddingLeft: 25, color: global.azul_oscuro,
                fontSize: 15, backgroundColor: "white", width: "100%"
              }]} >{'Ver los productos participantes'}</Text>
            </TouchableOpacity>

          </View>
          <View style={lineStyle}></View>

          {(global.user.activo) &&
            <View style={{ flexDirection: 'row', marginLeft: 10, backgroundColor: 'white', paddingTop: 10, paddingBottom: 20 }}>
              <View style={{ width: 65, height: 95, backgroundColor: 'white', justifyContent: "center", alignItems: "center" }}>
                <Icon name={'whatsapp'} size={30} color={'#000'} />
              </View>


              <View style={{ flex: 1, justifyContent: "center", marginLeft: 0 }}>
                <Text style={textStyle}>{'Llámanos o envíanos un mensaje a nuestro WhatsApp al:'}</Text>
                <TouchableOpacity onPress={() => {
                  Linking.openURL('whatsapp://send?phone=51' + this.state.mobile_number_1)
                }}>
                  <Text style={[textStyle, { fontWeight: 'bold', marginTop: 5 }]} >{'+51 913-033-857'}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                  Linking.openURL('whatsapp://send?phone=51' + this.state.mobile_number_2)
                }}>
                  <Text style={[textStyle, { fontWeight: 'bold', marginTop: 5 }]} >{'+51 965-406-943'}</Text>
                </TouchableOpacity>


              </View>


            </View>
          }

          {(global.user.activo) && <View style={lineStyle}></View>}




          {(global.user.activo) &&
            <View style={boxStyle}>
              <View style={iconContainer}>
                <Icon name={'at'} size={30} color={'#000'} />
              </View>
              <View style={{ flex: 1, justifyContent: "center", marginLeft: 0 }}>
                <Text style={textStyle}>{'También puedes escribirnos a'}</Text>

                <TouchableOpacity onPress={() => {
                  openComposer({
                    to: 'contacto@mobilcontigo.pe',
                    subject: 'Contacto Mobil Contigo',
                    body: (Platform.OS === 'ios') ? 'Hola mi nombre es ' + global.user.nombres + ' ' + global.user.apellidos + ', con número de celular ' + global.user.celular + ' ' : '',
                    cancelLabel: 'Cancelar',
                  })
                }}>
                  <Text style={[textStyle, { fontWeight: 'bold', marginTop: 5 }]} >{'contacto@mobilcontigo.pe'}</Text>
                </TouchableOpacity>

              </View>
            </View>}
        </ScrollView>

        <View style={{
          width: '100%', height: 80, backgroundColor: 'white', justifyContent: "space-between",
          alignItems: "flex-end", marginTop: 10, flexDirection: "row"
        }}>

          <TouchableOpacity onPress={() => {
            if (global.user.activo) {
              this.props.navigation.navigate("ReglamentoInterno", { tipo: "reglamento" });
            } else {
              this.props.navigation.navigate("Reglamento", { tipo: "reglamento" });
            }

          }}>
            <Text style={[textStyle, { fontWeight: 'bold', fontSize: 14, marginTop: 0, marginBottom: 20, marginLeft: 30 }]} >{'Reglamento Mobil™ Contigo'}</Text>
          </TouchableOpacity>

          <View style={{ width: 70, height: 70, backgroundColor: 'white', marginRight: 30 }}>
            <Image style={{ width: 70, height: 70 }} resizeMode="contain" source={img.logo_mobil_contigo} />
          </View>
        </View>
        {loading}
        <View style={{ height: (new IphoneXFix().isIphoneX()) ? 20 : 0 }}></View>
      </View >
    );

  }
}

export default Ayuda;





