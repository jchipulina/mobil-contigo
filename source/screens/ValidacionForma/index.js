import React, { Component } from "react";
import {
  View,
  Image,
  Alert,
  ImageBackground,
  Text,
  Linking,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import NetInfo from "@react-native-community/netinfo";
import { Navbar, Header, Input, Button, IphoneXFix, BottomSpace, SvgCustom } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import Reactotron from "reactotron-react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { img } from "@media";
import Ionicons from "react-native-vector-icons/Ionicons";
import { openComposer } from 'react-native-email-link'

var { height, width } = Dimensions.get("window");
var dniValues = ["", "", "", "", "", "", "", "", "", "", ""];
var timeOut;
class ValidacionForma extends Component {
  constructor(props) {
    super(props);
    var { params } = this.props.navigation.state;
    this.state = {
      email: String(global.user.email).toLowerCase(),
      celular: global.user.celular,
      tipo: '',
      loading: false,
      netStatus: false,
      userid: '',
    };
  }
  enviarValidacion() {
    this.setState({ loading: true });
    fetch(global.domain + "/api/sent_registration_code/" + global.user.id + "/" + this.state.tipo,
      { method: "POST" }).then(response => response.json())
      .then(responseJson => {

        if (responseJson.status == "OK") {
          this.props.navigation.navigate("Validacion", { tipo: this.state.tipo });
        }

        this.setState({ loading: false });
      }).catch(error => {
        this.setState({ loading: false });
      });
  }
  handleKeyPress({ nativeEvent: { key: keyValue } }) {
    if (keyValue === 'Backspace') {
      this.focusFieldByID(focus - 1);
      this.emptyFieldByID(focus);
    }
  }
  focusFieldByID(num) {
    focus = num;
    switch (num) {
      case 1:
        this.txtField1.focus();
        break;
      case 2:
        this.txtField2.focus();
        break;
      case 3:
        this.txtField3.focus();
        break;
      case 4:
        this.txtField4.focus();
        break;
    }
  }
  emptyFieldByID(num) {
    switch (num) {
      case 1:
        this.setState({ c1: '' });
        break;
      case 2:
        this.setState({ c2: '' });
        break;
      case 3:
        this.setState({ c3: '' });
        break;
      case 4:
        this.setState({ c4: '' });
        break;
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  onChangeText(stext, num) {

    clearTimeout(timeOut);
    timeOut = setTimeout(() => {
      Reactotron.log("setTimeout");
      if (this.state.c1 != '' && this.state.c2 != '' && this.state.c3 != '' && this.state.c4 != '') {
        this.setState({ loading: true })
        this.validarCodigo();
        return
      }
    }, 1000);




    if (num == 5) return;

    if (stext.length == 1) {
      this.focusFieldByID(num);
    }
  }
  componentWillFocus() {
    this.setState({
      c1: "",
      c2: "",
      c3: "",
      c4: ""
    });
  }
  render() {
    var inputSize = (width / 10) - 7;

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var styleBox = {
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
      width: '70%',
      height: 40,
      borderColor: "#ccc",
      borderWidth: 1,
      marginLeft: 1,
      marginRight: 1,
      backgroundColor: "#FFF",
      borderRadius: 10,
      fontFamily: "EMprint-Bold",
      fontSize: 18,
      paddingTop: 0,
      paddingBottom: 0
    };


    var boxStyle = { flexDirection: 'row', marginLeft: 10, backgroundColor: 'transparent' }
    var lineStyle = { width: '94%', marginTop: 0, marginLeft: '6%', marginBottom: 0, backgroundColor: 'transparent', height: 1 };
    var textStyle = { color: 'white', fontFamily: global.fontRegular, fontSize: 19, lineHeight: 20, marginBottom: 0 };
    var iconContainer = { width: 65, height: 50, backgroundColor: 'transparent', justifyContent: "center", alignItems: "center" }
    return (

      <ImageBackground source={img.bg_camion} style={{ flex: 1, width: '100%', backgroundColor: 'rgba(46,80,111,1)' }}>
        <View style={{ flex: 1, backgroundColor: "rgba(46,80,111,0.6)" }}>
          <Header colorArrow={"white"} onBack={() => {
            this.props.navigation.pop();

          }} />
          <View
            style={{ flex: 1, alignItems: "center" }}
          >
            <View
              style={{
                width: width,
                marginTop: (new IphoneXFix().isIphoneX()) ? 0 : 0,
                backgroundColor: "transparent",
                justifyContent: "center",
                alignItems: 'center'
              }}
            >
              <Text style={{ color: 'white', fontWeight: 'bold', textAlign: "center", padding: 20, paddingTop: 0, fontSize: 26, fontFamily: "EMprint", marginBottom: 10 }}>{(this.state.email != "") ? 'Elija la forma de  validación' : 'Validación de usuario'}</Text>

              {this.state.email != "" &&
                <Button
                  title={'Envíame el codigo a \n' + this.state.email}
                  style={{ width: "60%" }}
                  titleStyle={{ fontSize: 15 }}
                  onPress={() => {
                    this.setState({ tipo: "email", loading: true }, this.enviarValidacion);
                  }}
                />
              }
              {this.state.email != "" &&
                <Text style={{ color: 'white', textAlign: "center", fontWeight: 'bold', fontSize: 18, fontFamily: "EMprint", marginBottom: 0 }}>{'ó'}</Text>
              }



              <Button
                title={'Envíame un SMS al \n' + this.state.celular}
                style={{ width: "60%" }}
                titleStyle={{ fontSize: 15 }}
                onPress={() => {
                  this.setState({ tipo: "celular", loading: true }, this.enviarValidacion);
                }}
              />





            </View>




          </View>

          <IphoneXFix />
        </View>
        {loading}
      </ImageBackground >

    );
  }

  processRequest(params) {

  }
  msg(msg) {
    Alert.alert("Importante", msg, [{ text: "Aceptar", onPress: () => { } }], {
      cancelable: false
    });
  }
  validarCodigo() {
    this.setState({ loading: true });
    var codigo = this.state.c1 + this.state.c2 + this.state.c3 + this.state.c4;
    fetch(global.domain + "/api/validate_registration_code/" + global.user.id + "/" + codigo,
      { method: "POST", body: JSON.stringify({}) }).then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        if (responseJson.status == 'OK') {
          var user = global.user;
          user.activo = 1;
          global.user = user;
          global.saveUser();
          this.props.navigation.navigate("App");
        } else {
          this.txtField1.focus();
          Alert.alert("Error", responseJson.mensaje, [{ text: "Aceptar", onPress: () => { } }],
            { cancelable: false }
          );
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }
  volverAEnviar() {
    this.setState({ c1: '', c2: '', c3: '', c4: '', loading: true });
    this.onEnviar();
  }
  onEnviar() {
    this.setState({ loading: true });
    fetch(global.domain + "/api/sent_registration_code/" + global.user.id,
      { method: "POST", body: JSON.stringify({}) }).then(response => response.json())
      .then(responseJson => {
        this.setState({ loading: false });
        if (responseJson.status == 'OK') {
          this.msg("Se envio un nuevo codigo a su correo electronico");
        } else {
          this.txtField1.focus();
          Alert.alert("Error", "Intente más tarde por favor", [{ text: "Aceptar", onPress: () => { } }],
            { cancelable: false }
          );
        }
      }).catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }

}

export default ValidacionForma;
