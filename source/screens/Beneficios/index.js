import React, { Component } from "react";
import {
  AppRegistry,
  ImageBackground,
  Text,
  Dimensions,
  RefreshControl,
  TouchableOpacity,
  ScrollView,
  View,
  Platform,
  Image,
  ActivityIndicator,
  FlatList
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
var counter = 0;
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import Material from "react-native-vector-icons/MaterialIcons";
import LinearGradient from 'react-native-linear-gradient';

import { Navbar, HeaderMobil, Header, Beneficio, Categoria, TabBarHeight, IphoneXFix } from "@components";
import { List, ListItem } from "react-native-elements";
import { Input } from 'react-native-elements';
import { relativeTimeRounding } from "moment";
const fetch = require('react-native-cancelable-fetch');
var fetchProcess = false;
var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');
var scope;
class Beneficios extends Component {
  constructor(props) {
    super(props);
    scope = this;
    this.state = {
      categorias: [],
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      search: "",
      list_search: [],
      isSearch: false
    };
  }
  onRefresh() {
    this.makeRemoteRequest();
    this.getCategorias();
  }
  componentDidMount() {
    this.makeRemoteRequest();
    this.getCategorias();
  }
  updateSearch = search => {
    this.setState({ search }, this.search);
  };
  search = () => {
    if (this.state.search == "") {
      this.setState({ isSearch: false });
      return;
    }
    this.setState({ isSearch: true });
    var url = global.domain + "/api/search/" + this.state.search;
    this.setState({ loading: true });
    if (fetchProcess) { fetch.abort(1); }
    fetchProcess = true;
    fetch(url, { method: "POST" }, 1)
      .then(response => response.json())
      .then(responseJson => {
        fetchProcess = false;
        this.setState({
          list_search: responseJson,
          loading: false,
          refreshing: false,
        });
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };
  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = global.domain + "/api/start_info/" + global.user.id;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        var beneficios = new Array();
        if (responseJson.nuevos_beneficios.length > 0) {
          beneficios = responseJson.nuevos_beneficios[0];
        }
        this.setState({
          data: [beneficios],
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ loading: false });
      });
  };
  getCategorias = () => {
    const { page, seed } = this.state;
    const url = global.domain + "/api/categorias";
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        Reactotron.log(responseJson);
        this.setState({
          categorias: responseJson
        });
      })
      .catch(error => {
        console.error(error);
      });
  };
  update(Beneficios) {
    var tdata = scope.state.data;
    for (var i = 0; i < tdata.length; i++) {
      if (Number(tdata[i].id) == Number(Beneficios)) {
        tdata[i].activo = 1;
        tdata[i].fecha_inicio_Beneficios = moment().format('YYYY-MM-DD hh:mm:ss');
        break;
      }
    }
    scope.setState({ data: tdata });
  }
  _keyExtractor = (item, index) => item.id;
  _renderItem = ({ item }) => (
    <Beneficio beneficio={item} onPress={(beneficio) => {
      this.props.navigation.navigate("BeneficioItem", { beneficio: beneficio });
    }} />
  );
  render() {

    var content_search = (<View></View>);
    var content = (<View></View>);
    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.data.length == 0) {
        content = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: 'black' }}>

            <Image resizeMode='contain' source={img.beneficios_banner} style={{ width: '100%', height: '100%' }} />

          </View>
        );
      } else {
        content = (
          <FlatList
            style={{ paddingLeft: 0, paddingRight: 0 }}
            scrollEnabled={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(l) => l.id}
            data={this.state.data}
            onEndReachedThreshold={20}
            getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
            renderItem={this._renderItem}
          />
        );
      }
    }
    var icoSize = 18;
    var icoStyle = { marginRight: 4 }


    if (this.state.list_search.length > 0) {
      content_search = (
        <FlatList
          style={{ paddingLeft: 0, paddingRight: 0 }}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(l) => l.id}
          data={this.state.list_search}
          onEndReachedThreshold={20}
          getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
          renderItem={this._renderItem}
        />
      );
    }

    return (
      <LinearGradient colors={global.tabGradientColors} style={{ flex: 1 }}>
        <ScrollView refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={() => { this.onRefresh() }} />} style={{ flex: 1, backgroundColor: 'transparent' }}>

          <Header title={"Beneficios"} type={"azul_oscuro"} size={"normal"} marginLeft={25} />
          <View style={{ flex: 1, backgroundColor: 'transparent' }}>
            <View style={{ padding: 25, paddingTop: 0, marginTop: 10, backgroundColor: 'transparent' }}>

              <View style={{ flexDirection: "row" }}>
                <Input
                  containerStyle={{ backgroundColor: "rgba(1,1,1,0.1)", borderRadius: 5, width: '100%', flex: 1, height: 45 }}
                  inputContainerStyle={{ borderBottomColor: "rgba(0,0,0,0)" }}
                  inputStyle={{ fontFamily: global.fontSemiBold, color: '#000', fontSize: 16 }}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  placeholder={'Buscar'}
                  placeholderTextColor={"#8E8E93"}
                  value={this.state.search}
                  maxLength={100}
                  onChangeText={this.updateSearch}
                  leftIcon={
                    <Material
                      style={{ marginRight: 12, paddingLeft: 0, marginLeft: 0 }}
                      name='search'
                      size={24}
                      color={'#8E8E93'}
                    />
                  }
                />
                {(this.state.isSearch) &&
                  <View style={{ width: 40, height: 40, backgroundColor: 'transparent', justifyContent: "center", alignItems: "flex-end" }}>
                    <TouchableOpacity onPress={() => {
                      this.setState({ search: "", isSearch: false, list_search: [] })
                    }} style={{ width: 30, height: 30, justifyContent: "center", alignItems: "center" }}>
                      <Material
                        style={{ marginRight: 0, paddingLeft: 0, marginLeft: 0 }}
                        name='close'
                        size={30}
                        color={global.azul_oscuro}
                      />
                    </TouchableOpacity>
                  </View>
                }

              </View>
              {(this.state.isSearch) &&
                <View style={{ flex: 1 }}>
                  <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 20, marginTop: 15 }}>{(this.state.list_search.length > 0) ? 'Resultados' : 'No se encontraron resultados'}</Text>
                  <View style={{ flex: 1 }}>
                    {content_search}
                  </View>
                </View>
              }
              {(!this.state.isSearch) &&
                <View>
                  <View style={{ flex: 1, backgroundColor: "transparent" }}>{content}</View>

                  <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
                    <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 14 }}>{'CATEGORIAS'}</Text>
                    <TouchableOpacity onPress={() => {
                      if (this.state.categorias.length > 0) {
                        this.props.navigation.navigate("Categorias", { categorias: this.state.categorias });
                      }

                    }}>
                      <Text style={{ fontFamily: global.fontBold, color: global.rojo, fontSize: 14 }}>{'Ver todas (' + this.state.categorias.length + ')'}</Text>
                    </TouchableOpacity>

                  </View>
                  <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} contentContainerStyle={{ flexDirection: "row", paddingRight: 15, marginTop: 5, height: 90 }}>

                    {this.state.categorias.map((value, index) => {
                      return <Categoria color={"rgba(143,143,143,1)"} index={index} showLine={false} title={value.nombre} type="categoria" onPress={() => {
                        this.props.navigation.navigate("BeneficiosListado", { categoria: this.state.categorias[index], categorias: this.state.categorias });
                      }} />
                    })}

                  </ScrollView>

                  <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
                    <Text style={{ fontFamily: global.fontBold, color: global.azul_oscuro, fontSize: 14 }}>{'GRUPOS'}</Text>
                  </View>

                  <Categoria showLine={true} fontSize={18} style={{ width: "100%" }}
                    styleText={{ justifyContent: "space-between", alignItems: "center", marginLeft: 0, paddingLeft: 30, paddingRight: 25 }} title="Para tu Empresa" type="grupo" color={global.azul_claro} onPress={() => {
                      this.props.navigation.navigate("BeneficiosListado", { grupo: 1, categorias: this.state.categorias });
                    }} />
                  <Categoria showLine={true} fontSize={18} style={{ width: "100%" }} styleText={{ justifyContent: "space-between", alignItems: "center", marginLeft: 0, paddingLeft: 30, paddingRight: 25 }} title="Para tus Clientes" type="grupo" color={global.rojo} onPress={() => {
                    this.props.navigation.navigate("BeneficiosListado", { grupo: 2, categorias: this.state.categorias });
                  }} />
                  <Categoria showLine={true} fontSize={18} style={{ width: "100%" }} styleText={{ justifyContent: "space-between", alignItems: "center", marginLeft: 0, paddingLeft: 30, paddingRight: 25 }} title="Para tu Familia y para ti" type="grupo" color={global.azul} onPress={() => {
                    this.props.navigation.navigate("BeneficiosListado", { grupo: 3, categorias: this.state.categorias });
                  }} />
                </View>
              }

            </View>


          </View>
          <TabBarHeight />

        </ScrollView>
      </LinearGradient>
    );
  }
}



export default Beneficios;
