import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Linking,
  View,
  ActivityIndicator,
  FlatList,
  List,
  ScrollView,
} from "react-native";
var { height, width } = Dimensions.get("window");
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import { connect } from "react-redux";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { Navbar, HeaderMobil, Header } from "@components";
import LinearGradient from 'react-native-linear-gradient';
import Image from 'react-native-image-progress';
var moment = require('moment'); //load moment module to set local language
require('moment/locale/es'); //for import moment local language file during the application build
moment.locale('es');

class ComoFuncionaListado extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      uso: [],
      productos: [],
      refreshing: false,
      selectedUso: null,
      selectedUsoObj: { id: 0, nombre: "TODOS" }
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { params } = this.props.navigation.state;
    var categoria = -1;
    if (params.tipo == "SINTETICO") {
      categoria = 1;
    }
    if (params.tipo == "SEMI_SINTETICO") {
      categoria = 2
    }
    if (params.tipo == "MINERAL_PREMIUM") {
      categoria = 3;
    }
    if (params.tipo == "MINERAL_ESTANDAR") {
      categoria = 4;
    }
    const url = global.domain + "/api/productos/" + categoria;
    this.setState({ loading: true });
    fetch(url, { method: "POST" })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          data: responseJson.productos,
          productos: responseJson.productos,
          uso: responseJson.uso,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  };
  _keyExtractor = (item, index) => item.id;
  _renderItem = ({ item }) => (
    <MyListItem producto={item} />
  );
  selectedUso() {
    if (Number(this.state.selectedUsoObj.id) != 0) {
      var newdata = new Array();
      for (var i = 0; i < this.state.productos.length; i++) {
        if (Number(this.state.productos[i].uso) == Number(this.state.selectedUsoObj.id)) {
          newdata.push(this.state.productos[i]);
        }
      }
      this.setState({ data: newdata });
    } else {
      this.setState({ data: this.state.productos });
    }
  }
  render() {

    if (this.state.loading) {
      content = (
        <ActivityIndicator
          animating={true}
          color="#000"
          size="large"
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            height: 80
          }}
        />
      );
    } else {
      if (this.state.data.length == 0) {
        content = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          </View>
        );
      } else {
        content = (

          <View style={{ backgroundColor: "transparent", flex: 1 }}>
            <FlatList
              contentContainerStyle={{ paddingTop: 15 }}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(l) => l.id}
              data={this.state.data}
              onEndReachedThreshold={20}
              getItemLayout={(data, index) => ({ length: 40, offset: 40 * index, index })}
              renderItem={this._renderItem}
            />
          </View>

        );
      }
    }

    renderSeparator = () => {
      return (
        <View
          style={{
            height: 1,
            width: "86%",
            backgroundColor: "#CED0CE",
            marginLeft: "14%"
          }}
        />
      );
    };
    const { params } = this.props.navigation.state;
    var titulo = "";
    if (params.tipo == "SINTETICO") {
      titulo = "Sintético";
    }
    if (params.tipo == "SEMI_SINTETICO") {
      titulo = "Semi sintético";
    }
    if (params.tipo == "MINERAL_PREMIUM") {
      titulo = "Mineral premium";
    }
    if (params.tipo == "MINERAL_ESTANDAR") {
      titulo = "Mineral estándar";
    }
    var id_selected = -1;
    if (this.state.selectedUsoObj != null) {
      id_selected = this.state.selectedUsoObj.id;
    }
    return (
      <View style={{ flex: 1 }}>
        <Header showTooltip={true} tipo={params.tipo} onBack={() => {
          this.props.navigation.goBack();
        }} titleArrow={titulo} styleTitleArrow={{ fontSize: 25, lineHeight: 25, paddingTop: 8, fontWeight: 'bold' }} />
        <View style={{ width: '94%', marginTop: 10, marginLeft: '6%', marginBottom: 10, backgroundColor: '#D4EEF8', height: 1 }}></View>
        <View style={{ height: 40 }}>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} contentContainerStyle={{ paddingLeft: 30, flexDirection: "row", paddingRight: 15, marginTop: 5 }}>
            {this.state.productos.length > 0 &&
              <Uso title={"TODOS"} id={0} selected={(id_selected == 0) ? true : false}
                onPress={(id) => {
                  this.setState({ selectedUsoObj: { id: 0, nombre: "TODOS" } }, this.selectedUso);
                }} />
            }
            {this.state.uso.map((uso, index) => {
              return <Uso title={uso.nombre} id={uso.id} selected={(id_selected == uso.id) ? true : false}
                onPress={(id) => {
                  if (id_selected == uso.id) {
                    this.setState({ selectedUsoObj: null });
                    return
                  }
                  for (var i = 0; i < this.state.uso.length; i++) {
                    if (id == this.state.uso[i].id) {
                      Reactotron.log("index " + index);
                      Reactotron.log(this.state.uso[i]);
                      this.setState({ selectedUsoObj: this.state.uso[i] }, this.selectedUso);
                      break;
                    }
                  }
                }} />
            })}
          </ScrollView>
        </View>
        <View style={{ flex: 1, width: '100%', paddingLeft: 30, paddingRight: 30, marginTop: 0, backgroundColor: 'transparent' }}>
          {content}
        </View>
      </View>
    );
  }
}
class Uso extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var itemTopContentStyle = { marginRight: 40, alignItems: "center" };
    var itemTopContentLabelStyle = { fontFamily: global.fontBold, backgroundColor: "transparent", color: "rgba(148,174,212,1)", fontSize: 13, paddingBottom: 0 };
    var colorText = "rgba(148,174,212,1)";
    if (this.props.selected) {
      colorText = global.azul_oscuro;
    }
    return (
      <TouchableOpacity onPress={() => {
        this.props.onPress(this.props.id);
      }} style={[itemTopContentStyle, this.props.style]}>

        <Text style={[itemTopContentLabelStyle, { color: colorText }]}>{this.props.title}</Text>
        {this.props.selected &&
          <View style={{ width: '60%', height: 4, backgroundColor: global.azul_oscuro, borderRadius: 10, marginTop: 6 }}></View>
        }
      </TouchableOpacity>
    );
  }
}
class MyListItem extends React.PureComponent {
  render() {
    var descripcion = this.props.producto.descripcion;
    var tipo = this.props.producto.tipo;
    var presentacion = this.props.producto.presentacion;
    var presentaciones = new Array();
    if (presentacion != "" && presentacion != null) {
      presentaciones = presentacion.split(",");
    }

    return (
      <View>
        <View style={{ flexDirection: 'row', marginBottom: 20 }}>
          <View style={{ width: 70, height: 70, backgroundColor: 'transparent' }}>
            <Image indicator={ActivityIndicator} resizeMode='contain' style={{ width: 70, height: 70 }}
              source={{ uri: global.urlimg + this.props.producto.imagen }} />
          </View>
          <View style={{ flex: 1, marginLeft: 10, marginTop: 10, paddingRight: 10 }}>
            <Text style={{
              color: global.azul_oscuro, textAlign: 'left', fontFamily: global.fontBold
            }}>
              {descripcion}
            </Text>
            {(tipo != "" && tipo != null) &&
              <Text style={{ color: global.azul, textAlign: 'left', fontFamily: global.fontBold }}>
                {tipo}
              </Text>
            }
            {presentaciones.map((item) => {
              return <Text style={{ color: '#7584A4', textAlign: 'left', fontFamily: global.fontRegular, fontWeight: '500' }}>
                {item}
              </Text>
            })}


          </View>

        </View>
      </View>
    )
  }
}

export default ComoFuncionaListado;
