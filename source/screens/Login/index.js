import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import React, { Component } from "react";
//import { LoginButton } from 'react-native-fbsdk';
import {
  LoginManager,
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import {
  View,
  Alert,
  ImageBackground,
  Text,
  TouchableOpacity,
  Dimensions,
  Platform,
  ActivityIndicator
} from "react-native";
import OneSignal from 'react-native-onesignal';
import { connect } from "react-redux";
import NetInfo from "@react-native-community/netinfo";
import { img } from "@media";
import { Navbar, Header, Button, Input, IphoneXFix, SvgCustom } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import Reactotron from "reactotron-react-native";
//import { TouchableOpacity } from 'react-native-gesture-handler';

//  scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
GoogleSignin.configure({
  scopes: ['email', 'profile'],
  webClientId: '801243029634-cai1snro2od1aeo8i94aab06ftplg2i2.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  accountName: '', // [Android] specifies an account name on the device that should be used
});
// iosClientId: 'com.googleusercontent.apps.682205676816-ckbst4v67tr9kensioijk4knbrfms97e', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)

var { height, width } = Dimensions.get("window");
var dniValues = ["", "", "", "", "", "", "", "", "", "", ""];
var scope;

global.notification_push_id = '';
global.notification_user_id = '';

class Login extends Component {
  constructor(props) {
    super(props);
    scope = this;
    //10052130552
    //testtest
    //20567164595
    //mobil12345
    this.state = {
      ruc: "",
      password: "",
      loading: false,
      netStatus: false,
    };
    OneSignal.init("e35af3c3-387b-4fa7-8c6a-2aa81538a308");
    OneSignal.addEventListener('ids', this.onIds);
    Reactotron.log("OneSignal");
  }
  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }
  onIds(device) {
    global.notification_push_id = device.pushToken;
    global.notification_user_id = device.userId;
  }
  componentDidMount() {
    NetInfo.isConnected.addEventListener("change", this.handleConnectionChange);
    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({ netStatus: isConnected });
    });
  }
  //method
  handleConnectionChange = isConnected => {
    this.setState({ netStatus: isConnected });
    Reactotron.log(`is connected: ${this.state.netStatus}`);
  };
  onChangeText(stext, num) {

  }

  getDNI() {
    console.warn(this.txtField1.text);
  }
  render() {
    var inputSize = (width / 10) - 7;

    var loading = <View />;
    if (this.state.loading) {
      loading = (
        <View
          style={{
            width: "100%",
            height: "100%",
            position: "absolute",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0,0,0,0.5)"
          }}
        >
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      );
    }

    var h = 0;
    if (Platform.OS === "android") {
      h = 24;
    }
    return (
      <KeyboardAwareScrollView bounces={false} style={{ flex: 1, width: width, height: height, backgroundColor: 'rgba(17,17,17,1)' }}>
        <ImageBackground source={img.bg_porshe} style={{ flex: 1, width: width, height: height - h, backgroundColor: '#000' }}>
          <View style={{ flex: 1, alignItems: "center" }}>
            <SvgCustom style={{ marginTop: (new IphoneXFix().isIphoneX()) ? 90 : 60 }} width="100" height="150" source={"logo_white"} />
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: "transparent",
              justifyContent: "center",
              alignItems: 'center'
            }}
          >

            <Input placeholder={'RUC ó DNI'}
              value={this.state.ruc}
              invert={true}
              maxLength={11}
              onChange={ruc => {
                this.setState({ ruc });
              }} leftIcon={
                <SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_user_white"} />
              } />
            <Input placeholder={'Contraseña'}
              password={true}
              invert={true}
              value={this.state.password}
              maxLength={20}
              onChange={password => {
                this.setState({ password });
              }} leftIcon={
                <SvgCustom style={{ margin: 0, padding: 0 }} width="20" height="20" source={"ico_candado"} />
              } />
            <Button
              title="Iniciar Sesión"
              onPress={() => {
                this.onLogin();
              }}
            />
            <View style={{ height: '7%' }}></View>
            {
              this.state.social_login &&
              <Button
                facebook={true}
                title="Entrar con Facebook"
                onPress={() => {
                  this.onLoginWidthFacebook();
                }}
              />
            }
            {
              this.state.social_login &&
              <Button
                google={true}
                title="Entrar con Google"
                onPress={() => {
                  this.onLoginWidthGoogle();
                }}
              />
            }
          </View>
          <View style={{ width: "100%", backgroundColor: "transparent", alignItems: 'center', marginTop: 25, marginBottom: 50 }}>
            <View style={{ width: "70%", flexDirection: 'row', justifyContent: 'center' }}>
              <Text style={{ color: 'white' }}>{'¿Nuevo en Mobil™ Contigo? '}</Text>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('Registro', { isRegister: true });
              }} style={{ justifyContent: "center", alignItems: "center" }}>
                <Text style={{ color: global.rojo }}>{' Crear una cuenta'}</Text>
                <View style={{ width: "97%", height: 1, backgroundColor: global.rojo }}></View>
              </TouchableOpacity>

            </View>
            <View style={{ width: "70%", flexDirection: 'row', justifyContent: 'center', paddingBottom: 10 }}>
              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('ForgotPassword', { paso: 1 });
              }} style={{ justifyContent: "center", alignItems: "center" }}>
                <Text style={{ color: global.rojo, marginTop: 25 }}>{'Olvidé mi contraseña'}</Text>
                <View style={{ width: "100%", height: 1, backgroundColor: global.rojo }}></View>
              </TouchableOpacity>

            </View>
          </View>
          {loading}
        </ImageBackground >
      </KeyboardAwareScrollView >


    );
  }
  //<IphoneXFix />

  onLoginWidthGoogle() {
    this.googleSignIn();
  }

  signIn = async () => {
    try {
      console.warn("try")
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.warn("todo bien !")
    } catch (error) {
      console.warn("error !")
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.warn("*****1")
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.warn("*****2")
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {

        console.warn("*****3")
        // play services not available or outdated
      } else {
        console.warn(error)
        // some other error happened
      }
    }
  }
  googleSignIn = async () => {

    //const isSignedIn = await GoogleSignin.isSignedIn();
    //Reactotron.log("isSignedIn " + isSignedIn);
    try {

      console.warn("hasPlayService  !!!!!");
      await GoogleSignin.hasPlayServices();
      console.warn("*1 __");
      const userInfo = await GoogleSignin.signIn();
      console.warn("*2 __");


      this.setState({ userInfo });

      console.warn("2");

      fetch(global.domain + "/api/login_google", {
        method: "POST",
        body: JSON.stringify({ googleid: userInfo.user.id })
      }).then(response => response.json())
        .then(responseJson => {

          Reactotron.log(responseJson);
          Reactotron.log("OneSignal");

          this.setState({ loading: false });
          if (responseJson.response == "NO_EXISTE") {
            this.props.navigation.navigate("Registro", { isRegister: true, googleData: userInfo });
          } else if (responseJson.response == "EXISTE") {
            this.processUser(responseJson);
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({ loading: false });
        });


    } catch (error) {
      Reactotron.log(error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        Reactotron.log("SIGN_IN_CANCELLED");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        Reactotron.log("IN_PROGRESS");
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Reactotron.log("PLAY_SERVICES_NOT_AVAILABLE");
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  onLoginWidthFacebook() {

    LoginManager.logInWithPermissions(['public_profile,email']).then(
      function (result) {
        Reactotron.log(result);
        if (result.isCancelled) {
          Reactotron.log('Login was cancelled');
        } else {
          Reactotron.log('Login SUCCEES');
          scope.setState({ loading: true });
          scope.FBGraphRequest('id, first_name, last_name,email, picture.type(large)', scope.FBLoginCallback);
        }
      },
      function (error) {
        Reactotron.log('Login failed with error: ' + error);
      }
    );
  }

  async FBGraphRequest(fields, callback) {
    const accessData = await AccessToken.getCurrentAccessToken();
    const infoRequest = new GraphRequest('/me', {
      accessToken: accessData.accessToken,
      parameters: {
        fields: { string: fields }
      }
    }, callback.bind(this));
    new GraphRequestManager().addRequest(infoRequest).start();
  }
  async FBLoginCallback(error, result) {
    Reactotron.log("FBLoginCallback");
    if (error) {
      Reactotron.log("ERROR");
    } else {
      Reactotron.log(result);
      fetch(global.domain + "/api/login_fb", {
        method: "POST",
        body: JSON.stringify(result)
      }).then(response => response.json())
        .then(responseJson => {
          this.setState({ loading: false });
          if (responseJson.response == "NO_EXISTE") {
            this.props.navigation.navigate("Registro", { isRegister: true, facebookData: result });
          } else if (responseJson.response == "EXISTE") {
            this.processUser(responseJson);
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({ loading: false });
        });
    }
  }
  processUser(responseJson) {

    Reactotron.log(responseJson);

    if (responseJson.response == "EXISTE") {

      Reactotron.log(responseJson);
      //  return;
      var user = {};
      user.id = responseJson.id;

      user.ruc = responseJson.ruc;
      user.password = "";
      user.celular = responseJson.celular;
      user.email = responseJson.email;
      user.nombres = responseJson.nombres;
      user.apellidos = responseJson.apellidos;
      user.activo = responseJson.activo;
      user.imagen = responseJson.imagen;
      user.fbid = responseJson.fbid;
      user.googleid = responseJson.googleid;

      global.user = user;
      global.saveUser();
      if (global.onUpdateUserData != null) {
        global.onUpdateUserData();
      }
      this.props.navigation.navigate("Bienvenido", { fromRegister: false });
    } else {
      Alert.alert(
        "Error",
        "Usuario y/o Contraseña errados.",
        [{ text: "Aceptar", onPress: () => { } }],
        { cancelable: false }
      );
    }
  }
  processRequest(params) {
    global.super_user = false;
    fetch(global.domain + "/api/login", {
      method: "POST",
      body: JSON.stringify(params)
    }).then(response => response.json())
      .then(responseJson => {

        //Reactotron.log(responseJson);
        this.setState({ loading: false });
        this.processUser(responseJson);
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
      });
  }
  onLogin() {
    //var sRUC = this.state.ruc;
    var params = {};
    params.ruc = this.state.ruc;
    params.pass = this.state.password;
    params.notification_push_id = global.notification_push_id;
    params.notification_user_id = global.notification_user_id;
    params.os = Platform.OS;
    /*
    if (params.ruc.length < 11) {
      Alert.alert(
        "Importante",
        "Debes ingresar tu número de RUC.",
        [{ text: "Aceptar", onPress: () => {} }],
        { cancelable: false }
      );
      return;
    }*/
    //let numbers = "0123456789";
    //var isnum = /^\d+$/.test(params.ruc);
    /*
    if (!isnum) {
      Alert.alert(
        "Importante",
        "El número RUC debe contener solo numeros.",
        [{ text: "Aceptar", onPress: () => {} }],
        { cancelable: false }
      );
      return;
    }*/
    this.setState({ loading: true });

    //NetInfo.isConnected.fetch().then(isConnected => {
    if (this.state.netStatus) {
      this.processRequest(params);
    } else {
      Alert.alert(
        "Error",
        "Sin conexión a internet.",
        [{ text: "Aceptar", onPress: () => { } }],
        { cancelable: false }
      );

      this.setState({ loading: false });
    }
    //});
  }
}

export default Login;
