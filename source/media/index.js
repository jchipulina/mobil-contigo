
import Reactotron from "reactotron-react-native"

const dirImg = "./img/"

/*	check_circle: require(dirImg + "check_circle.svg"),
	ico_info_blue: require(dirImg + "ico_info_blue.svg"),
	ico_block: require(dirImg + "ico_block.svg"),
	ico_change: require(dirImg + "ico_change.svg"),
	ico_email_white: require(dirImg + "ico_email_white.svg"),
	call_outline_white: require(dirImg + "call-outline_white.svg"),
	call_outline_black: require(dirImg + "call-outline_black.svg"),
	logo_mobil_home: require(dirImg + "mobil_contigo_home.svg"),
	check: require(dirImg + "check.svg"),
	circle: require(dirImg + "circle.svg"),
	logo_white: require(dirImg + "logo_white.svg"),
	
	ico_email_black: require(dirImg + "ico_email_black.svg"),
	ico_ruc_black: require(dirImg + "ico_ruc_black.svg"),
	ico_user_black: require(dirImg + "ico_user_black.svg"),
	ico_candado_black: require(dirImg + "ico_candado_black.svg"),
	ico_candado: require(dirImg + "ico_candado.svg"),
	ico_email: require(dirImg + "ico_email.svg"),
	ico_info: require(dirImg + "ico_info.svg"),
	ico_user: require(dirImg + "ico_user.svg"),
	ico_user_white: require(dirImg + "ico_user_white.svg"),
	ico_call_outline: require(dirImg + "call-outline.svg"),
	ico_ayuda: require(dirImg + "ayuda.svg"),
	ico_ruc: require(dirImg + "ico_ruc.svg"),
	ico_beneficios: require(dirImg + "ico-beneficios.svg"),
	ico_logo: require(dirImg + "ico-logo.svg"),
	ico_canjes: require(dirImg + "ico-canjes.svg"),
	ico_metas: require(dirImg + "ico-metas.svg"),
	ico_profile: require(dirImg + "ico-profile.svg"),
	ico_pen: require(dirImg + "ico_user_image.svg"),*/


export const img = {
	bg_camion_oscuro: require(dirImg + "bg_camion_oscuro.png"),
	bg_camion: require(dirImg + "bg_camion.png"),
	bg_porshe: require(dirImg + "bg_porshe.jpg"),
	producto_lista_4: require(dirImg + "producto_lista_4.png"),
	producto_lista_3: require(dirImg + "producto_lista_3.png"),
	producto_lista_2: require(dirImg + "producto_lista_2.png"),
	producto_lista_1: require(dirImg + "producto_lista_1.png"),
	ico_canjes_img: require(dirImg + "ico_canjes.png"),
	ico_beneficios_img: require(dirImg + "ico_sorpresa.png"),
	ico_compras_img: require(dirImg + "ico_compras.png"),
	ico_cuenta_img: require(dirImg + "ico_cuenta.png"),
	producto: require(dirImg + "producto.png"),
	ico_user_image: require(dirImg + "user_img_ico.png"),
	icon_google: require(dirImg + "icon_google.png"),
	logo_mobil_contigo: require(dirImg + "logo_mobil_contigo.png")

}
