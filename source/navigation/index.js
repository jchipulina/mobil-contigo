
import { createSwitchNavigator, createStackNavigator, createAppContainer, StackNavigator, TabNavigator, DrawerNavigator, NavigationActions, createBottomTabNavigator } from 'react-navigation';


import { View, Text, ActivityIndicator, StatusBar, Dimensions, Image } from "react-native";
import { Platform, AsyncStorage, TouchableWithoutFeedback } from "react-native"
import Home from "./Home"
import Login from "./Login"
import ForgotPassword from "./ForgotPassword"
import Notificaciones from "./Notificaciones"
import PerfilSelector from "./PerfilSelector"
import Grupos from "./Grupos"
import CanjeItem from "./CanjeItem"
import Bienvenido from "./Bienvenido"
import Beneficios from "./Beneficios"
import Codigo from "./Codigo"
import BeneficiosListado from "./BeneficiosListado"
import Promociones from "./Promociones"
import Compras from "./Compras"
import CompraDetalle from "./CompraDetalle";
import Canjes from "./Canjes"
import Ayuda from "./Ayuda"
import Validacion from "./Validacion"
import ValidacionForma from "./ValidacionForma"
import Registro from "./Registro"
import Terminos from "./Terminos"
import React, { Component } from "react"
import { DrawerContent, SvgCustom } from "@components";
import BeneficioItem from "./BeneficioItem"
import PromocionesItem from "./PromocionesItem"
import ComoFunciona from "./ComoFunciona"
import ComoFuncionaListado from "./ComoFuncionaListado"
import Reactotron from 'reactotron-react-native'
import Categorias from "./Categorias"
import Icon from "react-native-vector-icons/FontAwesome";
import ViewOverflow from 'react-native-view-overflow';
import { LoginButton } from 'react-native-fbsdk';
import { img } from "@media";
Reactotron
	.configure() // controls connection & communication settings
	.useReactNative() // add all built-in react native plugins
	.connect() // let's connect!


global.domain = 'http://www.mobilcontigo.pe/control';
//global.domain = 'http://www.clubmobildelvac.com';
global.yellowColor = "#f8d12c";
global.urlimg = "http://www.mobilcontigo.pe/control/assets/uploads/files/";
global.user = null;
global.tabGradientColors = ['rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(255,255,255,0.2)', 'rgba(0,0,0,0.5)'];
global.tabGradientColorsGray = ['rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(242,242,242,1)', 'rgba(245,245,245,0.7)'];

//242	242	242	

if (Platform.OS === 'ios') {
	global.fontRegular = 'EMprint';
	global.fontBold = 'EMprint-Bold';
	global.fontSemiBold = 'EMprint-Semibold';
} else {
	global.fontRegular = 'EMprint-Regular';
	global.fontBold = 'EMprint-Bold';
	global.fontSemiBold = 'EMprint-Semibold';
}

global.saveUser = function () {
	AsyncStorage.setItem("user", JSON.stringify(global.user)).then((value) => {

	});
}
global.deleteUser = function () {
	AsyncStorage.removeItem("user").then((value) => {
		global.user = null;
	});
}
function isIphoneX() {
	const dimen = Dimensions.get('window');
	return (
		Platform.OS === 'ios' &&
		!Platform.isPad &&
		!Platform.isTVOS &&
		(dimen.height >= 812 || dimen.width >= 812)
	);
}
/*
    ico_cuenta: require(dirImg + "ico-cuenta.svg"),
	ico_beneficios: require(dirImg + "ico-beneficios.svg"),
	ico_logo: require(dirImg + "ico-logo.svg"),
	ico_canjes: require(dirImg + "ico-canjes.svg"),
	ico_metas: require(dirImg + "ico-metas.svg"), */
const rutas = {
	Home: {
		screen: createBottomTabNavigator({
			Compras: { screen: Compras },
			Canjes: { screen: Canjes },
			Home: {
				screen: Home
			},
			Beneficios: { screen: Beneficios },
			Profile: { screen: Registro },

		},
			{
				defaultNavigationOptions: ({ navigation }) => ({
					tabBarIcon: ({ focused, horizontal, tintColor }) => {
						const { routeName } = navigation.state;
						let IconComponent = Icon;
						var focusColor = global.azul;
						let iconName;
						var testBg = "white";
						var styleIco = { width: "100%", height: 50, backgroundColor: 'white', justifyContent: 'center', alignItems: "center" };
						var styleLabel = { fontFamily: global.fontBold, fontSize: 10, marginTop: 3, color: focused ? focusColor : '#000' }
						/*tintColor: focused ? focusColor : global.azul_oscuro, */
						if (routeName === 'Home') {
							return <View style={styleIco}>
								<SvgCustom width="35" height="35" source={"ico_logo"} />
							</View>
						} else if (routeName === 'Compras') {
							return <View style={styleIco}>
								<Image resizeMode="contain" style={{ tintColor: focused ? focusColor : global.azul_oscuro, width: 25, height: 23, marginBottom: 2, backgroundColor: testBg }}
									source={img.ico_compras_img} />
								<Text style={styleLabel}>{'Compras'}</Text>
							</View>
						} else if (routeName === 'Beneficios') {
							return <View style={styleIco}>

								<Image resizeMode="contain" style={{ tintColor: focused ? focusColor : global.azul_oscuro, width: 25, height: 23, marginBottom: 2, backgroundColor: testBg }}
									source={img.ico_beneficios_img} />
								<Text style={styleLabel}>{'Beneficios'}</Text>
							</View>
						} else if (routeName === 'Canjes') {
							return <View style={styleIco}>
								<Image resizeMode="contain" style={{ tintColor: focused ? focusColor : global.azul_oscuro, width: 25, height: 24, marginBottom: 2, backgroundColor: testBg }}
									source={img.ico_canjes_img} />
								<Text style={styleLabel}>{'Canjes'}</Text>
							</View>
						} else if (routeName === 'Profile') {
							return <View style={styleIco}>

								<Image resizeMode="contain" style={{ tintColor: focused ? focusColor : global.azul_oscuro, width: 25, height: 23, marginBottom: 2, backgroundColor: testBg }}
									source={img.ico_cuenta_img} />
								<Text style={styleLabel}>{'Cuenta'}</Text>
							</View>
						}


						return <View style={{ width: 30, height: 30, backgroundColor: 'white' }}>
							<SvgUri width="30" height="30" source={img.ico_logo} />
						</View>
					},
				}),
				tabBarComponent: (props) => {
					const {
						navigation: { state: { index, routes } },
						style,
						activeTintColor,
						inactiveTintColor,
						renderIcon,
						jumpTo
					} = props;

					var radius = 32;
					return (
						<ViewOverflow style={{
							flexDirection: 'row',
							width: '100%',
							paddingLeft: 0,
							paddingRight: 0,
							marginBottom: 0,
							position: 'absolute',
							bottom: 0,
							justifyContent: "center",
							alignItems: 'center',
							backgroundColor: 'transparent',
							borderRadius: 0
						}}><View style={{
							backgroundColor: "white", flexDirection: "row", borderTopLeftRadius: radius, borderTopRightRadius: radius, paddingLeft: '5%',
							paddingRight: '5%'
						}}>
								{
									routes.map((route, idx) => (
										<ViewOverflow
											key={route.key}
											style={{
												flex: 1,
												paddingTop: 10,
												paddingLeft: 0,
												paddingRight: 0,
												paddingBottom: (isIphoneX()) ? 25 : 10,
												alignItems: 'center',
												justifyContent: 'center',
												backgroundColor: "transparent"
											}}
										>
											<TouchableWithoutFeedback onPress={() => {
												if (route.key == "Canjes") {
													Reactotron.log("Canjes.update");
													Canjes.update();
												}
												if (route.key == "Home") {
													Reactotron.log("Home.update");
													Home.update();
												}
												if (route.key == "Compras") {
													Reactotron.log("Compras.update");
													Compras.update();
												}
												jumpTo(route.key)
											}}>
												{renderIcon({
													route,
													focused: index === idx,
													tintColor: index === idx ? activeTintColor : inactiveTintColor
												})}
											</TouchableWithoutFeedback>
										</ViewOverflow>
									))
								}
							</View>
						</ViewOverflow >
					);
				},
				tabBarOptions: {
					indicatorStyle: { backgroundColor: 'transparent' },
					showIcon: true,
					showLabel: true,
					tabBarVisible: true,
					iconStyle: { margin: 0, padding: 0 },
					activeTintColor: '#004155',
					inactiveTintColor: '#FFFFFF',
					style: { backgroundColor: 'transparent' }
				},
			})
	},
	BeneficiosListado: { screen: BeneficiosListado },
	Ayuda: { screen: Ayuda },
	CompraDetalle: { screen: CompraDetalle },
	Promociones: { screen: Promociones },
	PromocionesItem: { screen: PromocionesItem },
	ComoFunciona: { screen: ComoFunciona },
	ComoFuncionaListado: { screen: ComoFuncionaListado },
	Grupos: { screen: Grupos },
	Codigo: { screen: Codigo },
	Notificaciones: { screen: Notificaciones },
	Categorias: { screen: Categorias },
	BeneficioItem: { screen: BeneficioItem },
	CanjeItem: { screen: CanjeItem },
	Compras: { screen: Compras },
	Canjes: { screen: Canjes },
	ReglamentoInterno: { screen: Terminos },

}
/**/
const rutas_login = {
	Login: { screen: Login },
	Terminos: { screen: Terminos },
	Reglamento: { screen: Terminos },
	Login: { screen: Login },
	Bienvenido: { screen: Bienvenido },
	CodigoRegistro: { screen: Codigo },
	ForgotPassword: { screen: ForgotPassword },
	Registro: { screen: Registro },
	Validacion: { screen: Validacion },
	ValidacionForma: { screen: ValidacionForma },
	AyudaExterna: { screen: Ayuda },

}
const rutas_validacion = {
	Validacion: { screen: Validacion },
	AyudaExterna: { screen: Ayuda },
	ComoFuncionaExterna: { screen: ComoFunciona },
	ComoFuncionaListadoExterna: { screen: ComoFuncionaListado },
}

const opciones = {
	initialRouteName: "Terminos",
	lazy: true,
	headerMode: 'none',
	cardStyle: { backgroundColor: 'black', opacity: 1 }
}
/*cardStyle: { //Does not work
            backgroundColor: 'transparent',
        },*/
class AuthLoadingScreen extends React.Component {
	constructor(props) {
		super(props);
		this._bootstrapAsync();
	}
	_bootstrapAsync = async () => {
		AsyncStorage.getItem("user").then(value => {
			global.user = JSON.parse(value);
			if (global.user != null) {
				//this.props.navigation.navigate((!global.user.activo) ? 'Auth' : 'App');
				if (!global.user.activo) {
					global.user = null;
					this.props.navigation.navigate('Auth');
				} else {
					this.props.navigation.navigate('App');
				}

			} else {
				this.props.navigation.navigate('Auth');
			}
		});
	};
	render() {
		return (
			<View>
				<ActivityIndicator />
			</View>
		);
	}
}


const AppStack = createStackNavigator(rutas, { lazy: true, headerMode: 'none' });
const AuthStack = createStackNavigator(rutas_login, { lazy: true, headerMode: 'none' });

export default createAppContainer(createSwitchNavigator(
	{
		AuthLoading: AuthLoadingScreen,
		App: AppStack,
		Auth: AuthStack,
	},
	{
		initialRouteName: 'AuthLoading',
	}
));
