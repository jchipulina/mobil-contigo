import React, {Component} from "react";
import { Bienvenido as Pantalla } from "@screens";
import Icon from 'react-native-vector-icons/FontAwesome';

import { img } from "@media"

export default class sBienvenido extends Component {
	//console.warn(tintColor);
	static navigationOptions = ({ navigation }) => ({ title: '', tabBarLabel: 'Perfil', tabBarIcon: ({ tintColor }) => (
      <Icon name={'user'} size={22} color={tintColor} />
     
    
      
	  ),
  	});
  	
	render() { return <Pantalla navigation={this.props.navigation}/> }
}
