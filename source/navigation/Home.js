import React, { Component } from "react";
import { Home } from "@screens";
import { NavigationActions } from 'react-navigation';
import Reactotron from "reactotron-react-native";

export default class sHome extends Component {

	constructor(props) {
		super(props);
		this.props.navigation.setParams({ tabBarVisible: true });
	}
	static update() {
		Home.update();
	}
	//static navigationOptions = ({ navigation }) => ({ tabBarVisible: (navigation.state.params && navigation.state.params.tabBarVisible) === true, gesturesEnabled: false });
	/*return <Home navigation={this.props.navigation} checkUser={(activo) => {
			this.props.navigation.setParams({ tabBarVisible: activo });
		}} />*/
	render() {

		return <Home navigation={this.props.navigation}/>
	}
}
