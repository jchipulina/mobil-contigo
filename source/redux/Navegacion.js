const tipos = {
	IR: "IR"
}

export const acciones = {
	ir: pantalla => ({
		type: tipos.NAVEGAR, 
		pantalla: pantalla
	})
}

const estadoInicial = {
	pantalla: "PORTADA"
}

export const reductor = ( estado = estadoInicial, accion ) => {
    const { tipo } = accion;

    switch( tipo ){
    	case tipos.IR:{
			return estado;
    	}
    	default: {
            return estado;
        }
    }
}
