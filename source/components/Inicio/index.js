import React, { Component } from "react"
import {
	View,
	Text,
	Image,
} from "react-native"
import { img } from "@media"
import estilo from "./estilo"
//import Reactotron from 'reactotron-react-native';

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class Inicio extends Component{
	constructor(props) {
    	super(props);
  	}

	render() {
		
		const { navigate } = this.props.navigation; 
		
		return (
			<View style={ estilo.contenedor }>
				<View style={ estilo.fondo }>
					<Image style={ estilo.fondo_img } source={ img.fondo2 } />
				</View>
				<View style={ estilo.contenido }>
					<Image source={ img.logo } />
						<Button
						  title='BUTTON WITH RIGHT ICON'
						  onPress={()=>{ 
							// console.warn(this.props);
							 navigate('Login')
							  
						  }}
						/>
				</View>
			</View>
		);
	}
}
