import React, { StyleSheet } from "react-native";

export default StyleSheet.create({
	"contenedor": {
		"flex": 1
	},
	"fondo":{
		position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
	},
	"fondo_img":{
		flex: 1,
		resizeMode: "contain",
		height: undefined, 
		width: undefined
	},
	"contenido":{
		position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'flex-end',
	}
})
