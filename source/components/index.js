import _inicio from './Inicio';
export const Inicio = _inicio;

import _navbar from './Navbar';
export const Navbar = _navbar;

import _header from './Header';
export const Header = _header;

import _button from './Button';
export const Button = _button;

import _input from './Input';
export const Input = _input;

import _iphoneXFix from './IphoneXFix';
export const IphoneXFix = _iphoneXFix;

import _BottomSpace from './BottomSpace';
export const BottomSpace = _BottomSpace;

import _SvgCustom from './SvgCustom';
export const SvgCustom = _SvgCustom;

import _DrawerContent from './DrawerContent';
export const DrawerContent = _DrawerContent;

import _HeaderMobil from './HeaderMobil';
export const HeaderMobil = _HeaderMobil;

import _Beneficio from './Beneficio';
export const Beneficio = _Beneficio;

import _Categoria from './Categoria';
export const Categoria = _Categoria;

import _Accordion from './Accordion';
export const Accordion = _Accordion;

import _TabBarHeight from './TabBarHeight';
export const TabBarHeight = _TabBarHeight;
