import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native"
var { height, width } = Dimensions.get('window');

export default class TabBarHeight extends Component {
	constructor(props) {
		super(props);
	}
	isIphoneX() {
		const dimen = Dimensions.get('window');
		return (
			Platform.OS === 'ios' &&
			!Platform.isPad &&
			!Platform.isTVOS &&
			(dimen.height >= 812 || dimen.width >= 812)
		);
	}
	render() {
		return (
			<View style={{ height: (this.isIphoneX()) ? 30+60 : 10+60 }}></View>
		);
	}
}
