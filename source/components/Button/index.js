import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
var { height, width } = Dimensions.get('window');
import { Button } from 'react-native-elements';

export default class ButtonCutom extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let margin = 5;
		if (this.props.facebook) {
			return (
				<Button
					icon={
						<Icon
							name="facebook-square"
							size={20}
							color="white"
							style={{ marginRight: 10 }}
						/>
					}
					containerStyle={{ borderRadius: 5, margin: margin, width: '70%' }}
					buttonStyle={{ backgroundColor: '#4267B2' }}
					titleStyle={{ fontFamily: 'EMprint-Bold', fontSize: 15 }}
					title={'Entrar con facebook'}
					onPress={() => {
						this.props.onPress();
					}} />
			);
		}
		if (this.props.google) {
			return (
				<Button
					icon={
						<Image style={{ width: 20, height: 20, marginRight: 10 }} source={img.icon_google} />
					}
					containerStyle={{ borderRadius: 5, margin: margin, width: '70%' }}
					buttonStyle={{ backgroundColor: '#FFF' }}
					titleStyle={{ fontFamily: 'EMprint-Bold', color: '#666', fontSize: 15 }}
					title={'Entrar con Google'}
					onPress={() => {
						this.props.onPress();
					}} />
			);
		}
		var opacity = 1;
		if (this.props.disabled) {
			opacity = 0.5;
		}
		return (
			<Button
				containerStyle={[{ borderRadius: 5, margin: margin, width: '70%', opacity: opacity }, this.props.style]}
				disabled={this.props.disabled}
				buttonStyle={{ backgroundColor: global.rojo }}
				titleStyle={[{ fontFamily: 'EMprint-Bold', color: 'white', fontSize: 18 }, this.props.titleStyle]}
				title={this.props.title}
				onPress={() => {
					this.props.onPress();
				}} />
		);
	}
}
