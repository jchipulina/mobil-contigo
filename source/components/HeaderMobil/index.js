import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
var {height, width} = Dimensions.get('window');


export default class HeaderMobil extends Component{
	constructor(props) {
    	super(props);
  	}
	render() {

		return (
			<View style={{ width: width, height: 70, backgroundColor: "white", justifyContent: "center" }}>
         <View style={{ width: width, backgroundColor: 'black', paddingRight:10,
	            flexDirection: "row", justifyContent: "flex-end", alignItems: "center", height: 70}}>
	            <View style={{flex:this.props.flex1,height: 70,flexDirection: "row",paddingLeft:10,backgroundColor:global.yellowColor, alignItems: "center"}}>
	            	   
	            	   <Text style={{ flex:1,fontFamily: "EMprint-Bold",fontSize:12, color: 'black',textAlign:'left'}} >
	            	   {this.props.labelLeft}
	            	   </Text>
	            </View>
            <View style={{	width: 0, height: 0, borderStyle: 'solid',
						    borderRightWidth: 70, borderTopWidth: 70, borderRightColor: 'transparent',
						    borderTopColor: global.yellowColor}}>
			</View>
	            <Text style={{ flex:this.props.flex2,paddingRight: 5, fontFamily: "EMprint-Bold", fontSize: 16, color: 'white',textAlign:'right'}} >
	            	{this.props.title}
	            </Text>
			</View>
        </View>
		);
	}
}
