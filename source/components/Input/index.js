import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	TouchableOpacity,
	Platform
} from "react-native"
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import { img } from "@media";
import { IphoneXFix } from "@components";

export default class InputCutom extends Component {
	constructor(props) {
		super(props);
	}
	focus() {
		this.txtField.focus();
	}
	render() {
		var invert = false;
		if (this.props.invert) {
			invert = this.props.invert;
		}
		/*					<Icon
						style={{ marginRight: 12 }}
						name='user'
						size={24}
						color={(invert) ? 'white' : 'black'}
					/>*/

		//#B5B1AD

		var containerStyle = { backgroundColor: (this.props.password_style) ? "rgba(255,255,255,0.5)" : (invert) ? "rgba(181,177,173,0.9)" : "white", borderRadius: 5, margin: 4, width: '70%', height: 42, justifyContent: "center" };



		//var containerStyle = { backgroundColor: "red", borderRadius: 5, margin: 5, width: '70%', height: 45, justifyContent: "center" };

		//var textStyle = { fontFamily: global.fontRegular, fontWeight: '400', color: (invert) ? 'white' : '#000', fontSize: 15, top: (Platform.OS === "ios") ? 0 : -2 };

		var textStyle = { fontFamily: global.fontRegular, fontWeight: '400', color: (invert) ? 'white' : '#000', fontSize: 15, backgroundColor: "transparent", marginTop: 25 };

		return (
			<Input
				ref={c => (this.txtField = c)}
				keyboardType={(this.props.keyboardType) ? this.props.keyboardType : 'default'}
				containerStyle={[containerStyle, this.props.style]}
				inputContainerStyle={{ borderBottomColor: "rgba(0,0,0,0)" }}
				inputStyle={[textStyle, this.props.textStyle]}
				underlineColorAndroid={'rgba(0,0,0,0)'}
				placeholder={this.props.placeholder}
				placeholderTextColor={(invert) ? "#FFF" : "#000"}
				value={this.props.value}
				maxLength={this.props.maxLength}
				secureTextEntry={this.props.password}
				onChangeText={p => {
					this.props.onChange(p);
				}}
				disabled={this.props.disabled}
				leftIconContainerStyle={{ backgroundColor: 'transparent', marginLeft: 15, marginTop: 25, marginRight: 15 }}
				leftIcon={this.props.leftIcon}
			/>
		);
	}
}
