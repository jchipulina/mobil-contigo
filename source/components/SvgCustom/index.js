import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
var { height, width } = Dimensions.get('window');


import Logo_white from "./logo_white.svg";
import Ico_user_white from "./ico_user_white.svg";
import Ico_candado from "./ico_candado.svg";

import Check_circle from "./check_circle.svg";
import Ico_info_blue from "./ico_info_blue.svg";
import Ico_block from "./ico_block.svg";
import Ico_change from "./ico_change.svg";
import Ico_email_white from "./ico_email_white.svg";
import Call_outline_white from "./call-outline_white.svg";
import Call_outline_black from "./call-outline_black.svg";
import Logo_mobil_home from "./mobil_contigo_home.svg";
import Check from "./check.svg";
import Circle from "./circle.svg";
import Ico_email_black from "./ico_email_black.svg";
import Ico_ruc_black from "./ico_ruc_black.svg";
import Ico_user_black from "./ico_user_black.svg";
import Ico_candado_black from "./ico_candado_black.svg";
import Ico_email from "./ico_email.svg";
import Ico_info from "./ico_info.svg";
import Ico_user from "./ico_user.svg";
import Ico_call_outline from "./call-outline.svg";
import Ico_ayuda from "./ayuda.svg";
import Ico_ruc from "./ico_ruc.svg";
import Ico_beneficios from "./ico-beneficios.svg";
import Ico_logo from "./ico-logo.svg";
import Ico_canjes from "./ico-canjes.svg";
import Ico_metas from "./ico-metas.svg";
import Ico_profile from "./ico-profile.svg";
import Ico_pen from "./ico_user_image.svg";



export default class SvgCustom extends Component {
	constructor(props) {
		super(props);
	}
	render() {

		if (this.props.source == "logo_white") {
			return <Logo_white style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_user_white") {
			return <Ico_user_white style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_candado") {
			return <Ico_candado style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "check_circle") {
			return <Check_circle style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_info_blue") {
			return <Ico_info_blue style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_block") {
			return <Ico_block style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_change") {
			return <Ico_change style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_email_white") {
			return <Ico_email_white style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "call_outline_white") {
			return <Call_outline_white style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "call_outline_black") {
			return <Call_outline_black style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "logo_mobil_home") {
			return <Logo_mobil_home style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "check") {
			return <Check style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "circle") {
			return <Circle style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_email_black") {
			return <Ico_email_black style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_ruc_black") {
			return <Ico_ruc_black style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_user_black") {
			return <Ico_user_black style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_candado_black") {
			return <Ico_candado_black style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_email") {
			return <Ico_email style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_info") {
			return <Ico_info style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_user") {
			return <Ico_user style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_call_outline") {
			return <Ico_call_outline style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_ayuda") {
			return <Ico_ayuda style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_ruc") {
			return <Ico_ruc style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_beneficios") {
			return <Ico_beneficios style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_logo") {
			return <Ico_logo style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_canjes") {
			return <Ico_canjes style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_metas") {
			return <Ico_metas style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_metas") {
			return <Ico_profile style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		if (this.props.source == "ico_pen") {
			return <Ico_pen style={this.props.style} width={this.props.width} height={this.props.height} />
		}
		return (
			<View />
		);
	}
}
