import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
var { height, width } = Dimensions.get('window');
import { IphoneXFix, SvgCustom } from "@components";
import Reactotron from "reactotron-react-native";
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { Tooltip } from "react-native-elements";
export default class Header extends Component {
	constructor(props) {
		super(props);
	}
	render() {

		var marginLeft = 60;
		if (this.props.marginLeft != null) {
			marginLeft = this.props.marginLeft;
		}
		var color;
		if (this.props.type == "azul_claro") {
			color = global.azul_claro;
		}
		if (this.props.type == "azul_oscuro") {
			color = global.azul_oscuro;
		}
		if (this.props.type == "azul") {
			color = global.azul;
		}
		if (this.props.type == "blanco") {
			color = global.blanco;
		}
		var align = "left";
		if (this.props.align != null) {
			align = this.props.align;
		}

		var styleHeaderContentTop = 0;
		if (this.props.onBack != null) {
			styleHeaderContentTop = 10;
		}

		var styleTitle = {
			textAlign: align, fontFamily: global.fontBold,
			fontSize: 30, color: color, backgroundColor: 'transparent', lineHeight: 30
		}
		var styleHeaderContent = {
			backgroundColor: "transparent", marginTop: styleHeaderContentTop, marginLeft: 20, flexDirection: "row"
		}

		var colorArrow = global.azul_oscuro;
		if (this.props.colorArrow != null) {
			colorArrow = this.props.colorArrow;
		}

		var styleTitleArrow = {
			color: global.azul_oscuro, backgroundColor: 'transparent',
			fontFamily: global.fontSemiBold, fontSize: 15, lineHeight: 15
		}
		var contentStyle = { flexDirection: "column", paddingTop: (this.props.onBack != null) ? 10 : 25, justifyContent: "flex-start", backgroundColor: 'transparent' }

		var cellStyle = { flexDirection: "row", paddingBottom: 0, marginBottom: 0, width: "100%", backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }
		var cellTextStyle = { textAlign: "left", width: "33%", fontFamily: global.fontSemiBold, fontSize: 12, color: global.azul_oscuro, backgroundColor: "#ddd", margin: 1, padding: 1 }
		var c1 = { width: "46%" };
		var c2 = { width: "28%", textAlign: "center" };
		var c3 = { width: "28%", textAlign: "center" };
		var c_header = { fontFamily: global.fontBold, fontSize: 13 };
		var table1 = (<View style={{ padding: 5, width: 230, backgroundColor: "white" }}>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1, c_header]} > {'Presentación'}</Text>
				<Text style={[cellTextStyle, c2, c_header]}>{'Galones'}</Text>
				<Text style={[cellTextStyle, c3, c_header]}>{'KMS'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{''}</Text>
				<Text style={[cellTextStyle, c2]}>{'1.00'}</Text>
				<Text style={[cellTextStyle, c3]}>{'2.50'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{' Caja 12 Litros'}</Text><Text style={[cellTextStyle, c2]}>{'3.17'}</Text><Text style={[cellTextStyle, c3]}>{'7.92'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{' Caja 6 Cuartos'}</Text><Text style={[cellTextStyle, c2]}>{'1.50'}</Text><Text style={[cellTextStyle, c3]}>{'3.75'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{' Caja 12 Cuartos'}</Text><Text style={[cellTextStyle, c2]}>{'3.00'}</Text><Text style={[cellTextStyle, c3]}>{'7.50'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{' Caja 4 Galones'}</Text><Text style={[cellTextStyle, c2]}>{'4.00'}</Text><Text style={[cellTextStyle, c3]}>{'10.00'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{' Balde'}</Text><Text style={[cellTextStyle, c2]}>{'5.00'}</Text><Text style={[cellTextStyle, c3]}>{'12.50'}</Text>
			</View>
		</View>)


		var table2 = (<View style={{ padding: 5, width: 230, backgroundColor: "white" }}>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1, c_header]} > {'Presentación'}</Text>
				<Text style={[cellTextStyle, c2, c_header]}>{'Galones'}</Text>
				<Text style={[cellTextStyle, c3, c_header]}>{'KMS'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{''}</Text><Text style={[cellTextStyle, c2]}>{'1.00'}</Text><Text style={[cellTextStyle, c3]}>{'1.50'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Caja 12 Cuartos'}</Text><Text style={[cellTextStyle, c2]}>{'3.00'}</Text><Text style={[cellTextStyle, c3]}>{'4.50'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Caja 4 Galones'}</Text><Text style={[cellTextStyle, c2]}>{'4.00'}</Text><Text style={[cellTextStyle, c3]}>{'6.00'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Balde'}</Text><Text style={[cellTextStyle, c2]}>{'5.00'}</Text><Text style={[cellTextStyle, c3]}>{'7.50'}</Text>
			</View>

		</View>)


		var table3 = (<View style={{ padding: 5, width: 230, backgroundColor: "white" }}>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1, c_header]} > {'Presentación'}</Text>
				<Text style={[cellTextStyle, c2, c_header]}>{'Galones'}</Text>
				<Text style={[cellTextStyle, c3, c_header]}>{'KMS'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{''}</Text><Text style={[cellTextStyle, c2]}>{'1.00'}</Text><Text style={[cellTextStyle, c3]}>{'0.75'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Caja 12 Cuartos'}</Text><Text style={[cellTextStyle, c2]}>{'3.00'}</Text><Text style={[cellTextStyle, c3]}>{'2.25'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Caja 4 Galones'}</Text><Text style={[cellTextStyle, c2]}>{'4.00'}</Text><Text style={[cellTextStyle, c3]}>{'3.00'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Medio Balde'}</Text><Text style={[cellTextStyle, c2]}>{'2.50'}</Text><Text style={[cellTextStyle, c3]}>{'1.88'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Balde'}</Text><Text style={[cellTextStyle, c2]}>{'5.00'}</Text><Text style={[cellTextStyle, c3]}>{'3.75'}</Text>
			</View>
		</View>)


		var table4 = (<View style={{ padding: 5, width: 230, backgroundColor: "white" }}>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1, c_header]} > {'Presentación'}</Text>
				<Text style={[cellTextStyle, c2, c_header]}>{'Galones'}</Text>
				<Text style={[cellTextStyle, c3, c_header]}>{'KMS'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{''}</Text><Text style={[cellTextStyle, c2]}>{'1.00'}</Text><Text style={[cellTextStyle, c3]}>{'0.50'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Caja 12 Cuartos'}</Text><Text style={[cellTextStyle, c2]}>{'3.00'}</Text><Text style={[cellTextStyle, c3]}>{'1.50'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Caja 4 Galones'}</Text><Text style={[cellTextStyle, c2]}>{'4.00'}</Text><Text style={[cellTextStyle, c3]}>{'2.00'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Medio Balde'}</Text><Text style={[cellTextStyle, c2]}>{'2.50'}</Text><Text style={[cellTextStyle, c3]}>{'1.25'}</Text>
			</View>
			<View style={cellStyle}>
				<Text style={[cellTextStyle, c1]}>{'Balde'}</Text><Text style={[cellTextStyle, c2]}>{'5.00'}</Text><Text style={[cellTextStyle, c3]}>{'2.50'}</Text>
			</View>
		</View >)

		/*containerStyle={{ flexWrap: "wrap", paddingBottom: 10, left: 20, height: 60, right: 20, width: 'auto', marginTop: 5 }}*/

		var table;
		var table_height;
		if (this.props.showTooltip) {
			if (this.props.tipo == "SINTETICO") {
				table = table1;
				table_height = 170;
			}
			if (this.props.tipo == "SEMI_SINTETICO") {
				titulo = "Semi sintético";
				table = table2;
				table_height = 130;
			}
			if (this.props.tipo == "MINERAL_PREMIUM") {
				titulo = "Mineral premium";
				table = table3;
				table_height = 150;
			}
			if (this.props.tipo == "MINERAL_ESTANDAR") {
				titulo = "Mineral estándar";
				table = table4;
				table_height = 150;
			}
		}

		return (
			<View>

				<IphoneXFix></IphoneXFix>
				<View style={[contentStyle, this.props.contentStyle]}>

					{this.props.onBack != null &&
						<View style={{ marginTop: 0, backgroundColor: 'transparent', flexDirection: 'row', alignItems: "center" }}>
							<TouchableOpacity style={{ width: 45, marginLeft: 10, backgroundColor: 'transparent' }}
								onPress={() => { this.props.onBack(); }}>
								<EvilIcons name={'chevron-left'} size={50} color={colorArrow} />
							</TouchableOpacity>
							<Text style={[styleTitleArrow, this.props.styleTitleArrow]}>
								{this.props.titleArrow}
								{this.props.fecha &&
									<Text style={{ fontSize: 13, lineHeight: 20 }}>{"\n" + this.props.fecha}</Text>
								}
							</Text>
							{this.props.showTooltip &&
								<View style={{ marginLeft: 10 }}>
									<Tooltip onOpen={() => {
									}} skipAndroidStatusBar={true} withPointer={false} overlayColor={'rgba(0,0,0,0.8)'} containerStyle={{ flexWrap: "wrap", left: 20, width: 250, height: table_height, marginTop: 5 }}
										popover={table} pointerColor={'white'} backgroundColor={'white'}>
										<SvgCustom width="18" height="18" source={"ico_info_blue"} style={{ marginTop: 2 }} />
									</Tooltip>
								</View>
							}



						</View>
					}
					{this.props.title &&
						<View style={[styleHeaderContent, this.props.style]}>

							{this.props.number &&
								<View style={{ marginTop: 0, width: 25, height: 25, backgroundColor: 'transparent', marginRight: 6 }}>
									<View style={{
										width: 25,
										height: 25,
										borderRadius: 25 / 2,
										backgroundColor: '#ccc',
										justifyContent: "center"
									}}>
										<Text style={{ color: global.azul_oscuro, fontFamily: global.fontBold, fontSize: 15, textAlign: "center" }}>{1}</Text>
									</View>
								</View>
							}
							{this.props.title &&
								<Text style={[styleTitle, this.props.styleTitle]}>
									{this.props.title}
								</Text>
							}
							{
								this.props.counter &&
								<View style={{ marginTop: 0, width: 30, height: 30, backgroundColor: 'transparent', position: "absolute", right: 30 }}>
									<View style={{
										width: 30,
										height: 30,
										borderRadius: 30 / 2,
										backgroundColor: global.azul_claro,
										justifyContent: "center"
									}}>
										<Text style={{ color: 'white', fontFamily: global.fontBold, fontSize: 13, textAlign: "center" }}>{this.props.counter}</Text>
									</View>
								</View>
							}

							{
								this.props.onCerrar &&
								<TouchableOpacity onPress={() => {
									this.props.onCerrar();
								}} style={{ marginRight: 20, width: 70, height: 40, backgroundColor: 'transparent', position: "absolute", right: 0 }}>
									<View style={{
										width: 70,
										height: 40,
										backgroundColor: "transparent",
										justifyContent: "center"
									}}>
										<Text style={{ color: global.azul_super_claro, fontFamily: global.fontBold, fontSize: 13, textAlign: "right" }}>{"CERRAR"}</Text>
									</View>
								</TouchableOpacity>
							}


							{
								this.props.kms &&
								<View style={{ marginTop: 0, backgroundColor: 'transparent', position: "absolute", right: 30 }}>
									<View style={{
										height: 30,
										borderRadius: 30 / 2,
										backgroundColor: global.azul_claro,
										justifyContent: "center"
									}}>
										<Text style={{ paddingLeft: 15, paddingRight: 15, color: 'white', fontFamily: global.fontBold, fontSize: 13, textAlign: "center" }}>{this.props.kms}</Text>
									</View>
								</View>
							}
							{
								this.props.onAyuda &&
								<TouchableOpacity onPress={() => {
									this.props.onAyuda();
								}} style={{ width: 30, height: 30, backgroundColor: 'transparent', position: 'absolute', right: 30, top: 0 }}>
									<SvgCustom width="30" height="30" source={"ico_ayuda"} />
								</TouchableOpacity>
							}


						</View >
					}



				</View >

			</View>
		);
	}
}
/*


*/