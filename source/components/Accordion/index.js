import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Accordion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            expanded: false,
        }
    }

    render() {

        return (
            <View>
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingLeft: 10,
                    paddingRight: 25,
                    paddingTop: 8,
                    paddingBottom: 8,
                    alignItems: 'center',
                    backgroundColor: "#FFF",
                }} onPress={() => this.toggleExpand()}>
                    <Text style={{
                        fontSize: 15,
                        fontWeight: '500',
                        width: '80%',

                        backgroundColor: "white",
                        color: global.azul_oscuro,
                        fontFamily: globalThis.fontSemiBold,
                    }}>{this.props.title}</Text>
                    <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={'#ccc'} />

                </TouchableOpacity>

                {
                    !this.state.expanded &&
                    <View style={{ backgroundColor: '#ccc', width: '100%', height: 1 }}></View>
                }
                {
                    this.state.expanded &&
                    <View style={{ backgroundColor: "white", padding: 25, paddingTop: 0, paddingLeft: 10, paddingRight: 35 }}>
                        <Text style={{
                            fontSize: 14,
                            textAlign: "justify",
                            fontWeight: '400',
                            color: global.azul_oscuro,
                            fontFamily: globalThis.fontRegular,
                        }}>{this.props.data}</Text>
                    </View>
                }
                {
                    this.state.expanded &&
                    <View style={{ backgroundColor: '#ccc', width: '100%', height: 1 }}></View>
                }
            </View>
        )
    }

    toggleExpand = () => {
        this.setState({ expanded: !this.state.expanded })
    }

}
