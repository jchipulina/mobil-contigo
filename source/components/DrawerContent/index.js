import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
var {height, width} = Dimensions.get('window');


export default class DrawerContent extends Component{ 
  constructor(props) {
    super(props)
  }
  render(){
	  
	  var navitems = new Array();
	  var avatar = (<View/>);

	  navitems.push({ name:'Pagos y Deudas', nav:'contact', icon:'credit-card'});
	  navitems.push({ name:'Consumos y Facturaciones', nav:'publicidad', icon:'light-bulb'});
	  navitems.push({ name:'Alertas de vencimiento', nav:'Salir', icon:'bell'});
	  navitems.push({ name:'Salir', nav:'Salir', icon:'x'});
	 
	  
    return  (
        <View style={{borderWidth:0, flex:1, backgroundColor:'#EEE', padding:0}}>

            <View style={{backgroundColor:'#d1273f',height:70,justifyContent:'center'}}>
            
            <Text style={{color:'white',fontFamily:'PFBeauSansPro-Regular',fontSize:20,paddingLeft:10,paddingTop:10}}>{'Melissa Trujillo'}</Text>
            
            
            </View>
            
            
            
        <View>
                      {
                          navitems.map((l,i)=>{
                            return (
	                            <View key={i}>
                                 <Octicons.Button name={l.icon} backgroundColor='#EEE' size={22} color="#e67730" style={{margin:5}} onPress={()=>{
	                                 	if(l.nav=="Salir"){
		                                 	//AsyncStorage.removeItem("user");
		                                 	//global.user=null;
		                                 	//this.props.navigation.navigate('Login');
	                                 	}else{
		                                 	//this.props.navigation.navigate(l.nav);
	                                 	}
	                                 	
	                                 }}>  
                                     <Text style={{ fontSize: 16, fontWeight: '300', paddingTop: 4, color: '#004155' ,fontFamily:'PFBeauSansPro-Regular' }} >{l.name}</Text>
                                     	
                                 </Octicons.Button>
                                 <View style={{backgroundColor:'#d2d2d2',height:1}} />
                                 </View>
                                 

                            )
                          })
                      }
                      </View> 
                   
                              
          </View>)
  }
}