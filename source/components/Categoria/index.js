import React, { Component } from "react"
import {
  View,
  Text,
  Image,
  Dimensions,
  Platform,
  TouchableOpacity
} from "react-native"
var { height, width } = Dimensions.get('window');
import { img } from "@media";
import LinearGradient from 'react-native-linear-gradient';

export default class Categoria extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var type = this.props.type;

    var f_width = 120;
    var f_height = 70;
    var marginRight = 12;
    var alignItems = "center";
    var paddingLeft = 0;
    var fontSize = 15;
    var fontColor = 'white';
    if (this.props.type == "full_width") {
      f_width = "100%";
      f_height = 70;
      marginRight = 0;
      alignItems = "flex-start";
      paddingLeft = 30;
      fontSize = 20;
      fontColor = global.azul;
    }
    if (this.props.color) {
      fontColor = 'white';
    }
    if (this.props.fontSize) {
      fontSize = this.props.fontSize;
    }
    /*{this.props.type == "categoria__" &&
          <Image resizeMode="cover" style={{ width: f_width, height: f_height, borderRadius: 10, position: "absolute" }} source={{ uri: global.urlimg + this.props.imagen }} source={img.temp}></Image>
        }
        
                {this.props.type == "categoria__" &&
          <View style={{ width: f_width, height: f_height, borderRadius: 10, position: "absolute", backgroundColor: 'rgba(31, 58, 147, 0.5)' }}></View>
        }
        */
    return (
      <TouchableOpacity style={[{
        width: f_width, height: f_height, justifyContent: "center", alignItems: "center",
        marginTop: 15, marginRight: marginRight, borderRadius: 10, backgroundColor: this.props.color, padding: 0
      }, this.props.style]} onPress={() => {
        this.props.onPress();
      }}>

        {(this.props.type == "full_width" && this.props.color == null) &&
          <Image resizeMode="cover" style={{ width: f_width, height: f_height, borderRadius: 10, position: "absolute" }} source={{ uri: global.urlimg + this.props.imagen }}></Image>
        }
        {(this.props.type == "full_width" && this.props.color == null) &&
          <View style={{ width: f_width, height: f_height, borderRadius: 10, position: "absolute", backgroundColor: 'rgba(255,255,255, 0.4)' }}></View>
        }
        <LinearGradient useAngle={true} colors={[(this.props.type == "full_width") ? "white" : "transparent", "rgba(255,255,255,0)"]} style={[{
          flexDirection: 'row', flex: 1, width: '100%', position: "absolute", zIndex: 1000, width: "100%", height: 70,
          justifyContent: "center", alignItems: alignItems, backgroundColor: 'transparent', borderRadius: 10
        }, this.props.styleText]}>

          <Text style={{ color: fontColor, paddingLeft: paddingLeft, textAlign: 'left', fontFamily: global.fontBold, fontSize: fontSize }}>{this.props.title}</Text>

          {this.props.showLine &&
            <View style={{ width: 4, height: 30, backgroundColor: 'rgba(1,1,1,0.5)', borderRadius: 10 }}></View>
          }

        </LinearGradient>
        {this.props.type == "full_width" &&
          <View style={{ width: 4, height: 25, borderRadius: 5, backgroundColor: 'rgba(1,1,1, 0.4)', position: "absolute", right: 20 }}></View>
        }




      </TouchableOpacity>
    );
  }
}