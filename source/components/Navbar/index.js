import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
var { height, width } = Dimensions.get('window');


export default class Navbar extends Component {
	constructor(props) {
		super(props);
	}
	isIphoneX() {
		const dimen = Dimensions.get('window');
		return (
			Platform.OS === 'ios' &&
			!Platform.isPad &&
			!Platform.isTVOS &&
			(dimen.height >= 812 || dimen.width >= 812)
		);
	}
	render() {
		var htop = 0;
		var ptop = 0;
		var fixarrow = 2;
		if (Platform.OS === 'ios') {

			if (this.isIphoneX()) {
				ptop = 20;
				htop = 90;
				fixarrow = 8;
			} else {
				ptop = 10;
				htop = 60;
			}

		} else {
			ptop = 0;
			htop = 50;
		}
		var arrow = "#FFF";
		if (this.props.onBack != null) {
			if (this.props.arrow) {
				arrow = this.props.arrow;
			}
		}
		var styleHeader = { width: 40, height: htop, paddingTop: ptop, backgroundColor: 'transparent', justifyContent: 'flex-start', position: 'absolute', zIndex: 100 }
		return (
			<View style={[styleHeader, this.props.style]}>
				<View style={{ flex: 1, alignItems: 'center', backgroundColor: 'transparent' }}>
					<TouchableOpacity style={{ width: 70, height: 55, position: 'absolute', left: 10, top: ptop + fixarrow }}
						onPress={() => { this.props.onBack(); }}>
						<EvilIcons name={'chevron-left'} size={50} color={arrow} />
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
