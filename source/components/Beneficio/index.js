import React, { Component } from "react"
import {
	View,
	Text,
	Dimensions,
	Platform,
	ActivityIndicator,
	TouchableOpacity
} from "react-native"
var { height, width } = Dimensions.get('window');
import { img } from "@media";
import Reactotron from "reactotron-react-native";
import Image from 'react-native-image-progress';

export default class Beneficio extends Component {
	constructor(props) {
		super(props);
		Reactotron.log(this.props.beneficio);
	}
	render() {

		var isPromo = false;
		if (this.props.type) {
			if (this.props.type == "promocion") {
				isPromo = true;
			}
		}
		var grupo_color;
		if (Number(this.props.beneficio.grupo) == 1) {
			grupo_color = global.azul_claro;
		}
		if (Number(this.props.beneficio.grupo) == 2) {
			grupo_color = global.rojo;
		}
		if (Number(this.props.beneficio.grupo) == 3) {
			grupo_color = global.azul;
		}
		return (
			<TouchableOpacity style={{ width: '100%', marginTop: 15, backgroundColor: (isPromo) ? global.azul : 'white', borderRadius: 10 }} onPress={() => {
				this.props.onPress(this.props.beneficio);
			}}>
				<Image indicator={ActivityIndicator} style={{
					marginTop: 0, width: '100%', height: 200, borderTopLeftRadius: 10,
					resizeMode: 'cover', borderTopRightRadius: 10
				}} source={{ uri: global.urlimg + this.props.beneficio.imagen }} />
				<View style={{
					width: '100%', backgroundColor: (isPromo) ? global.azul : 'white', borderBottomLeftRadius: 10,
					borderBottomRightRadius: 10, padding: 15, paddingTop: 10, backgroundColor: "transparent"
				}}>

					<View style={{ flexDirection: 'row', justifyContent: "space-between", backgroundColor: "transparent" }}>
						<Text style={{
							fontFamily: global.fontBold, backgroundColor: 'tranparent', flex: 1,
							color: (isPromo) ? 'white' : global.azul_oscuro, fontSize: 18, lineHeight: 18, paddingTop: 2,
						}}>{this.props.beneficio.titulo}</Text>


						{!isPromo && <View style={{
							backgroundColor: grupo_color, paddingTop: 2,
							paddingLeft: 10, paddingRight: 10, height: 20, borderRadius: 10
						}}>
							{(Number(this.props.beneficio.grupo) == 1) &&
								<Text style={{ fontFamily: global.fontSemiBold, color: 'white', fontSize: 12 }}>{'Empresa'}</Text>
							}
							{(Number(this.props.beneficio.grupo) == 2) &&
								<Text style={{ fontFamily: global.fontSemiBold, color: 'white', fontSize: 12 }}>{'Clientes'}</Text>
							}
							{(Number(this.props.beneficio.grupo) == 3) &&
								<Text style={{ fontFamily: global.fontSemiBold, color: 'white', fontSize: 12 }}>{'Familia'}</Text>
							}

						</View>}
					</View>
					<View style={{ flexDirection: "row" }}>
						<Text style={{ flex: 1, fontFamily: global.fontSemiBold, color: (isPromo) ? 'white' : global.gris, fontSize: 14, marginTop: 5 }}>{this.props.beneficio.descripcion}</Text>
						{!isPromo &&
							<View style={{ justifyContent: "flex-end", backgroundColor: 'white', marginLeft: 10, marginTop: 10 }}>
								<Text style={{ textAlign: "right", fontFamily: global.fontBold, color: global.rojo, fontSize: 22, lineHeight: 22 }}>{this.props.beneficio.puntos}</Text>
								<Text style={{ textAlign: "right", fontFamily: global.fontBold, color: global.rojo, fontSize: 12, lineHeight: 12 }}>{"KMS"}</Text>
							</View>
						}

					</View>


				</View>
			</TouchableOpacity>
		);
	}
}
