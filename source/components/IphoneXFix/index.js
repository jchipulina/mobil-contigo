import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native"
var { height, width } = Dimensions.get('window');

export default class IphoneXFix extends Component {
	constructor(props) {
		super(props);
	}
	is() {
		const dimen = Dimensions.get('window');
		return (
			Platform.OS === 'ios' &&
			!Platform.isPad &&
			!Platform.isTVOS &&
			(dimen.height >= 812 || dimen.width >= 812)
		);
	}
	isIphoneX() {
		const dimen = Dimensions.get('window');
		return (
			Platform.OS === 'ios' &&
			!Platform.isPad &&
			!Platform.isTVOS &&
			(dimen.height >= 812 || dimen.width >= 812)
		);
	}


	render() {
		var nh = 2;
		if (Platform.OS === 'ios') {
			nh = 15;
		}
		return (
			<View style={{ width: '100%', height: (this.isIphoneX()) ? 40 : nh, backgroundColor: 'rgba(0,0,0,0.0)' }}></View>
		);
	}
}
