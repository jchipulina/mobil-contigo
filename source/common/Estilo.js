import color from "./Color"

export default {
	pantalla:{
		flex: 1,
		padding: 12,
		backgroundColor: color.gris
	},
	fondo:{
		position: 'absolute',
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
		zIndex: -1,
	},
	blueThin:{
		fontFamily:'PFBeauSansPro-Thin',color:'#004155',fontSize:24
	}
}
